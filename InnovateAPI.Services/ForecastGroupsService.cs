﻿using InnovateAPI.Business;
using InnovateAPI.DataAccess;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Services
{
    public class ForecastGroupsService : IForecastGroupsServeice
    {

        APILayer _apiLayer;
        MongoDBDataHelper _mdbHelper;
        IConfiguration _iconfiguration;
        public ForecastGroupsService(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            _apiLayer = new APILayer(_iconfiguration);
            _mdbHelper = new MongoDBDataHelper(_iconfiguration);
        }

        bool isSaveToMongoDb = false;
        public ForecastGroups GetForecoastGroupById(string refSessionId, long ForecastId)
        {
            dynamic resultData;
            dynamic mongoResult = _mdbHelper.GetForecastGroupByIdData(ForecastId);
            if (mongoResult != null)
            {
                isSaveToMongoDb = false;
                resultData = mongoResult;
            }
            else
            {
                resultData = _apiLayer.GetForecastGroupByIdDataFromAPI(refSessionId, ForecastId);
                isSaveToMongoDb = true;

            }
           // dynamic resultData = _apiLayer.GetForecastGroupByIdDataFromAPI(refSessionId, ForecastId);
           return GetForcastGroupData(resultData);
        }

        public ForecastGroups GetForecoastGroupByName(string refSessionId, string ForecastName)
        {
            dynamic resultData;
            dynamic mongoResult = _mdbHelper.GetForecastGroupByNameData(ForecastName);
            if (mongoResult != null)
            {
                isSaveToMongoDb = false;
                resultData = mongoResult;
            }
            else
            {
                resultData = _apiLayer.GetForecastGroupByNameDataFromAPI(refSessionId, ForecastName);
                isSaveToMongoDb = true;

            }
           // dynamic resultData = _apiLayer.GetForecastGroupByNameDataFromAPI(refSessionId, ForecastName);
            return GetForcastGroupData(resultData);
        }

        private ForecastGroups GetForcastGroupData(dynamic resultData)
        {
            ForecastGroups forecastGroup = new ForecastGroups();
            if (resultData!=null)
            {
                forecastGroup.id = resultData.id;
                forecastGroup.name = resultData.name;
            }
            if (isSaveToMongoDb)
            {
                _mdbHelper.SaveForcastGroupData(forecastGroup);
            }
            return forecastGroup;

        }
    }
}
