﻿using InnovateAPI.Business;
using InnovateAPI.DataAccess;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Services
{
    public class DepartmentService : IDepartmentService
    {
        APILayer _apiLayer;
        MongoDBDataHelper _mdbHelper;
        IConfiguration _iconfiguration;
        public DepartmentService(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            _apiLayer = new APILayer(_iconfiguration);
            _mdbHelper = new MongoDBDataHelper(_iconfiguration);
        }

        public List<Department> GetDepartmentDataById(string refSessionId, long id)
        {
            dynamic resultData = _apiLayer.GetDepartmentDataByIdFromAPI(refSessionId, id);
            List<Department> dept = new List<Department>();
            try
            {
                if (resultData != null)
                {
                    var depts = new Department
                    {
                        departmentId = resultData.id,
                        name = resultData.name
                    };

                    dept.Add(depts);

                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogError(ex);
            }
            return dept;
        }


        public List<Department> GetDepartmentDataBySiteId(string refSessionId, long siteIdFilter)
        {
            dynamic resultData = _apiLayer.GetDepartmentDataBySiteIdFromAPI(refSessionId, siteIdFilter);
            List<Department> dept = new List<Department>();
            try
            {
                if (resultData != null)
                {

                    foreach (var orglevel in resultData.entities)
                    {
                        var depts = new Department
                        {
                            departmentId = orglevel.id.Value,
                            name = orglevel.name.Value
                        };

                        dept.Add(depts);
                    }

                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogError(ex);
            }
            return dept;
        }

        public List<Department> GetDepartmentDataByName(string refSessionId, string name)
        {
            dynamic resultData = _apiLayer.GetDepartmentDataByNameFromAPI(refSessionId, name);
            List<Department> dept = new List<Department>();
            try
            {
                if (resultData != null)
                {
                    var depts = new Department
                    {
                        departmentId = resultData.id,
                        name = resultData.name
                    };

                    dept.Add(depts);

                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogError(ex);
            }
            return dept;
        }

    }
}
