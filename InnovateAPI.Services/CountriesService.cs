﻿using InnovateAPI.Business;
using InnovateAPI.DataAccess;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Services
{
    public class CountriesService : ICountriesService
    {
        APILayer _apiLayer;
        MongoDBDataHelper _mdbHelper;
        IConfiguration _iconfiguration;
        public CountriesService(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            _apiLayer = new APILayer(_iconfiguration);
            _mdbHelper = new MongoDBDataHelper(_iconfiguration);
        }
        public List<Countries> GetCountries(string refSessionId)
        {           
            List<Countries> lstcountries = new List<Countries>();
            try
            {
                dynamic result = _mdbHelper.GetCountriesData();
                if (result.Count>0)
                {
                    foreach (var countries in result)
                    {
                        var countrieslist = new Countries
                        {
                            Id = countries.Id,
                            Name = countries.Name,
                            Code = countries.Code

                        };
                        lstcountries.Add(countrieslist);
                    }
                }

                else
                {
                    dynamic resultData = _apiLayer.GetCountriesDataFromAPI(refSessionId);
                    if (resultData != null)
                    {
                        foreach (var countries in resultData.entities)
                        {
                            var countrieslist = new Countries
                            {
                                Id = countries.id.Value,
                                Name = countries.name.Value,
                                Code = countries.name.Value

                            };
                            lstcountries.Add(countrieslist);
                        }
                    }
                    _mdbHelper.SaveCountriesData(lstcountries);
                }
               
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogError(ex);
            }
            return lstcountries;
        }

        public List<Countries> GetCountriesById(string refSessionId, long countriesId)
        {
            
            List<Countries> lstcountries = new List<Countries>();
            try
            {
                dynamic result = _mdbHelper.GetCountriesDataByID(countriesId);
                if (result !=null)
                {
                    var countries = new Countries()
                    {
                        Id = result.Id,
                        Name = result.Name,
                        Code = result.Code
                    };

                    lstcountries.Add(countries);
                }
                else
                {
                    dynamic resultData = _apiLayer.GetCountriesByIdDataFromAPI(refSessionId, countriesId);
                    if (resultData != null)
                    {
                        var countries = new Countries()
                        {
                            Id = resultData.id,
                            Name = resultData.name,
                            Code = resultData.code
                        };

                        lstcountries.Add(countries);
                    }
                }
              
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogError(ex);
            }           
            return lstcountries;
        }

    }
}
