﻿using InnovateAPI.Business;
using InnovateAPI.DataAccess;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using InnovateAPI.ViewModel;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;

namespace InnovateAPI.Services
{
    public class LaborDemandService : ILaborDemandService
    {
        APILayer _apiLayer;
        MongoDBDataHelper _mdbHelper;
        IConfiguration _iconfiguration;
        public LaborDemandService(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            _apiLayer = new APILayer(_iconfiguration);
            _mdbHelper = new MongoDBDataHelper(_iconfiguration);
        }

        public LaborDemand GetlaborDemandByLaborRoleId(string sessionId, long siteId, long laborRoleId, string businessDate, string frequency)
        {

            dynamic resultData = _apiLayer.GetLaborDemandByLaborRoleIdDataFromAPI(sessionId, siteId, laborRoleId, businessDate, frequency);
            return LaborDemandData(resultData);
        }

        public LaborDemand GetlaborDemandBySiteId(string sessionId, long siteId, string businessDate, string frequency)
        {
            dynamic resultData;
            if (1 == 1)
            {
                resultData = _mdbHelper.GetLaborHourDayDataFromMongoDB();
                return resultData;
            }
            else
            {
                resultData = _apiLayer.GetLaborDemandBySiteIdDataFromAPI(sessionId, siteId, businessDate, frequency);
            }


            return LaborDemandData(resultData);

        }

        public LaborDemand GetlaborDemandByWorkgroupId(string sessionId, long siteId, long workgroupId, string businessDate, string frequency)
        {
            dynamic resultData = _apiLayer.GetLaborDemandByWorkgroupIdDataFromAPI(sessionId, siteId, workgroupId, businessDate, frequency);
            return LaborDemandData(resultData);

        }

        private LaborDemand LaborDemandData(dynamic resultData)
        {
            LaborDemand lstLaborDemand = new LaborDemand();
            try
            {
                if (resultData != null)
                {
                    List<LaborDemandByDay> lstLaborDemandByDay = new List<LaborDemandByDay>();
                    List<LaborDemandByQuarterHour> lstLaborDemandByQuarterHour = new List<LaborDemandByQuarterHour>();

                    foreach (var LaborDemandByDay in resultData.laborDemandByDay)
                    {
                        var laborDemandByDay = new LaborDemandByDay
                        {
                            SiteId = LaborDemandByDay.siteId,
                            //WorkgroupId = LaborDemandByDay.workgroupId,
                            //LaborRoleId = LaborDemandByDay.laborRoleId,
                            BusinessDate = LaborDemandByDay.businessDate,
                            Hours = LaborDemandByDay.hours
                        };
                        lstLaborDemandByDay.Add(laborDemandByDay);
                    }
                    ///////////////////////////////////////////////////////////
                    foreach (var LaborDemandByQuarterHour in resultData.laborDemandByQuarterHour)
                    {
                        var laborDemandByQuarterHour = new LaborDemandByQuarterHour
                        {
                            SiteId = LaborDemandByQuarterHour.siteId,
                            //WorkgroupId = LaborDemandByQuarterHour.workgroupId,
                            //LaborRoleId = LaborDemandByQuarterHour.laborRoleId,
                            BusinessDate = LaborDemandByQuarterHour.businessDate,
                            Hours = LaborDemandByQuarterHour.hours
                        };
                        lstLaborDemandByQuarterHour.Add(laborDemandByQuarterHour);
                    }
                    /////////////////////////////////////////////

                    var laborDemand = new LaborDemand
                    {
                        LaborDemandByDay = lstLaborDemandByDay
                        //LaborDemandByQuarterHour = lstLaborDemandByQuarterHour

                    };

                    lstLaborDemand = laborDemand;
                }
                return lstLaborDemand;
            }
            catch (Exception ex)
            {
                return lstLaborDemand;
            }
        }

        public List<LaborDemandHourByDayVM> GetLaborDemandDataFromMongoDB(string sessionId, string startdate, string enddate, string[] siteId)
        {
            List<LaborDemandHourByDayVM> lstLaborDemandHourVM = new List<LaborDemandHourByDayVM>();
            List<LaborHourDataOG> lstLaborDataOG = new List<LaborHourDataOG>();
            try
            {

                if (siteId.Length > 0)
                {
                    lstLaborDataOG = _mdbHelper.GetLaborDemandByDayDataBySiteId(startdate, enddate, siteId);
                }
                else
                {
                    lstLaborDataOG = _mdbHelper.GetLaborDemandByDayData(startdate, enddate);
                }
                if (lstLaborDataOG.Count > 0)
                {
                    dynamic resultData = _apiLayer.GetOrgUnitsDataFromAPI(sessionId);

                    if (resultData != null)
                    {
                        List<OrgnisationUnit> objorgunit = new List<OrgnisationUnit>();

                        foreach (var orgunit in resultData.entities)
                        {
                            var organizationalUnit = new OrgnisationUnit
                            {
                                id = orgunit.id.Value,
                                orgName = orgunit.name.Value,
                                hierarchyLevelId = orgunit.organizationalHierarchyLevelAssignmentId.Value,
                                parentUnitId = orgunit.parentOrganizationalUnitAssignmentId.Value

                            };

                            objorgunit.Add(organizationalUnit);
                        }

                        long valuedata = Convert.ToInt64(objorgunit.Where(a => a.hierarchyLevelId == 1).Select(a => a.id).Skip(1).FirstOrDefault());
                        foreach (var orgunit in objorgunit)
                        {
                            List<LaborDemandHourByDayVM> childData = new List<LaborDemandHourByDayVM>();
                            var checkvalue = objorgunit.Where(a => a.parentUnitId == orgunit.id).FirstOrDefault();
                            LaborDemandHourByDayVM labordemanddata = new LaborDemandHourByDayVM();
                            LaborData labordata = new LaborData();

                            if (checkvalue == null)
                            {
                                if (orgunit.parentUnitId != valuedata)
                                {
                                    labordata.id = orgunit.id;
                                    labordata.hierarchyLevelId = orgunit.hierarchyLevelId;
                                    labordata.parentUnitId = orgunit.parentUnitId;
                                    labordata.orgName = orgunit.orgName;
                                    labordata.Week1 = lstLaborDataOG.Where(a => a.Week == 1 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week2 = lstLaborDataOG.Where(a => a.Week == 2 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week3 = lstLaborDataOG.Where(a => a.Week == 3 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week4 = lstLaborDataOG.Where(a => a.Week == 4 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week5 = lstLaborDataOG.Where(a => a.Week == 5 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week6 = lstLaborDataOG.Where(a => a.Week == 6 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week7 = lstLaborDataOG.Where(a => a.Week == 7 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week8 = lstLaborDataOG.Where(a => a.Week == 8 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week9 = lstLaborDataOG.Where(a => a.Week == 9 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week10 = lstLaborDataOG.Where(a => a.Week == 10 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week11 = lstLaborDataOG.Where(a => a.Week == 11 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week12 = lstLaborDataOG.Where(a => a.Week == 12 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week13 = lstLaborDataOG.Where(a => a.Week == 13 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week14 = lstLaborDataOG.Where(a => a.Week == 14 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week15 = lstLaborDataOG.Where(a => a.Week == 15 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week16 = lstLaborDataOG.Where(a => a.Week == 16 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week17 = lstLaborDataOG.Where(a => a.Week == 17 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week18 = lstLaborDataOG.Where(a => a.Week == 18 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week19 = lstLaborDataOG.Where(a => a.Week == 19 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week20 = lstLaborDataOG.Where(a => a.Week == 20 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week21 = lstLaborDataOG.Where(a => a.Week == 21 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week22 = lstLaborDataOG.Where(a => a.Week == 22 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week23 = lstLaborDataOG.Where(a => a.Week == 23 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week24 = lstLaborDataOG.Where(a => a.Week == 24 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week25 = lstLaborDataOG.Where(a => a.Week == 25 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week26 = lstLaborDataOG.Where(a => a.Week == 26 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week27 = lstLaborDataOG.Where(a => a.Week == 27 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week28 = lstLaborDataOG.Where(a => a.Week == 28 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week29 = lstLaborDataOG.Where(a => a.Week == 29 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week30 = lstLaborDataOG.Where(a => a.Week == 30 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week31 = lstLaborDataOG.Where(a => a.Week == 31 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week32 = lstLaborDataOG.Where(a => a.Week == 32 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week33 = lstLaborDataOG.Where(a => a.Week == 33 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week34 = lstLaborDataOG.Where(a => a.Week == 34 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week35 = lstLaborDataOG.Where(a => a.Week == 35 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week36 = lstLaborDataOG.Where(a => a.Week == 36 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week37 = lstLaborDataOG.Where(a => a.Week == 37 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week38 = lstLaborDataOG.Where(a => a.Week == 38 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week39 = lstLaborDataOG.Where(a => a.Week == 39 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week40 = lstLaborDataOG.Where(a => a.Week == 40 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week41 = lstLaborDataOG.Where(a => a.Week == 41 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week42 = lstLaborDataOG.Where(a => a.Week == 42 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week43 = lstLaborDataOG.Where(a => a.Week == 43 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week44 = lstLaborDataOG.Where(a => a.Week == 44 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week45 = lstLaborDataOG.Where(a => a.Week == 45 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week46 = lstLaborDataOG.Where(a => a.Week == 46 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week47 = lstLaborDataOG.Where(a => a.Week == 47 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week48 = lstLaborDataOG.Where(a => a.Week == 48 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week49 = lstLaborDataOG.Where(a => a.Week == 49 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week50 = lstLaborDataOG.Where(a => a.Week == 50 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week51 = lstLaborDataOG.Where(a => a.Week == 51 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordata.Week52 = lstLaborDataOG.Where(a => a.Week == 52 && a.SiteId == orgunit.id.ToString()).Select(a => a.Hours).FirstOrDefault();
                                    labordemanddata.expanded = false;
                                }
                                else
                                {
                                    labordata.id = orgunit.id;
                                    labordata.orgName = orgunit.orgName;
                                    labordata.hierarchyLevelId = orgunit.hierarchyLevelId;
                                    labordata.parentUnitId = orgunit.parentUnitId;
                                    labordemanddata.expanded = true;
                                    labordata.Week1 = 0;
                                    labordata.Week2 = 0;
                                    labordata.Week3 = 0;
                                    labordata.Week4 = 0;
                                    labordata.Week5 = 0;
                                    labordata.Week6 = 0;
                                    labordata.Week7 = 0;
                                    labordata.Week8 = 0;
                                    labordata.Week9 = 0;
                                    labordata.Week10 = 0;
                                    labordata.Week11 = 0;
                                    labordata.Week12 = 0;
                                    labordata.Week13 = 0;
                                    labordata.Week14 = 0;
                                    labordata.Week15 = 0;
                                    labordata.Week16 = 0;
                                    labordata.Week17 = 0;
                                    labordata.Week18 = 0;
                                    labordata.Week19 = 0;
                                    labordata.Week20 = 0;
                                    labordata.Week21 = 0;
                                    labordata.Week22 = 0;
                                    labordata.Week23 = 0;
                                    labordata.Week24 = 0;
                                    labordata.Week25 = 0;
                                    labordata.Week26 = 0;
                                    labordata.Week27 = 0;
                                    labordata.Week28 = 0;
                                    labordata.Week29 = 0;
                                    labordata.Week30 = 0;
                                    labordata.Week31 = 0;
                                    labordata.Week32 = 0;
                                    labordata.Week33 = 0;
                                    labordata.Week34 = 0;
                                    labordata.Week35 = 0;
                                    labordata.Week36 = 0;
                                    labordata.Week37 = 0;
                                    labordata.Week38 = 0;
                                    labordata.Week39 = 0;
                                    labordata.Week40 = 0;
                                    labordata.Week41 = 0;
                                    labordata.Week42 = 0;
                                    labordata.Week43 = 0;
                                    labordata.Week44 = 0;
                                    labordata.Week45 = 0;
                                    labordata.Week46 = 0;
                                    labordata.Week47 = 0;
                                    labordata.Week48 = 0;
                                    labordata.Week49 = 0;
                                    labordata.Week50 = 0;
                                    labordata.Week51 = 0;
                                    labordata.Week52 = 0;

                                }

                            }
                            else
                            {

                                labordata.id = orgunit.id;
                                labordata.orgName = orgunit.orgName;
                                labordata.hierarchyLevelId = orgunit.hierarchyLevelId;
                                labordata.parentUnitId = orgunit.parentUnitId;
                                labordemanddata.expanded = true;
                                labordata.Week1 = 0;
                                labordata.Week2 = 0;
                                labordata.Week3 = 0;
                                labordata.Week4 = 0;
                                labordata.Week5 = 0;
                                labordata.Week6 = 0;
                                labordata.Week7 = 0;
                                labordata.Week8 = 0;
                                labordata.Week9 = 0;
                                labordata.Week10 = 0;
                                labordata.Week11 = 0;
                                labordata.Week12 = 0;
                                labordata.Week13 = 0;
                                labordata.Week14 = 0;
                                labordata.Week15 = 0;
                                labordata.Week16 = 0;
                                labordata.Week17 = 0;
                                labordata.Week18 = 0;
                                labordata.Week19 = 0;
                                labordata.Week20 = 0;
                                labordata.Week21 = 0;
                                labordata.Week22 = 0;
                                labordata.Week23 = 0;
                                labordata.Week24 = 0;
                                labordata.Week25 = 0;
                                labordata.Week26 = 0;
                                labordata.Week27 = 0;
                                labordata.Week28 = 0;
                                labordata.Week29 = 0;
                                labordata.Week30 = 0;
                                labordata.Week31 = 0;
                                labordata.Week32 = 0;
                                labordata.Week33 = 0;
                                labordata.Week34 = 0;
                                labordata.Week35 = 0;
                                labordata.Week36 = 0;
                                labordata.Week37 = 0;
                                labordata.Week38 = 0;
                                labordata.Week39 = 0;
                                labordata.Week40 = 0;
                                labordata.Week41 = 0;
                                labordata.Week42 = 0;
                                labordata.Week43 = 0;
                                labordata.Week44 = 0;
                                labordata.Week45 = 0;
                                labordata.Week46 = 0;
                                labordata.Week47 = 0;
                                labordata.Week48 = 0;
                                labordata.Week49 = 0;
                                labordata.Week50 = 0;
                                labordata.Week51 = 0;
                                labordata.Week52 = 0;
                            }

                            labordemanddata.data = labordata;

                            lstLaborDemandHourVM.Add(labordemanddata);

                        }

                    }

                }

                return lstLaborDemandHourVM.Skip(1).ToList();

            }
            catch (Exception ex)
            {
                return lstLaborDemandHourVM;
            }
        }

        private LaborDemand LaborDemandByDayData(dynamic resultData)
        {
            LaborDemand lstLaborDemand = new LaborDemand();
            List<LaborDemandByDay> lstLaborDemandByDay = new List<LaborDemandByDay>();
            try
            {
                if (resultData != null)
                {
                    foreach (var LaborDemandByDay in resultData)
                    {
                        var laborDemandByDay = new LaborDemandByDay
                        {
                            SiteId = LaborDemandByDay.SiteId,

                            BusinessDate = LaborDemandByDay.BusinessDate,
                            Hours = LaborDemandByDay.Hours
                        };
                        lstLaborDemandByDay.Add(laborDemandByDay);
                    }

                    var laborDemand = new LaborDemand
                    {
                        LaborDemandByDay = lstLaborDemandByDay

                    };

                    lstLaborDemand = laborDemand;



                }
                return lstLaborDemand;
            }
            catch (Exception ex)
            {
                return lstLaborDemand;
            }
        }
    }
}
