﻿using InnovateAPI.Business;
using InnovateAPI.DataAccess;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using InnovateAPI.ViewModel;
using Microsoft.Extensions.Configuration;
using System; 
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InnovateAPI.Services
{
    public class OrganizationalUnitsService : IOrganizationalUnitsService
    {
        APILayer _apiLayer;
        MongoDBDataHelper _mdbHelper;
        IConfiguration _iconfiguration;
        public OrganizationalUnitsService(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            _apiLayer = new APILayer(_iconfiguration);
            _mdbHelper = new MongoDBDataHelper(_iconfiguration);
        }
        public List<OrganizationalUnitsVM> GetOrganizationalUnits(string refSessionId)
        {

            //Code for Retriving Organizational Units Data from MongoDB Database
            //GetOrgUnitsDataFromDB();
            //this call will go to Data Access Layer

            List<OrganizationalUnits> lstOrganizationalUnits = _mdbHelper.GetOrganizationalUnits();

            List<OrganizationalUnitsVM> lstOrgUnitVM = new List<OrganizationalUnitsVM>();
            try
            {
                if (lstOrganizationalUnits.Count != 0)
                {
                    foreach (var orgunit in lstOrganizationalUnits)
                    {
                        var organizationalUnit = new OrganizationalUnitsVM
                        {
                            data = new Data
                            {
                                id = orgunit.Id,
                                orgName = orgunit.OrgName,
                                hierarchyLevelId = orgunit.HierarchyLevelId,
                                parentUnitId = orgunit.ParentUnitId
                            },
                            expanded = true
                        };

                        lstOrgUnitVM.Add(organizationalUnit);
                    }
                }
                else
                {
                    //this call will go to Business Layer
                    dynamic resultData = _apiLayer.GetOrgUnitsDataFromAPI(refSessionId);
                    if (resultData != null)
                    {
                        foreach (var orgunit in resultData.entities)
                        {
                            var organizationalUnit = new OrganizationalUnitsVM
                            {
                                data = new Data
                                {
                                    id = orgunit.id.Value,
                                    orgName = orgunit.name.Value,
                                    hierarchyLevelId = orgunit.organizationalHierarchyLevelAssignmentId.Value,
                                    parentUnitId = orgunit.parentOrganizationalUnitAssignmentId.Value
                                },
                                expanded = true
                            };

                            lstOrgUnitVM.Add(organizationalUnit);
                        }
                        lstOrganizationalUnits = lstOrgUnitVM.Select(a => new OrganizationalUnits() { Id = a.data.id, OrgName = a.data.orgName, HierarchyLevelId = a.data.hierarchyLevelId, ParentUnitId = a.data.parentUnitId }).ToList();
                    }
                    //call to save API retrived data into database
                    _mdbHelper.SaveOrgUnitsData(lstOrganizationalUnits);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogError(ex);
            }
            return lstOrgUnitVM;
        }

        public List<OrganizationalUnitsVM> GetOrganizationalUnitsMetricData(string sessionId, string startDate, string endDate, string[] metricId)
        {

         //   List<OrganizationalUnits> lstOrganizationalUnits = _mdbHelper.GetOrganizationalUnits();

            List<OrganizationalUnitsVM> lstOrgUnitVM = new List<OrganizationalUnitsVM>();
            List<MatricDataOG> lstMatricDataOG = new List<MatricDataOG>();
            if (metricId.Length > 0)
            {
                lstMatricDataOG = _mdbHelper.GetNewMetricDatasByMetric(startDate, endDate, metricId);
            }
            else
            {
                lstMatricDataOG = _mdbHelper.GetNewMetricDatas(startDate, endDate);
            }
            if (lstMatricDataOG.Count > 0)
            {

                try
                {
                    //if (lstOrganizationalUnits.Count != 0)
                    //{
                    //    foreach (var orgunit in lstOrganizationalUnits)
                    //    {
                           
                    //        var organizationalUnit = new OrganizationalUnitsVM
                    //        {
                    //            data = new Data
                    //            {
                    //                id = orgunit.Id,
                    //                orgName = orgunit.OrgName,
                    //                hierarchyLevelId = orgunit.HierarchyLevelId,
                    //                parentUnitId = orgunit.ParentUnitId,
                    //                Week1 = lstMatricDataOG.Where(a => a.Week == 1 && a.Store == orgunit.OrgName).Select(a => a.MetricValue).FirstOrDefault(),
                    //                Week2 = lstMatricDataOG.Where(a => a.Week == 2 && a.Store == orgunit.OrgName).Select(a => a.MetricValue).FirstOrDefault(),
                    //                Week3 = lstMatricDataOG.Where(a => a.Week == 3 && a.Store == orgunit.OrgName).Select(a => a.MetricValue).FirstOrDefault(),
                    //                Week4 = lstMatricDataOG.Where(a => a.Week == 4 && a.Store == orgunit.OrgName).Select(a => a.MetricValue).FirstOrDefault(),
                    //                Week5 = lstMatricDataOG.Where(a => a.Week == 5 && a.Store == orgunit.OrgName).Select(a => a.MetricValue).FirstOrDefault(),
                    //                Week6 = lstMatricDataOG.Where(a => a.Week == 6 && a.Store == orgunit.OrgName).Select(a => a.MetricValue).FirstOrDefault(),
                    //                Week7 = lstMatricDataOG.Where(a => a.Week == 7 && a.Store == orgunit.OrgName).Select(a => a.MetricValue).FirstOrDefault(),
                    //                Week8 = lstMatricDataOG.Where(a => a.Week == 8 && a.Store == orgunit.OrgName).Select(a => a.MetricValue).FirstOrDefault(),
                    //                Week9 = lstMatricDataOG.Where(a => a.Week == 9 && a.Store == orgunit.OrgName).Select(a => a.MetricValue).FirstOrDefault(),
                    //                Week10 = lstMatricDataOG.Where(a => a.Week == 10 && a.Store == orgunit.OrgName).Select(a => a.MetricValue).FirstOrDefault(),
                    //                Week11 = lstMatricDataOG.Where(a => a.Week == 11 && a.Store == orgunit.OrgName).Select(a => a.MetricValue).FirstOrDefault(),
                    //                Week12 = lstMatricDataOG.Where(a => a.Week == 12 && a.Store == orgunit.OrgName).Select(a => a.MetricValue).FirstOrDefault(),
                    //                Week13 = lstMatricDataOG.Where(a => a.Week == 13 && a.Store == orgunit.OrgName).Select(a => a.MetricValue).FirstOrDefault()
                    //            },
                    //            expanded = true
                    //        };

                    //        lstOrgUnitVM.Add(organizationalUnit);
                    //    }
                    //}
                    //else
                    //{
                        //this call will go to Business Layer
                        dynamic resultData = _apiLayer.GetOrgUnitsDataFromAPI(sessionId);
                        if (resultData != null)
                        {
                            foreach (var orgunit in resultData.entities)
                            {
                                var organizationalUnit = new OrganizationalUnitsVM
                                {
                                    data = new Data
                                    {
                                        id = orgunit.id.Value,
                                        orgName = orgunit.name.Value,
                                        hierarchyLevelId = orgunit.organizationalHierarchyLevelAssignmentId.Value,
                                        parentUnitId = orgunit.parentOrganizationalUnitAssignmentId.Value,
                                        Week1 = lstMatricDataOG.Where(a => a.Week == 1 && a.Store == (string)orgunit.name).Select(a => a.MetricValue).FirstOrDefault(),
                                        Week2 = lstMatricDataOG.Where(a => a.Week == 2 && a.Store == (string)orgunit.name).Select(a => a.MetricValue).FirstOrDefault(),
                                        Week3 = lstMatricDataOG.Where(a => a.Week == 3 && a.Store == (string)orgunit.name).Select(a => a.MetricValue).FirstOrDefault(),
                                        Week4 = lstMatricDataOG.Where(a => a.Week == 4 && a.Store == (string)orgunit.name).Select(a => a.MetricValue).FirstOrDefault(),
                                        Week5 = lstMatricDataOG.Where(a => a.Week == 5 && a.Store == (string)orgunit.name).Select(a => a.MetricValue).FirstOrDefault(),
                                        Week6 = lstMatricDataOG.Where(a => a.Week == 6 && a.Store == (string)orgunit.name).Select(a => a.MetricValue).FirstOrDefault(),
                                        Week7 = lstMatricDataOG.Where(a => a.Week == 7 && a.Store == (string)orgunit.name).Select(a => a.MetricValue).FirstOrDefault(),
                                        Week8 = lstMatricDataOG.Where(a => a.Week == 8 && a.Store == (string)orgunit.name).Select(a => a.MetricValue).FirstOrDefault(),
                                        Week9 = lstMatricDataOG.Where(a => a.Week == 9 && a.Store == (string)orgunit.name).Select(a => a.MetricValue).FirstOrDefault(),
                                        Week10 = lstMatricDataOG.Where(a => a.Week == 10 && a.Store == (string)orgunit.name).Select(a => a.MetricValue).FirstOrDefault(),
                                        Week11 = lstMatricDataOG.Where(a => a.Week == 11 && a.Store == (string)orgunit.name).Select(a => a.MetricValue).FirstOrDefault(),
                                        Week12 = lstMatricDataOG.Where(a => a.Week == 12 && a.Store == (string)orgunit.name).Select(a => a.MetricValue).FirstOrDefault(),
                                        Week13 = lstMatricDataOG.Where(a => a.Week == 13 && a.Store == (string)orgunit.name).Select(a => a.MetricValue).FirstOrDefault()
                                    },
                                    expanded = true
                                };

                                lstOrgUnitVM.Add(organizationalUnit);
                            }
                           // lstOrganizationalUnits = lstOrgUnitVM.Select(a => new OrganizationalUnits() { Id = a.data.id, OrgName = a.data.orgName, HierarchyLevelId = a.data.hierarchyLevelId, ParentUnitId = a.data.parentUnitId }).ToList();
                        }
                        //call to save API retrived data into database
                        //_mdbHelper.SaveOrgUnitsData(lstOrganizationalUnits);
                   // }
                }
                catch (Exception ex)
                {
                    ExceptionLogger.LogError(ex);
                }
            }
            var metricOrg = new OrganizationalUnitsVM
            {
                Metrics = _mdbHelper.GetMetricList(),
                data = new Data
                {
                    id = 0
                }
            };
            lstOrgUnitVM.Add(metricOrg);

            // for OrganizationLevels
          //var OrgLevels = _apiLayer.GetOrgHierarchyLevelsDataFromAPI(sessionId);
          //  List<OrgHierarchyLevels> lstOrganizationalLevels = new List<OrgHierarchyLevels>();
           
          //      if (OrgLevels != null)
          //      {
          //          foreach (var orglevel in OrgLevels.entities)
          //          {
          //          var levels = lstOrgUnitVM.Where(a => a.data.id == Convert.ToInt64(orglevel.id)).FirstOrDefault();
          //          if (levels != null)
          //          {
          //              var organizationalLevels = new OrgHierarchyLevels
          //              {
          //                  Id = orglevel.id.Value,
          //                  Name = orglevel.name.Value,
          //                  Level =(long)levels.data.hierarchyLevelId
          //              };

          //              lstOrganizationalLevels.Add(organizationalLevels);
          //          }
          //          }
          //      }
            

            return lstOrgUnitVM;
        }

    }
}
