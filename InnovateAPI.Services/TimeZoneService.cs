﻿using InnovateAPI.Business;
using InnovateAPI.DataAccess;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Services
{
    public class TimeZoneService : ITimeZoneService
    {
        APILayer _apiLayer;
        MongoDBDataHelper _mdbHelper;
        IConfiguration _iconfiguration;
        public TimeZoneService(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            _apiLayer = new APILayer(_iconfiguration);
            _mdbHelper = new MongoDBDataHelper(_iconfiguration);
        }
        public List<TimeZones> GetTimeZones(string refSessionId)
        {
            dynamic resultData = _apiLayer.GetTimeZonesDataFromAPI(refSessionId);
            List<TimeZones> lsttimezones = new List<TimeZones>();
            try
            {
                if (resultData != null)
                {
                    foreach (var timezones in resultData.entities)
                    {
                        var timezonelist = new TimeZones
                        {
                            Id = timezones.id.Value,
                            Name = timezones.name.Value

                        };
                        lsttimezones.Add(timezonelist);
                    }
                }
               
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogError(ex);              
            }
            return lsttimezones;
        }
    }
}
