﻿using InnovateAPI.Business;
using InnovateAPI.DataAccess;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Services
{
    public class ActualLaborCostsService : IActualLaborCostsService
    {
        APILayer _apiLayer;
        MongoDBDataHelper _mdbHelper;
        IConfiguration _iconfiguration;
        public ActualLaborCostsService(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            _apiLayer = new APILayer(_iconfiguration);
            _mdbHelper = new MongoDBDataHelper(_iconfiguration);
        }
        bool isSaveToMongoDb = false;
        public ActualLaborCosts GetActualLaborCostsByEmployeeId(string refSessionId, long siteId, long employeeId, string businessDate,  string frequency)
        {
          
            dynamic resultData;
            dynamic mongoResult = _mdbHelper.GetActaulLaborCostByEmployeeIdData(siteId, employeeId);
            if (mongoResult != null)
            {
                isSaveToMongoDb = false;
                resultData = mongoResult;
            }
            else
            {
                resultData = _apiLayer.GetActualLaborCostsByEmployeeId(refSessionId, siteId, employeeId, businessDate, frequency);
                isSaveToMongoDb = true; 

            }


            ActualLaborCosts lstActualLaborCosts = new ActualLaborCosts();

            //try
            //{
            if (resultData != null)
            {
                ////    foreach (var actualCost in resultData)
                ////    {
                List<ActualLaborCostByHour> lstActualLaborCostByHour = new List<ActualLaborCostByHour>();
                List<ActualLaborCostByDay> lstActualLaborCostByDay = new List<ActualLaborCostByDay>();

                foreach (var ActualLaborHour in resultData.actualLaborCostByHour)
                {
                    var actualLaborHour = new ActualLaborCostByHour
                    {
                        siteId = ActualLaborHour.siteId,
                        employeeId = ActualLaborHour.employeeId,
                        businessDate = ActualLaborHour.businessDate,
                        startTime = ActualLaborHour.startTime,
                        endTime = ActualLaborHour.endTime,
                        actualLaborCost = ActualLaborHour.ActualLaborCost
                    };
                    lstActualLaborCostByHour.Add(actualLaborHour);

                }
                foreach (var ActualLaborDay in resultData.actualLaborCostByDay)
                {
                    var actualLaborDay = new ActualLaborCostByDay
                    {
                        siteId = ActualLaborDay.siteId,
                        employeeId = ActualLaborDay.employeeId,
                        businessDate = ActualLaborDay.businessDate,
                        actualLaborCost = ActualLaborDay.ActualLaborCost
                    };
                    lstActualLaborCostByDay.Add(actualLaborDay);

                }
                //   long datas = actualCost;
                var actualLaborCost = new ActualLaborCosts
                {
                    siteId = resultData.siteId,
                    employeeId = resultData.employeeId,
                    businessDate = resultData.businessDate,
                    frequencyCode = resultData.frequencyCode,
                    actualLaborCostByDay = lstActualLaborCostByDay,
                    actualLaborCostByHour = lstActualLaborCostByHour
                };
                lstActualLaborCosts = actualLaborCost;
                //    }
                if (isSaveToMongoDb)
                {
                    _mdbHelper.SaveActualLaborCostData(actualLaborCost);
                }
            }
          

            return lstActualLaborCosts;
            //}
            //catch (Exception ex)
            //{
            //   return lstActualLaborCosts;
            //}
        }

        public ActualLaborCosts GetActualLaborCostsBySiteId(string refSessionId, long siteId,  string businessDate, string frequency)
        {

            dynamic resultData;
            dynamic mongoResult = _mdbHelper.GetActaulLaborCostBySiteIdData(siteId);
            if (mongoResult !=null)
            {
                isSaveToMongoDb = false;
                resultData = mongoResult;
            }
            else
            {
                 resultData = _apiLayer.GetActualLaborCostsBySiteId(refSessionId, siteId, businessDate, frequency);
                isSaveToMongoDb = true;

            }


            ActualLaborCosts lstActualLaborCosts = new ActualLaborCosts();

            //try
            //{
            if (resultData != null)
            {
                ////    foreach (var actualCost in resultData)
                ////    {
                List<ActualLaborCostByHour> lstActualLaborCostByHour = new List<ActualLaborCostByHour>();
                List<ActualLaborCostByDay> lstActualLaborCostByDay = new List<ActualLaborCostByDay>();

                foreach (var ActualLaborHour in resultData.actualLaborCostByHour)
                {
                    var actualLaborHour = new ActualLaborCostByHour
                    {
                        siteId = ActualLaborHour.siteId,
                        employeeId = ActualLaborHour.employeeId,
                        businessDate = ActualLaborHour.businessDate,
                        startTime = ActualLaborHour.startTime,
                        endTime = ActualLaborHour.endTime,
                        actualLaborCost = ActualLaborHour.ActualLaborCost
                    };
                    lstActualLaborCostByHour.Add(actualLaborHour);

                }
                foreach (var ActualLaborDay in resultData.actualLaborCostByDay)
                {
                    var actualLaborDay = new ActualLaborCostByDay
                    {
                        siteId = ActualLaborDay.siteId,
                        employeeId = ActualLaborDay.employeeId,
                        businessDate = ActualLaborDay.businessDate,
                        actualLaborCost = ActualLaborDay.ActualLaborCost
                    };
                    lstActualLaborCostByDay.Add(actualLaborDay);

                }
                //   long datas = actualCost;
                var actualLaborCost = new ActualLaborCosts
                {
                    siteId = resultData.siteId,
                    employeeId = resultData.employeeId,
                    businessDate = resultData.businessDate,
                    frequencyCode = resultData.frequencyCode,
                    actualLaborCostByDay = lstActualLaborCostByDay,
                    actualLaborCostByHour = lstActualLaborCostByHour
                };
                lstActualLaborCosts = actualLaborCost;
                //    }

            }
            if (isSaveToMongoDb)
            {
                _mdbHelper.SaveActualLaborCostData(lstActualLaborCosts);
            }
            return lstActualLaborCosts;
            //}
            //catch (Exception ex)
            //{
            //   return lstActualLaborCosts;
            //}
        }

       
    }
}
