﻿using InnovateAPI.Business;
using InnovateAPI.DataAccess;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Services
{
    public class LanguagesService : ILanguagesService
    {
        APILayer _apiLayer;
        MongoDBDataHelper _mdbHelper;
        IConfiguration _iconfiguration;
        public LanguagesService(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            _apiLayer = new APILayer(_iconfiguration);
            _mdbHelper = new MongoDBDataHelper(_iconfiguration);
        }
        public List<Languages> GetLanguages(string refSessionId)
        {
            List<Languages> lstlanguages = new List<Languages>();
            try
            {   //Get Data From Database 
                dynamic result = _mdbHelper.GetLanguagesData();
                if (result != null)
                {

                    foreach (var languages in result)
                    {
                        var languageslist = new Languages
                        {
                            Id = languages.Id,
                            Name = languages.Name,
                            IsoLanguageId = languages.IsoLanguageId

                        };
                        lstlanguages.Add(languageslist);
                    }
                }
                //Get Data From Blue Younder API
                else
                {
                    dynamic resultData = _apiLayer.GetLangauesDataFromAPI(refSessionId);
                    if (resultData != null)
                    {
                        foreach (var languages in resultData.entities)
                        {
                            var languageslist = new Languages
                            {
                                Id = languages.id.Value,
                                Name = languages.name.Value,
                                IsoLanguageId = languages.isoLanguageId.Value

                            };
                            lstlanguages.Add(languageslist);
                        }
                    }
                    //Save Data to database 
                    _mdbHelper.SaveLanguagesData(lstlanguages);
                }
            }

            catch (Exception ex)
            {
                ExceptionLogger.LogError(ex);
            }
            return lstlanguages;
        }
        public List<Languages> GetLanguageById(string refSessionId, long languagesId)
        {
            List<Languages> lstlanguages = new List<Languages>();
            try
            {
                dynamic result = _mdbHelper.GetLanguagesDataByID(languagesId);
                if (result != null)
                {
                    var Languages = new Languages()
                    {
                        Id = result.Id,
                        Name = result.Name,
                        IsoLanguageId = result.IsoLanguageId
                    };
                    lstlanguages.Add(Languages);
                }
                else
                {
                    dynamic resultData = _apiLayer.GetLangaueByIdDataFromAPI(refSessionId, languagesId);
                    if (resultData != null)
                    {
                        var Languages = new Languages()
                        {
                            Id = resultData.id,
                            Name = resultData.name,
                            IsoLanguageId = resultData.isoLanguageId
                        };
                        lstlanguages.Add(Languages);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogError(ex);
            }
            return lstlanguages;
        }

    }
}
