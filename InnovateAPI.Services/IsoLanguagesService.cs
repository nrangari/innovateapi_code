﻿using InnovateAPI.Business;
using InnovateAPI.DataAccess;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Services
{
    public class IsoLanguagesService : IIsoLanguagesService
    {
        APILayer _apiLayer;
        MongoDBDataHelper _mdbHelper;
        IConfiguration _iconfiguration;
        public IsoLanguagesService(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            _apiLayer = new APILayer(_iconfiguration);
            _mdbHelper = new MongoDBDataHelper(_iconfiguration);
        }
        public List<IsoLanguages> GetIsoLanguages(string refSessionId, long isolanguagesid)
        {
            dynamic resultData = _apiLayer.GetIsoLangauesDataFromAPI(refSessionId, isolanguagesid);
            List<IsoLanguages> lstisolanguages = new List<IsoLanguages>();
            try
            {               
                if (resultData != null)
                {
                    var isoLanguages = new IsoLanguages()
                    {
                        Id = resultData.id,
                        Name = resultData.name,
                        Code = resultData.code
                    };

                    lstisolanguages.Add(isoLanguages);
                }
               
            }
            catch (Exception ex)
            {
               ExceptionLogger.LogError(ex);
            }
            return lstisolanguages;
        }
    }
}
