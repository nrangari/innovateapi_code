﻿using InnovateAPI.Business;
using InnovateAPI.DataAccess;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Services
{
    public class MetricDatasService : IMetricDatasService
    {
        APILayer _apiLayer;
        MongoDBDataHelper _mdbHelper;
        IConfiguration _iconfiguration;
        public MetricDatasService(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            _apiLayer = new APILayer(_iconfiguration);
            _mdbHelper = new MongoDBDataHelper(_iconfiguration);
        }


        public dynamic GetMetricDatas(string startDate, string endDate)
        {
            List<MatricDataOG> lstMatricDataOG = new List<MatricDataOG>();
            try
            {
                var resultData = _mdbHelper.GetNewMetricDatas(startDate,endDate);
                if (resultData !=null)
                {
                    return /*lstMetricData =*/ resultData;
                }
                return lstMatricDataOG;
            }
            catch (Exception ex)
            {
                return lstMatricDataOG;
            }
        }
    }
}

