﻿using InnovateAPI.Business;
using InnovateAPI.DataAccess;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Services
{
    public class LaborRolesService : ILaborRolesService
    {
        APILayer _apiLayer;
        MongoDBDataHelper _mdbHelper;
        IConfiguration _iconfiguration;
        public LaborRolesService(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            _apiLayer = new APILayer(_iconfiguration);
            _mdbHelper = new MongoDBDataHelper(_iconfiguration);
        }

        public LaborRoles GetLaborRoleByRoleId(string sessionId, long laborRoleId)
        {
            dynamic resultData = _apiLayer.GetLaborRoleByRoleIddDataFromAPI(sessionId, laborRoleId);
            return LaborRolesData(resultData);

        }

        public LaborRoles GetLaborRoleByRoleName(string sessionId, string laborRoleName)
        {
            dynamic resultData = _apiLayer.GetLaborRoleByRoleNamedDataFromAPI(sessionId, laborRoleName);
           return LaborRolesData(resultData);
        }

        private LaborRoles LaborRolesData(dynamic resultData)
        {
            LaborRoles lstLaborRoles = new LaborRoles();
            try
            {
                if (resultData != null)
                {
                    var labrRoles = new LaborRoles
                    {
                        Id = resultData.id,
                        Name = resultData.name
                    };
                    lstLaborRoles = labrRoles;
                }
                return lstLaborRoles;
            }
            catch (Exception ex)
            {
                return lstLaborRoles;
            }
        }
    }
}
