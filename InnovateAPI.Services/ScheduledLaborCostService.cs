﻿using InnovateAPI.Business;
using InnovateAPI.DataAccess;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace InnovateAPI.Services
{
    public class ScheduledLaborCostService : IScheduledLaborCostService
    {
        APILayer _apiLayer;
        MongoDBDataHelper _mdbHelper;
        IConfiguration _iconfiguration;
        public ScheduledLaborCostService(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            _apiLayer = new APILayer(_iconfiguration);
            _mdbHelper = new MongoDBDataHelper(_iconfiguration);
        }

        public ScheduledLaborCost GetScheduledLaborCostByEmployeeId(string sessionId, long siteId, long employeeId, string businessDate, string frequency)
        {
            dynamic resultData = _apiLayer.GetScheduledLaborCostByEmployeeIdDataFromAPI(sessionId, siteId, employeeId, businessDate, frequency);

            return ScheduleLaborCostData(resultData);

        }

        public ScheduledLaborCost GetScheduledLaborCostBySiteId(string sessionId, long siteId, string businessDate, string frequency)
        {
            dynamic resultData = _apiLayer.GetScheduledLaborCostBySiteIdDataFromAPI(sessionId, siteId, businessDate, frequency);
            return ScheduleLaborCostData(resultData);
        }

        private ScheduledLaborCost ScheduleLaborCostData(dynamic resultData)
        {
            ScheduledLaborCost lstScheduledLaborCost = new ScheduledLaborCost();
            try
            {
                if (resultData != null)
                {
                    List<ScheduledLaborCostByDay> lstScheduledLaborCostByDay = new List<ScheduledLaborCostByDay>();
                    List<ScheduledLaborCostByHour> lstScheduledLaborCostByHour = new List<ScheduledLaborCostByHour>();
                    foreach (var ScheduledLaborCostByDay in resultData.scheduledLaborCostByDay)
                    {
                        var scheduledLaborCostByDay = new ScheduledLaborCostByDay
                        {
                            SiteId = ScheduledLaborCostByDay.siteId,
                            EmployeeId = ScheduledLaborCostByDay.EmployeeId,
                            BusinessDate = ScheduledLaborCostByDay.businessDat,
                            ScheduledLaborCost = ScheduledLaborCostByDay.scheduledLaborCost
                        };
                        lstScheduledLaborCostByDay.Add(scheduledLaborCostByDay);
                    }
                    foreach (var ScheduledLaborCostByHour in resultData.scheduledLaborCostByHour)
                    {
                        var scheduledLaborCostByHour = new ScheduledLaborCostByHour
                        {
                            SiteId = ScheduledLaborCostByHour.siteId,
                            EmployeeId = ScheduledLaborCostByHour.EmployeeId,
                            BusinessDate = ScheduledLaborCostByHour.businessDat,
                            StartTime = ScheduledLaborCostByHour.etartTime,
                            EndTime = ScheduledLaborCostByHour.endTime,
                            ScheduledLaborCost = ScheduledLaborCostByHour.scheduledLaborCost
                        };
                        lstScheduledLaborCostByHour.Add(scheduledLaborCostByHour);
                    }

                    var scheduledLaborCost = new ScheduledLaborCost
                    {
                        SiteId = resultData.siteId,
                        EmployeeId = resultData.employeeId,
                        BusinessDate = resultData.businessDate,
                        FrequencyCode = resultData.frequencyCode,
                        ScheduledLaborCostByDay = lstScheduledLaborCostByDay,
                        ScheduledLaborCostByHour = lstScheduledLaborCostByHour
                    };
                    lstScheduledLaborCost = scheduledLaborCost;
                }
                return lstScheduledLaborCost;
            }
            catch (Exception ex)
            {
                return lstScheduledLaborCost;
            }
        }
    }
}
