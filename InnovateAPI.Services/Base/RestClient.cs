﻿using InnovateAPI.Models.Common;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace InnovateAPI.Services
{
    public static class RestClient<T>
    {
        static IConfiguration _iconfiguration;
        public static async Task<string> PostLoginAsync(string userName,string password)
        {
            string BaseUrl = _iconfiguration["ServerUrl"];
            userName = "MMAdmin";
            password = "Admin#2";
            CookieContainer cookies = new CookieContainer();
            HttpClientHandler handler = new HttpClientHandler();
            handler.CookieContainer = cookies;

            using (var httpClient = new HttpClient(handler))
            {
                string REFSSessionID = string.Empty;
                var formContent = new FormUrlEncodedContent(new[]
                    {
                     new KeyValuePair<string, string>("loginName", userName),
                     new KeyValuePair<string, string>("password",password),
                    });

                HttpResponseMessage responseMessage = await httpClient.PostAsync(BaseUrl + Constants.LoginAPI, formContent).ConfigureAwait(false);

                if (responseMessage.IsSuccessStatusCode)
                {
                    Uri uri = new Uri(BaseUrl + "/retail/data/login");
                    IEnumerable<Cookie> responseCookies = cookies.GetCookies(uri).Cast<Cookie>();
                    foreach (Cookie cookie in responseCookies)
                    {
                        REFSSessionID = cookie.Value;
                    }

                };

                return REFSSessionID;
            }
        }

    }
}
