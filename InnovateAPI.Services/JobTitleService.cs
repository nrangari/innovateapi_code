﻿using InnovateAPI.Business;
using InnovateAPI.DataAccess;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Services
{
    public class JobTitleService : IJobTitleService
    {
        APILayer _apiLayer;
        MongoDBDataHelper _mdbHelper;
        IConfiguration _iconfiguration;
        public JobTitleService(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            _apiLayer = new APILayer(_iconfiguration);
            _mdbHelper = new MongoDBDataHelper(_iconfiguration);
        }

        public List<JobTitle> GetJobTitleData(string refSessionId)
        {
            dynamic resultData = _apiLayer.GetJobTitleDataFromAPI(refSessionId);
            List<JobTitle> jobTitle = new List<JobTitle>();
            try
            {

                if (resultData != null)
                {
                    foreach (var orglevel in resultData.entities)
                    {
                        var jobs = new JobTitle
                        {
                            jobTitleId = orglevel.JobTitleId.Value,
                            name = orglevel.name.Value

                        };

                        jobTitle.Add(jobs);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogError(ex);
            }
            return jobTitle;
        }

        public List<JobTitle> GetJobTitleDataByTitleId(string refSessionId, long titleId)
        {
            dynamic resultData = _apiLayer.GetJobTitleDataByTitleIdFromAPI(refSessionId, titleId);
            List<JobTitle> jobTitleById = new List<JobTitle>();
            try
            {
                if (resultData != null)
                {
                    var jobs = new JobTitle
                    {
                        jobTitleId = resultData.JobTitleId,
                        name = resultData.name
                    };

                    jobTitleById.Add(jobs);

                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogError(ex);
            }
            return jobTitleById;
        }

        public List<JobTitle> GetJobTitleDataByName(string refSessionId, string name)
        {
            dynamic resultData = _apiLayer.GetJobTitleDataByNameFromAPI(refSessionId, name);
            List<JobTitle> jobTitleByName = new List<JobTitle>();
            try
            {
                if (resultData != null)
                {

                    var jobs = new JobTitle
                    {
                        jobTitleId = resultData.JobTitleId,
                        name = resultData.name
                    };

                    jobTitleByName.Add(jobs);

                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogError(ex);
            }
            return jobTitleByName;
        }

        public List<JobTitle> GetJobTitleDataBySiteId(string refSessionId, long siteId)
        {
            dynamic resultData = _apiLayer.GetJobTitleDataBySiteIdFromAPI(refSessionId, siteId);
            List<JobTitle> jobTitleBySiteId = new List<JobTitle>();
            try
            {
                if (resultData != null)
                {
                    var jobs = new JobTitle
                    {
                        siteId = resultData.siteId,
                        //jobTitleId = resultData.jobTitleId,
                        //name = resultData.name
                    };

                    jobTitleBySiteId.Add(jobs);

                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogError(ex);
            }
            return jobTitleBySiteId;
        }
    }
}
