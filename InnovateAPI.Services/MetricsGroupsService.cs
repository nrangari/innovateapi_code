﻿using InnovateAPI.Business;
using InnovateAPI.DataAccess;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Services
{
    public class MetricsGroupsService : IMetricsGroupsService
    {
        APILayer _apiLayer;
        MongoDBDataHelper _mdbHelper;
        IConfiguration _iconfiguration;
        public MetricsGroupsService(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            _apiLayer = new APILayer(_iconfiguration);
            _mdbHelper = new MongoDBDataHelper(_iconfiguration);
        }
        public List<MetricsGroups> MetricsGroups(string refSessionId ,long metricsGroupsId)
        {
            dynamic resultData = _apiLayer.GetMetricsGroupsDataFromAPI(refSessionId, metricsGroupsId);
            List<MetricsGroups> lstmetricsGroups = new List<MetricsGroups>();
            try
            {
                if (resultData != null)
                {
                    var metricsGroup = new MetricsGroups()
                    {
                        Id = resultData.id,
                        Name = resultData.name,
                    };

                    lstmetricsGroups.Add(metricsGroup);
                }
            }
            catch (Exception ex)
            {
                 ExceptionLogger.LogError(ex);
            }
           
            return lstmetricsGroups;
        }
    }
}
