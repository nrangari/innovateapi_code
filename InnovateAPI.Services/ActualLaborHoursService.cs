﻿using InnovateAPI.Business;
using InnovateAPI.DataAccess;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Services
{
    public class ActualLaborHoursService : IActualLaborHoursService
    {
        APILayer _apiLayer;
        MongoDBDataHelper _mdbHelper;
        IConfiguration _iconfiguration;
        public ActualLaborHoursService(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            _apiLayer = new APILayer(_iconfiguration);
            _mdbHelper = new MongoDBDataHelper(_iconfiguration);
        }

        public ActualLaborHours GetActualLaborHoursByEmployeeId(string refSessionId, long siteId, long employeeId, string businessDate, string frequency)
        {
            dynamic resultData = _apiLayer.GetActualLaborHoursByEmployeeIdDataFromAPI(refSessionId, siteId, employeeId, businessDate, frequency);

            return GetActualLaborHoursdata(resultData);


        }

        public ActualLaborHours GetActualLaborHoursByJobId(string refSessionId, long siteId, long jobId, string businessDate)
        {
            dynamic resultData = _apiLayer.GetActualLaborHoursByJobIdDataFromAPI(refSessionId, siteId, jobId, businessDate);
            return GetActualLaborHoursdata(resultData);
        }

        public ActualLaborHours GetActualLaborHoursBySiteId(string refSessionId, long siteId, string businessDate)
        {
            dynamic resultData = _apiLayer.GetActualLaborHoursBySiteIdDataFromAPI(refSessionId, siteId, businessDate);
            return GetActualLaborHoursdata(resultData);
        }

        public ActualLaborHours GetActualLaborHoursByWorkgroupId(string refSessionId, long siteId, long workgroupId, string businessDate, string frequency)
        {
            dynamic resultData = _apiLayer.GetActualLaborHoursByWorkgroupIdDataFromAPI(refSessionId, siteId, workgroupId, businessDate, frequency);
            return GetActualLaborHoursdata(resultData);
        }


        public ActualLaborHours GetActualLaborHoursdata(dynamic resultData)
        {
            ActualLaborHours lstActualLaborHours = new ActualLaborHours();
            try
            {


                if (resultData != null)
                {
                    List<ActualLaborHourByDay> lstActualLaborHourByDay = new List<ActualLaborHourByDay>();
                    List<ActualLaborHourByHour> lstActualLaborHourByHour = new List<ActualLaborHourByHour>();
                    foreach (var actualLaborDay in resultData.actualLaborHourByDay)
                    {
                        var actualLaborHourByDay = new ActualLaborHourByDay
                        {
                            JobId = actualLaborDay.jobId,
                            JobTitleId = actualLaborDay.jobTitleId,
                            SiteId = actualLaborDay.siteId,
                            EmployeeId = actualLaborDay.employeeId,
                            WorkgroupId = actualLaborDay.workgroupId,
                            BusinessDate = actualLaborDay.businessDate,
                            ActualLaborHours = actualLaborDay.actualLaborHours
                        };
                        lstActualLaborHourByDay.Add(actualLaborHourByDay);


                    }
                    foreach (var actualLaborHours in resultData.actualLaborHourByDay)
                    {
                        var actualLaborHourByHours = new ActualLaborHourByHour
                        {
                            JobId = actualLaborHours.jobId,
                            JobTitleId = actualLaborHours.jobTitleId,
                            SiteId = actualLaborHours.siteId,
                            EmployeeId = actualLaborHours.employeeId,
                            WorkgroupId = actualLaborHours.workgroupId,
                            BusinessDate = actualLaborHours.businessDate,
                            ActualLaborHours = actualLaborHours.actualLaborHours,
                            StartTime = actualLaborHours.startTime,
                            EndTime = actualLaborHours.endTime
                        };
                        lstActualLaborHourByHour.Add(actualLaborHourByHours);
                    }
                    var ActualLaborHours = new ActualLaborHours
                    {
                        JobId = resultData.jobId,
                        SiteId = resultData.siteId,
                        WorkgroupId = resultData.workgroupId,
                        EmployeeId = resultData.employeeId,
                        JobTitleId = resultData.jobTitleId,
                        BusinessDate = resultData.businessDate,
                        FrequencyCode = resultData.frequencyCode,
                        ActualLaborHourByDay = lstActualLaborHourByDay,
                        ActualLaborHourByHour = lstActualLaborHourByHour
                    };
                    lstActualLaborHours = ActualLaborHours;
                }
                return lstActualLaborHours;
            }
            catch (Exception ex)
            {
                return lstActualLaborHours;
            }
        }
    }
}
