﻿using InnovateAPI.Business;
using InnovateAPI.DataAccess;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Services
{
    public class OrganizationalHierarchyLevelService : IOrganizationalHierarchyLevelService
    {
        APILayer _apiLayer;
        MongoDBDataHelper _mdbHelper;
        IConfiguration _iconfiguration;
        public OrganizationalHierarchyLevelService(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            _apiLayer = new APILayer(_iconfiguration);
            _mdbHelper = new MongoDBDataHelper(_iconfiguration);
        }
        public List<OrganizationalHierarchyLevels> GetOrganizationalHierarchyLevels(string refSessionId)
        {
            dynamic resultData = _apiLayer.GetOrgHierarchyLevelsDataFromAPI(refSessionId);
            List<OrganizationalHierarchyLevels> lstOrganizationalLevels = new List<OrganizationalHierarchyLevels>();
            try
            {
                if (resultData != null)
                {
                    foreach (var orglevel in resultData.entities)
                    {
                        var organizationalLevels = new OrganizationalHierarchyLevels
                        {
                            Id = orglevel.id.Value,
                            Name = orglevel.name.Value
                        };

                        lstOrganizationalLevels.Add(organizationalLevels);
                    }
                }
            }
            catch(Exception ex)
            {
                ExceptionLogger.LogError(ex);
            }
            return lstOrganizationalLevels;
        }
    }
}

