﻿using InnovateAPI.Business;
using InnovateAPI.DataAccess;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Services
{
    public class SitesService : ISitesService
    {
        APILayer _apiLayer;
        MongoDBDataHelper _mdbHelper;
        IConfiguration _iconfiguration;
        public SitesService(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            _apiLayer = new APILayer(_iconfiguration);
            _mdbHelper = new MongoDBDataHelper(_iconfiguration);
        }

        public SiteOrganizationalUnit GetSiteByOrganizationalUnitId(string refSessionId, long organizationUnitId)
        {
            dynamic resultData = _apiLayer.GetSitesByOrganizationalUnitIdDataFromAPI(refSessionId, organizationUnitId);
            SiteOrganizationalUnit siteOrganizationalUnit = new SiteOrganizationalUnit();
            List<SiteIds> SiteIds = new List<SiteIds>();
            if (resultData != null)
            {
                foreach (var ids in resultData.entities)
                {
                    var siteIds = new SiteIds
                    {
                        SiteId = ids.siteId
                    };
                    SiteIds.Add(siteIds);
                }
                siteOrganizationalUnit.OrganizationalUnitId = resultData.organizationalUnitId;
                siteOrganizationalUnit.SiteIds = SiteIds;
            }
            return siteOrganizationalUnit;
        }

        public SiteCurrentTimeInformation GetSiteCurrentTimeInformation(string refSessionId, long siteId)
        {
            dynamic resultData = _apiLayer.GetSiteByCurrentTimeInformationDataFromAPI(refSessionId, siteId);

            SiteCurrentTimeInformation siteCurrentTimeInformation = new SiteCurrentTimeInformation();
            try
            {
                if (resultData != null)
                {

                    siteCurrentTimeInformation.SiteId = resultData.siteId;
                    siteCurrentTimeInformation.CurrentBusinessDate = resultData.currentBusinessDate;
                    siteCurrentTimeInformation.CurrentBusinessDayEndTime = resultData.currentBusinessDayEndTime;
                    siteCurrentTimeInformation.CurrentBusinessDayStartTime = resultData.currentBusinessDayStartTime;
                    siteCurrentTimeInformation.CurrentSiteLocalTimestamp = resultData.currentSiteLocalTimestamp;
                    siteCurrentTimeInformation.LaborDayStartTime = resultData.laborDayStartTime;
                    siteCurrentTimeInformation.LaborWeekEndDateTime = resultData.laborWeekEndDateTime;
                    siteCurrentTimeInformation.LaborWeekStartDateTime = resultData.laborWeekStartDateTime;
                    siteCurrentTimeInformation.PayPeriodEndDateTime = resultData.payPeriodEndDateTime;
                    siteCurrentTimeInformation.PayPeriodStartDateTime = resultData.payPeriodStartDateTime;


                }


                return siteCurrentTimeInformation;
            }
            catch (Exception ex)
            {
                return siteCurrentTimeInformation;
            }

        }

        public List<Sites> GetSites(string refSessionId)
        {
            dynamic resultData = _apiLayer.GetSitesDataFromAPI(refSessionId);
            List<Sites> lstSites = new List<Sites>();
            if (resultData != null)
            {
                foreach (var Sites in resultData.entities)
                {
                    List<OperatingHoursSiteAssignments> lstOperatingHoursSiteAssignments = new List<OperatingHoursSiteAssignments>();
                    foreach (var OperatingHoursSiteAssignments in Sites.operatingHoursSiteAssignments)
                    {
                        var operatingHoursSiteAssignments = new OperatingHoursSiteAssignments
                        {
                            Id = OperatingHoursSiteAssignments.id,
                            SiteId = OperatingHoursSiteAssignments.siteId,
                            Start = OperatingHoursSiteAssignments.start,
                            OperatingHoursId = OperatingHoursSiteAssignments.operatingHoursId,
                            OperatingHours = OperatingHoursSiteAssignments.operatingHours,
                            Self = OperatingHoursSiteAssignments.self,
                        };
                        lstOperatingHoursSiteAssignments.Add(operatingHoursSiteAssignments);
                    }

                    var sites = new Sites
                    {
                        Id = Sites.id,
                        Name = Sites.name,
                        LongName = Sites.longName,
                        Status = Sites.status,
                        OpenedDate = Sites.openedDate,
                        MailingAddress = Sites.mailingAddress,
                        OperatingHoursSiteAssignments = lstOperatingHoursSiteAssignments,
                        ParentOrganizationalUnitAssignment = Sites.parentOrganizationalUnitAssignment,
                        ParentOrganizationalUnitAssignmentId = Sites.parentOrganizationalUnitAssignmentId,
                        TimeZoneAssignment = Sites.timeZoneAssignment,
                        TimeZoneAssignmentId = Sites.timeZoneAssignmentId
                    };
                    lstSites.Add(sites);
                }
                
            }
            return lstSites;
        }

        public List<Sites> GetSitesById(string refSessionId, long siteId)
        {

            dynamic resultData = _apiLayer.GetSitesByIdDataFromAPI(refSessionId, siteId);
            return GetSitesData(resultData);

        }



        public List<Sites> GetSitesByMultipleId(string refSessionId, long[] siteId)
        {

            dynamic resultData = _apiLayer.GetSitesByMultipleIdDataFromAPI(refSessionId, siteId);
            List<Sites> lstSities = new List<Sites>();

            try
            {
                if (resultData != null)
                {
                    foreach (var sit in resultData)
                    {
                        List<OperatingHoursSiteAssignments> lstOperatingHoursSiteAssignments = new List<OperatingHoursSiteAssignments>();
                        // List<Actions> lstAction = new List<Actions>();

                        foreach (var OperatingHoursSiteAssignments in sit.operatingHoursSiteAssignments)
                        {
                            var operatingHoursSiteAssignments = new OperatingHoursSiteAssignments
                            {
                                Id = OperatingHoursSiteAssignments.id,
                                SiteId = OperatingHoursSiteAssignments.siteId,
                                Start = OperatingHoursSiteAssignments.start,
                                OperatingHoursId = OperatingHoursSiteAssignments.operatingHoursId,
                                OperatingHours = OperatingHoursSiteAssignments.operatingHours,
                                Self = OperatingHoursSiteAssignments.self,

                            };
                            lstOperatingHoursSiteAssignments.Add(operatingHoursSiteAssignments);
                        }

                        //foreach (var Actions in resultData.actions)
                        //{
                        //    var actions = new Actions
                        //    {
                        //        Rel = Actions.rel,
                        //        Method = Actions.siteId,
                        //        Uri = Actions.uri

                        //    };
                        //    lstAction.Add(actions);
                        //}


                        var sites = new Sites
                        {
                            Id = sit.id,
                            Name = sit.name,
                            LongName = sit.longName,
                            Status = sit.status,
                            OpenedDate = sit.openedDate,
                            TimeZoneAssignmentId = sit.timeZoneAssignmentId,
                            TimeZoneAssignment = sit.timeZoneAssignment,
                            MailingAddress = sit.mailingAddress,
                            ParentOrganizationalUnitAssignmentId = sit.parentOrganizationalUnitAssignmentId,
                            ParentOrganizationalUnitAssignment = sit.parentOrganizationalUnitAssignment,
                            OperatingHoursSiteAssignments = lstOperatingHoursSiteAssignments
                            // Actions = lstAction
                        };

                        lstSities.Add(sites);
                    }
                }


                return lstSities;
            }
            catch (Exception ex)
            {
                return lstSities;
            }

        }

        public List<Sites> GetSitesByName(string refSessionId, string siteName)
        {
            dynamic resultData = _apiLayer.GetSitesByNameDataFromAPI(refSessionId, siteName);
            return GetSitesData(resultData);
        }

        private List<Sites> GetSitesData(dynamic resultData)
        {
            List<Sites> lstSities = new List<Sites>();

            try
            {
                if (resultData != null)
                {
                    List<OperatingHoursSiteAssignments> lstOperatingHoursSiteAssignments = new List<OperatingHoursSiteAssignments>();
                    if (resultData.operatingHoursSiteAssignments != null)
                    {
                        foreach (var OperatingHoursSiteAssignments in resultData.operatingHoursSiteAssignments)
                        {
                            var operatingHoursSiteAssignments = new OperatingHoursSiteAssignments
                            {
                                Id = OperatingHoursSiteAssignments.id,
                                SiteId = OperatingHoursSiteAssignments.siteId,
                                Start = OperatingHoursSiteAssignments.start,
                                OperatingHoursId = OperatingHoursSiteAssignments.operatingHoursId,
                                OperatingHours = OperatingHoursSiteAssignments.operatingHours,
                                Self = OperatingHoursSiteAssignments.self,

                            };
                            lstOperatingHoursSiteAssignments.Add(operatingHoursSiteAssignments);
                        }
                    }

                    //foreach (var Actions in resultData.actions)
                    //{
                    //    //var actions = new Actions
                    //    //{
                    //    //    Rel = Actions.rel,
                    //    //    Method = Actions.siteId,
                    //    //    Uri = Actions.uri

                    //    //};
                    //    //lstAction.Add(actions);
                    //}

                    if (lstOperatingHoursSiteAssignments.Count > 0)
                    {
                        var sites = new Sites
                        {
                            Id = resultData.id,
                            Name = resultData.name,
                            LongName = resultData.longName,
                            Status = resultData.status,
                            OpenedDate = resultData.openedDate,
                            TimeZoneAssignmentId = resultData.timeZoneAssignmentId,
                            TimeZoneAssignment = resultData.timeZoneAssignment,
                            MailingAddress = resultData.mailingAddress,
                            ParentOrganizationalUnitAssignmentId = resultData.parentOrganizationalUnitAssignmentId,
                            ParentOrganizationalUnitAssignment = resultData.parentOrganizationalUnitAssignment,
                            OperatingHoursSiteAssignments = lstOperatingHoursSiteAssignments,
                            // Actions = lstAction
                        };

                        lstSities.Add(sites);
                        // }
                    }
                }


                return lstSities;
            }
            catch (Exception ex)
            {
                return lstSities;
            }
        }
    }
}
