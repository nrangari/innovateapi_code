﻿using InnovateAPI.Business;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using InnovateAPI.Models.Common;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace InnovateAPI.Services
{
    public class LoginUserService : ILoginUserService
    {
        public string RefSessionId { get; set; }

        private ResponseResult _response;
        IConfiguration _iconfiguration;
        APILayer _apiLayer;
        public LoginUserService(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            _apiLayer = new APILayer(_iconfiguration);
        }
        public ResponseResult LoginUser(string userName, string password)
        {            
            _response = ValidateUser(userName,password);
            return _response;
        }

        public static async Task<string> GetREFSSessionID(string userName, string password)
        {
            Task<string> REFSSessionID = RestClient<string>.PostLoginAsync(userName, password);
            return await REFSSessionID;
        }
        private ResponseResult ValidateUser(string userName, string password)
        {
            _response = new ResponseResult();
            try
            {
                //var request = WebRequest.Create(string.Concat(_iconfiguration["ServerUrl"], Constants.LoginAPI)) as HttpWebRequest;
                //request.CookieContainer = new CookieContainer();
                //request.Method = "POST";
                //string postData = string.Concat("loginName=", userName, "&password=", password);
                //byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                //request.ContentType = "application/x-www-form-urlencoded";
                //request.ContentLength = byteArray.Length;
                //Stream dataStream = request.GetRequestStream();
                //dataStream.Write(byteArray, 0, byteArray.Length);
                //dataStream.Close();
                //var response = request.GetResponse() as HttpWebResponse;
                var response = _apiLayer.GetLoginResponse(userName, password);
                string siteName = string.Empty;
                if (response != null)
                {
                    RefSessionId = response.Cookies["REFSSessionID"].Value.ToString();
                    Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                    //dataStream = response.GetResponseStream();
                    //StreamReader reader = new StreamReader(dataStream);
                    //string responseFromServer = reader.ReadToEnd();
                    //if (!string.IsNullOrEmpty(responseFromServer))
                    //{
                    //    //dynamic values = JObject.Parse(responseFromServer);
                    //    //siteName = values.data.siteId;
                    //}
                    //Console.WriteLine(responseFromServer);
                    //reader.Close();
                   // dataStream.Close();
                    response.Close();
                }

                if (!string.IsNullOrEmpty(RefSessionId))
                {
                    _response.REFSessionId = RefSessionId;
                    _response.returnStatus = "Success";
                    _response.HttpStatusCode = 200;
                }
                else
                {
                    _response.returnStatus = "Fail";
                    _response.HttpStatusCode = 500;
                }
                //throw new NullReferenceException("Student object is null.");
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogError(ex);
            }

            return _response;
        }

    }
}
