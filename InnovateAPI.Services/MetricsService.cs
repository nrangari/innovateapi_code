﻿using InnovateAPI.Business;
using InnovateAPI.DataAccess;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Services
{
    public class MetricsService : IMetricsService
    {
        APILayer _apiLayer;
        MongoDBDataHelper _mdbHelper;
        IConfiguration _iconfiguration;
        public MetricsService(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            _apiLayer = new APILayer(_iconfiguration);
            _mdbHelper = new MongoDBDataHelper(_iconfiguration);
        }
        public List<Metrics> GetMetrics(string refSessionId, long metricsId)
        {
            dynamic resultData = _apiLayer.GetMetricsDataFromAPI(refSessionId, metricsId);
            List<Metrics> lstmetrics = new List<Metrics>();
            try
            {
                if (resultData != null)
                {
                    var metrics = new Metrics()
                    {
                        Id = resultData.id,
                        Name = resultData.name,
                        ExternalName = resultData.externalName
                    };

                    lstmetrics.Add(metrics);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogError(ex);
            }
            
            return lstmetrics;
        }
    }
}
