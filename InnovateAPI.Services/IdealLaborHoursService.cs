﻿using InnovateAPI.Business;
using InnovateAPI.DataAccess;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Services
{
    public class IdealLaborHoursService : IIdealLaborHoursService
    {
        APILayer _apiLayer;
        MongoDBDataHelper _mdbHelper;
        IConfiguration _iconfiguration;

        public IdealLaborHoursService(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            _apiLayer = new APILayer(_iconfiguration);
            _mdbHelper = new MongoDBDataHelper(_iconfiguration);
        }

        public List<IdealLaborHours> GetidealLaborHoursByBusinessDate(string refSessionId, long siteId, string businessDate, string frequency)
        {
            dynamic resultData = _apiLayer.GetidealLaborHoursByBusinessDateFromAPI(refSessionId, siteId, businessDate, frequency);

            string datas = resultData.businessDate;

            List<IdealLaborHours> lstIdealLaborHours = new List<IdealLaborHours>();
            try
            {
                if (resultData != null)
                {

                    List<IdealLaborHoursByQuarterHour> lstIdealLaborHoursByQuarterHour = new List<IdealLaborHoursByQuarterHour>();

                    foreach (var IdealLaborQuarterHour in resultData.idealLaborHoursByQuarterHour)
                    {
                        var idealLaborQuarterHours = new IdealLaborHoursByQuarterHour
                        {
                            SiteId = IdealLaborQuarterHour.siteId,
                            WorkgroupId = IdealLaborQuarterHour.workgroupId,
                            LaborRoleId = IdealLaborQuarterHour.laborRoleId,
                            BusinessDate = IdealLaborQuarterHour.businessDate,
                            StartTime = IdealLaborQuarterHour.startTime,
                            EndTime = IdealLaborQuarterHour.endTime,
                            IdealLaborHours = IdealLaborQuarterHour.idealLaborHours
                        };
                        lstIdealLaborHoursByQuarterHour.Add(idealLaborQuarterHours);

                    }
                    List<IdealLaborHoursByDay> lstIdealLaborHoursByDay = new List<IdealLaborHoursByDay>();
                    foreach (var idealLaborQuaterDay in resultData.actualLaborCostByDay)
                    {
                        var actualLaborDay = new IdealLaborHoursByDay
                        {
                            SiteId = idealLaborQuaterDay.siteId,
                            WorkgroupId = idealLaborQuaterDay.workgroupId,
                            LaborRoleId = idealLaborQuaterDay.laborRoleId,
                            BusinessDate = idealLaborQuaterDay.businessDate,
                            IdealLaborHours = idealLaborQuaterDay.idealLaborHours
                        };
                        lstIdealLaborHoursByDay.Add(actualLaborDay);

                    }


                    var idealLaborHours = new IdealLaborHours
                    {
                        SiteId = resultData.siteId,
                        BusinessDate = resultData.businessDate,
                        FrequencyCode = resultData.frequencyCode,
                        IdealLaborHoursByDay = lstIdealLaborHoursByDay,
                        IdealLaborHoursByQuarterHour = lstIdealLaborHoursByQuarterHour
                    };
                    lstIdealLaborHours.Add(idealLaborHours);
                    //}

                }

            }
            catch (Exception ex)
            {
                ExceptionLogger.LogError(ex);
            }
            return lstIdealLaborHours;
        }

        public List<IdealLaborHours> GetidealLaborHoursByWorkGroupId(string refSessionId, long siteId, long workgroupId, string businessDate, string frequency)
        {
            dynamic resultData = _apiLayer.GetidealLaborHoursByWorkGroupIdFromAPI(refSessionId, siteId, workgroupId, businessDate, frequency);

            string datas = resultData.businessDate;

            List<IdealLaborHours> lstIdealLaborHours = new List<IdealLaborHours>();
            try
            {
                if (resultData != null)
                {
                    List<IdealLaborHoursByQuarterHour> lstIdealLaborHoursByQuarterHour = new List<IdealLaborHoursByQuarterHour>();
                    try
                    {


                        foreach (var IdealLaborQuarterHour in resultData.idealLaborHoursByQuarterHour)
                        {
                            var idealLaborQuarterHours = new IdealLaborHoursByQuarterHour
                            {
                                SiteId = IdealLaborQuarterHour.siteId,
                                WorkgroupId = IdealLaborQuarterHour.workgroupId,
                                LaborRoleId = IdealLaborQuarterHour.laborRoleId,
                                BusinessDate = IdealLaborQuarterHour.businessDate,
                                StartTime = IdealLaborQuarterHour.startTime,
                                EndTime = IdealLaborQuarterHour.endTime,
                                IdealLaborHours = IdealLaborQuarterHour.idealLaborHours
                            };
                            lstIdealLaborHoursByQuarterHour.Add(idealLaborQuarterHours);

                        }
                    }
                    catch (Exception ex)
                    {
                        ExceptionLogger.LogError(ex);

                    }
                    List<IdealLaborHoursByDay> lstIdealLaborHoursByDay = new List<IdealLaborHoursByDay>();

                    try
                    {


                        foreach (var idealLaborQuaterDay in resultData.actualLaborCostByDay)
                        {
                            var actualLaborDay = new IdealLaborHoursByDay
                            {
                                SiteId = idealLaborQuaterDay.siteId,
                                WorkgroupId = idealLaborQuaterDay.workgroupId,
                                LaborRoleId = idealLaborQuaterDay.laborRoleId,
                                BusinessDate = idealLaborQuaterDay.businessDate,
                                IdealLaborHours = idealLaborQuaterDay.idealLaborHours
                            };
                            lstIdealLaborHoursByDay.Add(actualLaborDay);

                        }
                    }
                    catch (Exception ex)
                    {

                        ExceptionLogger.LogError(ex);
                    }
                    var idealLaborHours = new IdealLaborHours
                    {
                        SiteId = resultData.siteId,
                        BusinessDate = resultData.businessDate,
                        FrequencyCode = resultData.frequencyCode,
                        IdealLaborHoursByDay = lstIdealLaborHoursByDay,
                        IdealLaborHoursByQuarterHour = lstIdealLaborHoursByQuarterHour
                    };
                    lstIdealLaborHours.Add(idealLaborHours);
                    //}

                }

            }
            catch (Exception ex)
            {
                ExceptionLogger.LogError(ex);
            }
            return lstIdealLaborHours;
        }
    }
}
