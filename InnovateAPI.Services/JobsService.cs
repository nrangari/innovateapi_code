﻿using InnovateAPI.Business;
using InnovateAPI.DataAccess;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Services
{
    public class JobsService : IJobsService
    {
        APILayer _apiLayer;
        MongoDBDataHelper _mdbHelper;
        IConfiguration _iconfiguration;
        public JobsService(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            _apiLayer = new APILayer(_iconfiguration);
            _mdbHelper = new MongoDBDataHelper(_iconfiguration);
        }

        public List<Jobs> GetJobsById(string refSessionId, long jobId)
        {
            dynamic resultData = _apiLayer.GetJobsDataByIdFromAPI(refSessionId, jobId);
            List<Jobs> jobsById = new List<Jobs>();
            try
            {
                if (resultData != null)
                {
                    var jobs = new Jobs
                    {
                        jobId = resultData.jobId,
                        name = resultData.name,
                        jobCode = resultData.jobCode,
                        departmentId = resultData.departmentId
                    };
                    jobsById.Add(jobs);
                }
                //From Here API retrived Data will be stored into the MongoDB
                //_mdbHelper.SaveOrgUnitsData(lstOrganizationalUnits);
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogError(ex);
            }
            return jobsById;
        }

        public List<Jobs> GetJobsByName(string refSessionId, string name)
        {
            dynamic resultData = _apiLayer.GetJobsDataByNameFromAPI(refSessionId, name);
            List<Jobs> jobsByName = new List<Jobs>();
            try
            {

                if (resultData != null)
                {

                    var jobs = new Jobs
                    {
                        jobId = resultData.jobId,
                        name = resultData.name,
                        jobCode = resultData.jobCode,
                        departmentId = resultData.departmentId
                    };

                    jobsByName.Add(jobs);

                }
                //From Here API retrived Data will be stored into the MongoDB
                //_mdbHelper.SaveOrgUnitsData(lstOrganizationalUnits);
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogError(ex);
            }
            return jobsByName;
        }

        public List<Jobs> GetJobsBySiteId(string refSessionId, long siteId)
        {
            dynamic resultData = _apiLayer.GetJobsDataBySiteIdFromAPI(refSessionId, siteId);
            List<Jobs> jobsBySiteId = new List<Jobs>();
            try
            {
                if (resultData != null)
                {

                    foreach (var orglevel in resultData.entities)
                    {
                        var jobs = new Jobs
                        {
                            
                            jobId = orglevel.jobId.Value,
                            name = orglevel.name.Value,
                            jobCode = orglevel.jobCode.Value,
                            departmentId = orglevel.departmentId.Value
                        };

                        jobsBySiteId.Add(jobs);
                    }

                }
                //From Here API retrived Data will be stored into the MongoDB
                //_mdbHelper.SaveOrgUnitsData(lstOrganizationalUnits);
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogError(ex);
            }
            return jobsBySiteId;
        }

    }
}
