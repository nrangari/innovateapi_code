﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.ViewModel
{
    public class OrganizationalUnitsVM
    {

        public Data data { get; set; }
        public bool expanded { get; set; }
        public ICollection<OrganizationalUnitsVM> children { get; set; }
        public List<string> Metrics { get; set; }
    }

    public class Data
    {
        public long? id { get; set; }
        public string orgName { get; set; }
        public long? hierarchyLevelId { get; set; }
        public long? parentUnitId { get; set; }
        public long? Week1 { get; set; }
      
        public long? Week2 { get; set; }
        public long? Week3 { get; set; }
        public long? Week4 { get; set; }
        public long? Week5 { get; set; }
        public long? Week6 { get; set; }
        public long? Week7 { get; set; }
        public long? Week8 { get; set; }
        public long? Week9 { get; set; }
        public long? Week10 { get; set; }
        public long? Week11 { get; set; }
        public long? Week12 { get; set; }
        public long? Week13 { get; set; }
    }
}
