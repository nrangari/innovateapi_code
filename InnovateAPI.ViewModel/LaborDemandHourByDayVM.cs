﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.ViewModel
{
    public class LaborDemandHourByDayVM
    {
        public LaborData data { get; set; }

        public bool expanded { get; set; }
        public ICollection<LaborDemandHourByDayVM> children { get; set; }
        // public List<string> LaborDemand { get; set; }
    }

    public class OrgnisationUnit
    {
        public long? id { get; set; }
        public string orgName { get; set; }
        public long? hierarchyLevelId { get; set; }
        public long? parentUnitId { get; set; }
        public string laborHours { get; set; }
        
    }

    public class LaborData
    {
        public long? id { get; set; }
        public string orgName { get; set; }
        public long? hierarchyLevelId { get; set; }
        public long? parentUnitId { get; set; }
        public string laborHours { get; set; }
        public long? Week1 { get; set; }
        public long? Week2 { get; set; }
        public long? Week3 { get; set; }
        public long? Week4 { get; set; }
        public long? Week5 { get; set; }
        public long? Week6 { get; set; }
        public long? Week7 { get; set; }
        public long? Week8 { get; set; }
        public long? Week9 { get; set; }
        public long? Week10 { get; set; }

        public long? Week11 { get; set; }
        public long? Week12 { get; set; }
        public long? Week13 { get; set; }
        public long? Week14 { get; set; }
        public long? Week15 { get; set; }
        public long? Week16 { get; set; }
        public long? Week17 { get; set; }
        public long? Week18 { get; set; }
        public long? Week19 { get; set; }
        public long? Week20 { get; set; }
        public long? Week21 { get; set; }
        public long? Week22 { get; set; }
        public long? Week23 { get; set; }
        public long? Week24 { get; set; }
        public long? Week25 { get; set; }
        public long? Week26 { get; set; }
        public long? Week27 { get; set; }
        public long? Week28 { get; set; }
        public long? Week29 { get; set; }
        public long? Week30 { get; set; }
        public long? Week31 { get; set; }
        public long? Week32 { get; set; }
        public long? Week33 { get; set; }
        public long? Week34 { get; set; }
        public long? Week35 { get; set; }
        public long? Week36 { get; set; }
        public long? Week37 { get; set; }
        public long? Week38 { get; set; }
        public long? Week39 { get; set; }
        public long? Week40 { get; set; }
        public long? Week41 { get; set; }
        public long? Week42 { get; set; }
        public long? Week43 { get; set; }
        public long? Week44 { get; set; }
        public long? Week45 { get; set; }
        public long? Week46 { get; set; }
        public long? Week47 { get; set; }
        public long? Week48 { get; set; }
        public long? Week49 { get; set; }
        public long? Week50 { get; set; }
        public long? Week51 { get; set; }
        public long? Week52 { get; set; }
    }
}
