﻿using EasyCaching.Core;
using InnovateAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InnovateAPI.DataAccess
{
    public class RedisCacheHelper
    {
        private IEasyCachingProvider cachingProvider;
        private IEasyCachingProviderFactory cachingProviderFactory;

        public RedisCacheHelper(IEasyCachingProviderFactory cachingProviderFactor)
        {
            this.cachingProviderFactory = cachingProviderFactor;

            this.cachingProvider = cachingProviderFactor.GetCachingProvider("redis1");
        }



        public string GetCacheResponse(string keyValue)
        {
            string CacheResponse = null;
            try
            {
                if (this.cachingProvider.Get<string>(keyValue).HasValue)
                {
                    CacheResponse = this.cachingProvider.Get<string>(keyValue).Value;

                }
                return CacheResponse;
            }
            catch (Exception ex)
            {
                return CacheResponse;
            }
        }

        public void SetCacheIntoData(string keyValue, string values)
        {
            try
            {
               
                this.cachingProvider.SetAsync(keyValue, values, TimeSpan.FromDays(100));
            }
            catch (Exception ex)
            {

                ex.Message.ToString();
            }
        }


        DateTime fromDate = Convert.ToDateTime("2019/08/05");
        DateTime toDate = Convert.ToDateTime("2019/08/11");
       
        public string GetMetricDataCacheResponse(string keyValue)
        {
            fromDate = Convert.ToDateTime("2019/01/01");
            toDate = Convert.ToDateTime("2019/12/31");

            string CacheResponse = null;
            string CacheResponse1 = null;
            try
            {
                if (this.cachingProvider.Get<string>("MData").HasValue)
                {
                    CacheResponse = this.cachingProvider.Get<string>("Fiscal_Calendar").Value;
                    CacheResponse1 = this.cachingProvider.Get<string>("MData").Value;
                    var resultdata = JsonConvert.DeserializeObject<List<MetricDatas>>(CacheResponse);
                    var resultdata1 = JsonConvert.DeserializeObject<List<MetricCalendar>>(CacheResponse1);

                    var resultsValue = from p in resultdata
                                       from p1 in resultdata1
                                       where p.businessDate.Date >= p1.Start_date.Date && p.businessDate.Date <= p1.End_date.Date
                                       group p by new { p.metricValue } into g
                                       select new WeekMatricData
                                       {
                                           MetricValue = g.Sum(a => a.metricValue)
                                       } ;
                       return JsonConvert.SerializeObject(resultsValue, Formatting.Indented);
                }



                    //if (this.cachingProvider.Get<string>(keyValue).HasValue)
                    //{
                    //    CacheResponse = this.cachingProvider.Get<string>(keyValue).Value;
                    //    if (CacheResponse != null)
                    //    {
                    //        var resultdata = JsonConvert.DeserializeObject<List<MetricDatas>>(CacheResponse);


                    //        var results = from p in resultdata
                    //                      where p.businessDate.Date >= fromDate.Date && p.businessDate.Date <= toDate.Date
                    //                      group p by new { Weeknumber = (Weekly(p.businessDate)) } into Grp
                    //                      orderby (Grp.Key.Weeknumber) ascending

                    //                      // group p by (Quarterly(p.businessDate.Date.Month)) into Grp

                    //                      select new WeekMatricData
                    //                      {
                    //                          WeekMetricValue = Grp.Sum(a => a.metricValue),
                    //                          Week = Grp.Key.Weeknumber

                    //                      };

                    //        return JsonConvert.SerializeObject(results, Formatting.Indented);
                    //    }


                    //}
                    return null;
            }
            catch (Exception ex)
            {
                return CacheResponse;
            }
        }
        public int Quarterly(int month)
        {
            if (month == 1 || month == 2 || month == 3)
            {
                return 1;
            }
            if (month == 4 || month == 5 || month == 6)
            {
                return 2;
            }
            if (month == 7 || month == 8 || month == 9)
            {
                return 3;
            }
            else
            {
                return 4;
            }

        }
        public int Weekly(DateTime day)
        {
            return System.Globalization.CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(day, System.Globalization.CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }
    }
}
