﻿using InnovateAPI.Models;
using InnovateAPI.Models.Common;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualBasic.CompilerServices;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security.Cryptography;
using System.Text;

namespace InnovateAPI.DataAccess
{
    public class MongoDBDataHelper
    {
        MongoClient client;
        MongoClient clientLabor;
        IConfiguration _iconfiguration;
        IMongoDatabase database;
        IMongoDatabase databaseLabor;
        IMongoCollection<OrganizationalUnits> _orgUnit;
        public IMongoCollection<Languages> _languages;
        public IMongoCollection<Countries> _countries;
        public IMongoCollection<ActualLaborCosts> _actaulLaborCost;
        public IMongoCollection<ForecastGroups> _forecastgroups;
        public IMongoCollection<MetricData> _metricData;
        public IMongoCollection<MetricCalendar> _metricCalendar;
        public IMongoCollection<LaborDemandByDayOG> _laborDemandByDay;
        public IMongoCollection<WeekLaborDemandData> _LaborDemand;

        public MongoDBDataHelper(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            // client = new MongoClient("mongodb+srv://User1:Password123@cluster0-tcm52.azure.mongodb.net/FirstDb?retryWrites=true&w=majority");
            client = new MongoClient(_iconfiguration["MongoServerUrl"]);
            clientLabor = new MongoClient(_iconfiguration["MongoServerUrlLabor"]);
            database = client.GetDatabase(_iconfiguration["MongoDbName"]);
            databaseLabor = clientLabor.GetDatabase(_iconfiguration["MongoDbName"]);

        }

        #region OrganizationUnits
        public void SaveOrgUnitsData(List<OrganizationalUnits> lstOrganizationalUnits)
        {
            //Code for Saving Organizational Units Data into the MongoDB Database

            var filter = new BsonDocument("name", "OrganizationalUnits");

            var options = new ListCollectionNamesOptions { Filter = filter };
            bool isPresent = database.ListCollectionNames(options).Any();
            if (!isPresent)
            {
                database.CreateCollection("OrganizationalUnits");

                var collection = database.GetCollection<object>("OrganizationalUnits");
                collection.InsertMany(lstOrganizationalUnits);
            }

        }
        public List<OrganizationalUnits> GetOrganizationalUnits()
        {
            List<OrganizationalUnits> organizationalUnits = new List<OrganizationalUnits>();
            var database = client.GetDatabase(_iconfiguration["MongoDbName"]);
            _orgUnit = database.GetCollection<OrganizationalUnits>("OrganizationalUnits");
            var filter = new BsonDocument("name", "OrganizationalUnits");
            var options = new ListCollectionNamesOptions { Filter = filter };
            bool isPresent = database.ListCollectionNames(options).Any();
            if (isPresent)
            {
                organizationalUnits = _orgUnit.Find(org => org.Id >= 0).Skip(1).ToList();
            }
            return organizationalUnits;
        }
        public List<OrganizationalUnits> GetOrganizationalUnitsByMetric(string[] metricId)
        {
            List<OrganizationalUnits> organizationalUnits = new List<OrganizationalUnits>();
            var database = client.GetDatabase(_iconfiguration["MongoDbName"]);
            _orgUnit = database.GetCollection<OrganizationalUnits>("OrganizationalUnits");
            var filter = new BsonDocument("name", "OrganizationalUnits");
            var options = new ListCollectionNamesOptions { Filter = filter };
            bool isPresent = database.ListCollectionNames(options).Any();
            if (isPresent)
            {
                organizationalUnits = _orgUnit.Find(org => org.Id >= 0 && metricId.Contains(org.OrgName)).ToList();
            }
            return organizationalUnits;
        }

        #endregion

        #region LanguageData
        public void SaveLanguagesData(List<Languages> lstLanguages)
        {

            var database = client.GetDatabase(_iconfiguration["MongoDbName"]);

            var filter = new BsonDocument("name", "Languages");

            var options = new ListCollectionNamesOptions { Filter = filter };
            bool isPresent = database.ListCollectionNames(options).Any();

            if (!isPresent)
            {
                database.CreateCollection("Languages");

                var collection = database.GetCollection<object>("Languages");
                collection.InsertMany(lstLanguages);
            }

        }
        public List<Languages> GetLanguagesData()
        {
            List<Languages> languages = new List<Languages>();
            var database = client.GetDatabase(_iconfiguration["MongoDbName"]);

            _languages = database.GetCollection<Languages>("Languages");

            var filter = new BsonDocument("name", "Languages");

            var options = new ListCollectionNamesOptions { Filter = filter };
            bool isPresent = database.ListCollectionNames(options).Any();
            if (isPresent)
            {
                return _languages.Find(lag => lag.Id >= 0).ToList();
            }
            return languages;
        }

        public Languages GetLanguagesDataByID(long id)
        {
            Languages languages = new Languages();
            var database = client.GetDatabase(_iconfiguration["MongoDbName"]);

            _languages = database.GetCollection<Languages>("Languages");

            var filter = new BsonDocument("name", "Languages");

            var options = new ListCollectionNamesOptions { Filter = filter };
            bool isPresent = database.ListCollectionNames(options).Any();
            if (isPresent)
            {
                return _languages.Find<Languages>(lag => lag.Id == id).FirstOrDefault();
            }
            return languages;
        }

        #endregion

        #region CounttriesData
        public void SaveCountriesData(List<Countries> lstCountries)
        {
            //Code for Saving Organizational Units Data into the MongoDB Database

            // Need to call Data Access Layer from here to save data

            var database = client.GetDatabase(_iconfiguration["MongoDbName"]);

            var filter = new BsonDocument("name", "Countries");

            var options = new ListCollectionNamesOptions { Filter = filter };
            bool isPresent = database.ListCollectionNames(options).Any();

            if (!isPresent)
            {

                //database.DropCollection("Countries");
                database.CreateCollection("Countries");
                var collection = database.GetCollection<object>("Countries");
                collection.InsertMany(lstCountries);
            }

        }
        public List<Countries> GetCountriesData()
        {
            List<Countries> countries = new List<Countries>();
            var database = client.GetDatabase(_iconfiguration["MongoDbName"]);

            _countries = database.GetCollection<Countries>("Countries");

            var filter = new BsonDocument("name", "Countries");

            var options = new ListCollectionNamesOptions { Filter = filter };
            bool isPresent = database.ListCollectionNames(options).Any();

            if (isPresent)
            {
                return _countries.Find(lag => lag.Id > 0).ToList();
            }
            return countries;
        }

        public Countries GetCountriesDataByID(long id)
        {
            Countries countries = new Countries();
            var database = client.GetDatabase(_iconfiguration["MongoDbName"]);

            _countries = database.GetCollection<Countries>("Countries");

            var filter = new BsonDocument("name", "Countries");

            var options = new ListCollectionNamesOptions { Filter = filter };
            bool isPresent = database.ListCollectionNames(options).Any();
            if (isPresent)
            {
                return _countries.Find<Countries>(lag => lag.Id == id).FirstOrDefault();
            }
            return countries;
        }

        #endregion

        #region ActualLaborCost
        public void SaveActualLaborCostData(ActualLaborCosts lstActualLaborCosts)
        {
            //Code for Saving Organizational Units Data into the MongoDB Database

            // Need to call Data Access Layer from here to save data

            try
            {
                var database = client.GetDatabase(_iconfiguration["MongoDbName"]);

                var filter = new BsonDocument("name", "ActaulLaborCost");

                var options = new ListCollectionNamesOptions { Filter = filter };
                bool isPresent = database.ListCollectionNames(options).Any();

                if (!isPresent)
                {
                    database.DropCollection("ActaulLaborCost");
                    database.CreateCollection("ActaulLaborCost");
                    var collection = database.GetCollection<object>("ActaulLaborCost");
                    collection.InsertOne(lstActualLaborCosts);
                }
                else
                {
                    var datas = _actaulLaborCost.Find<ActualLaborCosts>(lag => lag.siteId == lstActualLaborCosts.siteId).FirstOrDefault();
                    if (datas == null)
                    {
                        var collection = database.GetCollection<object>("ActaulLaborCost");
                        collection.InsertOne(lstActualLaborCosts);
                    }
                }
            }
            catch (Exception ex)
            {

            }

        }

        public dynamic GetActaulLaborCostBySiteIdData(long siteId)
        {
            var database = client.GetDatabase(_iconfiguration["MongoDbName"]);

            _actaulLaborCost = database.GetCollection<ActualLaborCosts>("ActaulLaborCost");
            var filter = new BsonDocument("name", "ActaulLaborCost");

            var options = new ListCollectionNamesOptions { Filter = filter };
            bool isPresent = database.ListCollectionNames(options).Any();


            if (isPresent)
            {
                return _actaulLaborCost.Find<ActualLaborCosts>(lag => lag.siteId == siteId).FirstOrDefault();
            }
            return null;
        }

        public ActualLaborCosts GetActaulLaborCostByEmployeeIdData(long siteId, long employeeId)
        {
            ActualLaborCosts actualLaborCost = new ActualLaborCosts();
            var database = client.GetDatabase(_iconfiguration["MongoDbName"]);

            _actaulLaborCost = database.GetCollection<ActualLaborCosts>("ActaulLaborCost");

            var filter = new BsonDocument("name", "ActaulLaborCost");

            var options = new ListCollectionNamesOptions { Filter = filter };
            bool isPresent = database.ListCollectionNames(options).Any();
            if (isPresent)
            {
                return _actaulLaborCost.Find(lag => lag.siteId == siteId && lag.employeeId == employeeId).FirstOrDefault();
            }
            return actualLaborCost;
        }
        #endregion

        #region ForecastGroup
        public void SaveForcastGroupData(ForecastGroups forecastgroups)
        {
            //Code for Saving Organizational Units Data into the MongoDB Database

            // Need to call Data Access Layer from here to save data

            var database = client.GetDatabase(_iconfiguration["MongoDbName"]);

            var filter = new BsonDocument("name", "ForecastGroup");

            var options = new ListCollectionNamesOptions { Filter = filter };
            bool isPresent = database.ListCollectionNames(options).Any();

            if (isPresent)
            {
                database.DropCollection("ForecastGroup");
                //database.CreateCollection("ForecastGroup");
                var collection = database.GetCollection<object>("ForecastGroup");
                collection.InsertOne(forecastgroups);
            }
            else
            {
                var datas = _forecastgroups.Find<ForecastGroups>(lag => lag.id == forecastgroups.id).FirstOrDefault();
                if (datas == null)
                {
                    var collection = database.GetCollection<object>("ForecastGroup");
                    collection.InsertOne(forecastgroups);
                }
            }

        }

        public dynamic GetForecastGroupByIdData(long Id)
        {
            // ActualLaborCosts actualLaborCost = new ActualLaborCosts();

            var database = client.GetDatabase(_iconfiguration["MongoDbName"]);

            _forecastgroups = database.GetCollection<ForecastGroups>("ForecastGroup");

            var filter = new BsonDocument("name", "ForecastGroup");

            var options = new ListCollectionNamesOptions { Filter = filter };
            bool isPresent = database.ListCollectionNames(options).Any();

            if (isPresent)
            {
                return _forecastgroups.Find<ForecastGroups>(lag => lag.id == Id).FirstOrDefault();
            }
            return null;
        }

        public dynamic GetForecastGroupByNameData(string Name)
        {
            // ActualLaborCosts actualLaborCost = new ActualLaborCosts();

            var database = client.GetDatabase(_iconfiguration["MongoDbName"]);

            _forecastgroups = database.GetCollection<ForecastGroups>("ForecastGroup");

            var filter = new BsonDocument("name", "ForecastGroup");

            var options = new ListCollectionNamesOptions { Filter = filter };
            bool isPresent = database.ListCollectionNames(options).Any();

            if (isPresent)
            {
                return _forecastgroups.Find<ForecastGroups>(lag => lag.name == Name).FirstOrDefault();
            }
            return null;
        }
        #endregion

        #region MetricData

        public List<MatricDataOG> GetNewMetricDatas(string startDate, string endDate)
        {
            DateTime fromDate = Convert.ToDateTime(startDate);
            DateTime toDate = Convert.ToDateTime(endDate);
            var database = client.GetDatabase(_iconfiguration["MongoDbName"]);

            _metricData = database.GetCollection<MetricData>("metrics_t");
            _metricCalendar = database.GetCollection<MetricCalendar>("fiscal_calender");
            ///////////////////////////////
            var filter = new BsonDocument("name", "metrics_t");
            var filter1 = new BsonDocument("name", "fiscal_calender");
            ///////////////////////////////
            var optionsF = new ListCollectionNamesOptions { Filter = filter };
            var optionsF1 = new ListCollectionNamesOptions { Filter = filter1 };
            ////////////////////////////////
            bool isPresentF = database.ListCollectionNames(optionsF).Any();
            bool isPresentF1 = database.ListCollectionNames(optionsF1).Any();
            ///////////////////////////////

            if (isPresentF && isPresentF1 == true)
            {
                //var metricD = _metricData.Find(x => true).ToList();
                //var calendarD = _metricCalendar.Find(x => true).ToList();
                var fiscalCalendar = _metricCalendar.Find(x => x.Start_date >= fromDate && x.End_date <= toDate).ToList();
                // var firstDocument = _metricData.Find(a => a.businessDate >= fromDate && a.businessDate <= toDate).ToList();

                var firstDocument = fiscalCalendar.AsQueryable()
                              //.Where(lag => lag.Start_date >= fromDate && lag.End_date <= toDate)
                              .GroupBy(lag => new
                              {
                                  getMatrics = (getWeekData(lag.Start_date, lag.End_date)),
                                  lag.Week,
                                  lag.Month,
                                  lag.Quarter,
                                  lag.Year

                              })
                                .Select(g => new MetricCollection
                                {
                                    WeekMatric = g.Key.getMatrics.ToList(),
                                    Week = g.Key.Week,
                                    Month = g.Key.Month,
                                    Quarter = g.Key.Quarter,
                                    Year = g.Key.Year

                                }
                                      //.Select(g => new MetricCollection
                                      //{
                                      //    WeekMatric = g.Key.getMatrics.Cast<WeekMatricData>().ToList(),
                                      //    Week = g.Key.Week,
                                      //    Month = g.Key.Month,
                                      //    Quarter = g.Key.Quarter,
                                      //    Year = g.Key.Year

                                      //}
                                      )
                              .ToList();
                List<MatricDataOG> lstMatricDataOG = new List<MatricDataOG>();

                foreach (var matcoll in firstDocument)
                {
                    foreach (var weeksValue in matcoll.WeekMatric)
                    {
                        var matricDataOG = new MatricDataOG
                        {
                            MetricValue = weeksValue.MetricValue,
                            Store = weeksValue.Store,
                            Week = matcoll.Week,
                            Year = matcoll.Year,
                            Month = matcoll.Month


                        };
                        lstMatricDataOG.Add(matricDataOG);
                    }

                }

                return lstMatricDataOG;
            }

            return null;

        }

        public IEnumerable<WeekMatricData> getWeekData(DateTime start, DateTime to)
        {

            var metricVal = _metricData.AsQueryable()
               .Where(a => a.businessDate >= start && a.businessDate <= to)
               .GroupBy(a => new { a.siteId })
               .Select(Grp => new WeekMatricData
               {
                   MetricValue = Grp.Sum(a => a.metricValue),
                   Store = Grp.Key.siteId,

               });
            return metricVal;
        }

        public List<MatricDataOG> GetNewMetricDatasByMetric(string startDate, string endDate, string[] mtricId)
        {
            DateTime fromDate = Convert.ToDateTime(startDate);
            DateTime toDate = Convert.ToDateTime(endDate);
            var database = client.GetDatabase(_iconfiguration["MongoDbName"]);

            _metricData = database.GetCollection<MetricData>("metrics_t");
            _metricCalendar = database.GetCollection<MetricCalendar>("fiscal_calender");
            ///////////////////////////////
            var filter = new BsonDocument("name", "metrics_t");
            var filter1 = new BsonDocument("name", "fiscal_calender");
            ///////////////////////////////
            var optionsF = new ListCollectionNamesOptions { Filter = filter };
            var optionsF1 = new ListCollectionNamesOptions { Filter = filter1 };
            ////////////////////////////////
            bool isPresentF = database.ListCollectionNames(optionsF).Any();
            bool isPresentF1 = database.ListCollectionNames(optionsF1).Any();
            ///////////////////////////////

            if (isPresentF && isPresentF1 == true)
            {

                var fiscalCalendar = _metricCalendar.Find(x => x.Start_date >= fromDate && x.End_date <= toDate).ToList();


                var firstDocument = fiscalCalendar.AsQueryable()
                              //.Where(lag => lag.Start_date >= fromDate && lag.End_date <= toDate)
                              .GroupBy(lag => new
                              {
                                  getMatrics = (getWeekDataByMetrics(lag.Start_date, lag.End_date, mtricId)),
                                  lag.Week,
                                  lag.Month,
                                  lag.Quarter,
                                  lag.Year
                              })
                                .Select(g => new MetricCollection
                                {
                                    WeekMatric = g.Key.getMatrics.ToList(),
                                    Week = g.Key.Week,
                                    Month = g.Key.Month,
                                    Quarter = g.Key.Quarter,
                                    Year = g.Key.Year

                                }

                                      )
                              .ToList();
                List<MatricDataOG> lstMatricDataOG = new List<MatricDataOG>();
                foreach (var matcoll in firstDocument)
                {
                    foreach (var weeksValue in matcoll.WeekMatric)
                    {
                        var matricDataOG = new MatricDataOG
                        {
                            MetricValue = weeksValue.MetricValue,
                            Store = weeksValue.Store,
                            Week = matcoll.Week,
                            Year = matcoll.Year,
                            Month = matcoll.Month
                        };
                        lstMatricDataOG.Add(matricDataOG);
                    }

                }

                return lstMatricDataOG;
            }

            return null;

        }

        public IEnumerable<WeekMatricData> getWeekDataByMetrics(DateTime start, DateTime to, string[] mtricId)
        {

            var metricVal = _metricData.AsQueryable()
               .Where(a => a.businessDate >= start && a.businessDate <= to && mtricId.Contains(a.metricId))
               .GroupBy(a => new { a.siteId })
               .Select(Grp => new WeekMatricData
               {
                   MetricValue = Grp.Sum(a => a.metricValue),
                   Store = Grp.Key.siteId


                   //MetricId = Grp.Key.metricId

               });
            return metricVal;
        }

        public List<string> GetMetricList()
        {
            var metricList = _metricData.AsQueryable().Select(a => a.metricId).Distinct().ToList();
            return metricList;
        }




        #endregion

        #region LaborDemandHour

        public void SaveLaborDemandByDayData(List<LaborDemandByDay> lstlabourdemandbyday)
        {
            //Code for Saving LaborDemandByDay Data into the MongoDB Database

            var filter = new BsonDocument("name", "LaborDemandByDay");

            var options = new ListCollectionNamesOptions { Filter = filter };
            bool isPresent = database.ListCollectionNames(options).Any();
            if (!isPresent)
            {
                database.CreateCollection("LaborDemandByDay");

                var collection = database.GetCollection<object>("LaborDemandByDay");
                collection.InsertMany(lstlabourdemandbyday);
            }
            else
            {
                var collection = database.GetCollection<object>("LaborDemandByDay");
                collection.InsertMany(lstlabourdemandbyday);
            }

        }
        public List<WeekLaborDemandData> GetLaborHourDayDataFromMongoDB()
        {
            List<WeekLaborDemandData> labordemand = new List<WeekLaborDemandData>();
            var database = clientLabor.GetDatabase(_iconfiguration["MongoDbName"]);

            _LaborDemand = databaseLabor.GetCollection<WeekLaborDemandData>("LaborDemandByDay");

            var filter = new BsonDocument("name", "LaborDemandByDay");

            var options = new ListCollectionNamesOptions { Filter = filter };
            bool isPresent = database.ListCollectionNames(options).Any();
            try
            {
                if (isPresent)
                {
                    var labordemandbydaday = _LaborDemand.Find(lag => lag.siteId != "").ToList();

                    var getdata = labordemandbydaday.AsQueryable().ToList();
                    List<WeekLaborDemandData> lstlabordata = new List<WeekLaborDemandData>();

                    foreach (var LaborDemandByDay in getdata)
                    {
                        var laborDemandByDay = new WeekLaborDemandData
                        {
                            siteId = LaborDemandByDay.siteId,
                            businessDate = LaborDemandByDay.businessDate,
                            hours = LaborDemandByDay.hours
                        };
                        lstlabordata.Add(laborDemandByDay);
                    }

                    return lstlabordata;
                }
            }
            catch (Exception ex)
            {


            }
            return labordemand;
        }
        //////////////////////////////////////////////////////////
        public List<LaborHourDataOG> GetLaborDemandByDayData(string startDate, string endDate)
        {
            DateTime fromDate = Convert.ToDateTime(startDate);
            DateTime toDate = Convert.ToDateTime(endDate);
            var database = clientLabor.GetDatabase(_iconfiguration["MongoDbName"]);

            _laborDemandByDay = database.GetCollection<LaborDemandByDayOG>("LaborDemandByDay");
            _metricCalendar = database.GetCollection<MetricCalendar>("fiscal_calender");
            ///////////////////////////////
            var filter = new BsonDocument("name", "LaborDemandByDay");
            var filter1 = new BsonDocument("name", "fiscal_calender");
            ///////////////////////////////
            var optionsF = new ListCollectionNamesOptions { Filter = filter };
            var optionsF1 = new ListCollectionNamesOptions { Filter = filter1 };
            ////////////////////////////////
            bool isPresentF = database.ListCollectionNames(optionsF).Any();
            bool isPresentF1 = database.ListCollectionNames(optionsF1).Any();
            ///////////////////////////////

            if (isPresentF && isPresentF1 == true)
            {

                var fiscalCalendar = _metricCalendar.Find(x => x.Start_date >= fromDate && x.End_date <= toDate).ToList();

                var firstDocument = fiscalCalendar.AsQueryable()
                              .GroupBy(lag => new
                              {
                                  getLabor = (getLaborWeekData(lag.Start_date, lag.End_date)),
                                  lag.Week,
                                  lag.Month,
                                  lag.Quarter,
                                  lag.Year

                              })
                                .Select(g => new LaborCollection
                                {
                                    WeekLabor = g.Key.getLabor.ToList(),
                                    Week = g.Key.Week,
                                    Month = g.Key.Month,
                                    Quarter = g.Key.Quarter,
                                    Year = g.Key.Year

                                }).ToList();

                List<LaborHourDataOG> lstLabourDataOG = new List<LaborHourDataOG>();

                foreach (var matcoll in firstDocument)
                {
                    foreach (var weeksValue in matcoll.WeekLabor)
                    {
                        var matricDataOG = new LaborHourDataOG
                        {
                            Hours = weeksValue.hours,
                            SiteId = weeksValue.siteId,
                            Week = matcoll.Week,
                            Year = matcoll.Year,
                            Month = matcoll.Month,
                            Quarter = matcoll.Quarter


                        };
                        lstLabourDataOG.Add(matricDataOG);
                    }

                }

                return lstLabourDataOG;
            }

            return null;

        }
        public IEnumerable<WeekLaborDemandData> getLaborWeekData(DateTime start, DateTime to)
        {

            var laborVal = _laborDemandByDay.AsQueryable()
               .Where(a => a.businessDate >= start && a.businessDate <= to)
               .GroupBy(a => new { a.siteId })
               .Select(Grp => new WeekLaborDemandData
               {
                   hours = Grp.Sum(a => a.hours),
                   siteId = Grp.Key.siteId

               });
            return laborVal;
        }
        //////////////////////////////////////////////////////////
        public List<LaborHourDataOG> GetLaborDemandByDayDataBySiteId(string startDate, string endDate, string[] siteId)
        {
            DateTime fromDate = Convert.ToDateTime(startDate);
            DateTime toDate = Convert.ToDateTime(endDate);
            var database = clientLabor.GetDatabase(_iconfiguration["MongoDbName"]);

            _laborDemandByDay = database.GetCollection<LaborDemandByDayOG>("LaborDemandByDay");
            _metricCalendar = database.GetCollection<MetricCalendar>("fiscal_calender");
            ///////////////////////////////
            var filter = new BsonDocument("name", "LaborDemandByDay");
            var filter1 = new BsonDocument("name", "fiscal_calender");
            ///////////////////////////////
            var optionsF = new ListCollectionNamesOptions { Filter = filter };
            var optionsF1 = new ListCollectionNamesOptions { Filter = filter1 };
            ////////////////////////////////
            bool isPresentF = database.ListCollectionNames(optionsF).Any();
            bool isPresentF1 = database.ListCollectionNames(optionsF1).Any();
            ///////////////////////////////

            if (isPresentF && isPresentF1 == true)
            {

                var fiscalCalendar = _metricCalendar.Find(x => x.Start_date >= fromDate && x.End_date <= toDate).ToList();

                var firstDocument = fiscalCalendar.AsQueryable()
                              .GroupBy(lag => new
                              {
                                  getLabor = (getLaborWeekDataBySiteId(lag.Start_date, lag.End_date, siteId)),
                                  lag.Week,
                                  lag.Month,
                                  lag.Quarter,
                                  lag.Year

                              })
                                .Select(g => new LaborCollection
                                {
                                    WeekLabor = g.Key.getLabor.ToList(),
                                    Week = g.Key.Week,
                                    Month = g.Key.Month,
                                    Quarter = g.Key.Quarter,
                                    Year = g.Key.Year

                                }).ToList();

                List<LaborHourDataOG> lstLabourDataOG = new List<LaborHourDataOG>();

                foreach (var matcoll in firstDocument)
                {
                    foreach (var weeksValue in matcoll.WeekLabor)
                    {
                        var matricDataOG = new LaborHourDataOG
                        {
                            Hours = weeksValue.hours,
                            SiteId = weeksValue.siteId,
                            Week = matcoll.Week,
                            Year = matcoll.Year,
                            Month = matcoll.Month,
                            Quarter = matcoll.Quarter


                        };
                        lstLabourDataOG.Add(matricDataOG);
                    }

                }

                return lstLabourDataOG;
            }

            return null;

        }
        public IEnumerable<WeekLaborDemandData> getLaborWeekDataBySiteId(DateTime start, DateTime to, string[] siteId)
        {

            var laborVal = _laborDemandByDay.AsQueryable()
               .Where(a => a.businessDate >= start && a.businessDate <= to && siteId.Contains(a.siteId))
               .GroupBy(a => new { a.siteId })
               .Select(Grp => new WeekLaborDemandData
               {
                   hours = Grp.Sum(a => a.hours),
                   siteId = Grp.Key.siteId

               });
            return laborVal;
        }
        //////////////////////////////////////////////////////////
        public List<string> GetLaborDemandHourList()
        {
            var laborList = _laborDemandByDay.AsQueryable().Select(a => a.siteId).Distinct().ToList();
            return laborList;
        }


        #endregion




    }
}
