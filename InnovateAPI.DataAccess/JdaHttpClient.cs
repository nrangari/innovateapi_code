﻿using InnovateAPI.Models.Common;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace InnovateAPI.DataAccess
{
   public class JdaHttpClient
    {
        IConfiguration _iconfiguration;
        public JdaHttpClient(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
        }
        public dynamic JdaHttpClientResponse(string refSessionId, string jdaClientUrl)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(_iconfiguration["ServerUrl"]);
                    var response = client.GetAsync(jdaClientUrl).Result;

                    var result = response.Content.ReadAsStringAsync().Result;
                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);
                    return dynamicObject;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
