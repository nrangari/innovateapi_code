﻿using InnovateAPI.DataAccess;
using InnovateAPI.Models.Common;
using Microsoft.Extensions.Configuration;
using InnovateAPI.IPlugin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Reflection;
using System.IO;

namespace InnovateAPI.Business
{
    public class APILayer
    {
        readonly JdaHttpClient _jdaHttpClient;
        IConfiguration _iconfiguration;
        InnovateInterface _iInnovatePlugin;
        
        public APILayer(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            _jdaHttpClient = new JdaHttpClient(_iconfiguration);
            if (_iconfiguration["SelectedServer"] == "BlueYonder") //checking for plugin Name
            {

                var appRoot = Path.GetFullPath(AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("InnovateAPI")));
                 string assemblyPath =_iconfiguration["BlueYonderDllPath"];
                // string assemblyPath = Path.GetFullPath(appRoot) + Path.Combine("PluginBlueYonder\\bin\\Debug\\netcoreapp3.1\\PluginBlueYonder.dll");
               // string assemblyPath = Path.GetFullPath(appRoot) + Path.Combine("InnovateAPI_24_07_20\\innovateapi_code\\PluginBlueYonder\\bin\\Debug\\netcoreapp3.1\\PluginBlueYonder.dll");

                Assembly.LoadFrom(assemblyPath);
                foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
                {
                    foreach (Type t in a.GetTypes())
                    {
                        if (t.GetInterface("InnovateInterface") != null)
                        {
                            _iInnovatePlugin = Activator.CreateInstance(t) as InnovateInterface;
                        }
                    }
                }
            }
        }
        public dynamic GetLoginResponse(string userName, string password)
        {           
            return _iInnovatePlugin.GetLoginAPIResponse(_iconfiguration["ServerUrl"], Constants.LoginAPI, userName, password);
        }
        public dynamic GetOrgUnitsDataFromAPI(string refSessionId)
        {

            //Code to Get Organizational Units Data from Blue Yonder API

            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], Constants.orgUnitsDataAPI, refSessionId);
            //return _jdaHttpClient.JdaHttpClientResponse(refSessionId, Constants.orgUnitsDataAPI);

        }

        public dynamic GetOrgHierarchyLevelsDataFromAPI(string refSessionId)
        {

            //Code to Get Organizational Hierarchy Level Data from Blue Yonder API
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], Constants.orgHierarchyLevelsDataAPI, refSessionId);

            //return _jdaHttpClient.JdaHttpClientResponse(refSessionId, Constants.orgHierarchyLevelsDataAPI);

        }

        #region Sites

        public dynamic GetSitesDataFromAPI(string refSessionId)
        {
            string queryString = Constants.sitesDataAPI;
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);
            // return _jdaHttpClient.JdaHttpClientResponse(refSessionId, queryString);

        }
        public dynamic GetSitesByIdDataFromAPI(string refSessionId, long siteId)
        {
            string queryString = Constants.sitesDataAPI + $"/{siteId}";
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);
            // return _jdaHttpClient.JdaHttpClientResponse(refSessionId, queryString);

        }
        public dynamic GetSitesByMultipleIdDataFromAPI(string refSessionId, long[] siteId)
        {
            string querystring = $"?";
            for (int i = 0; i < siteId.Length; i++)
            {
                querystring = querystring + "siteIds=" + siteId[i];
                if (i != siteId.Length - 1)
                {
                    querystring = querystring + "&";
                }
            }
            string urlString = Constants.sitesDataAPI + querystring;
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], urlString, refSessionId);

            //return _jdaHttpClient.JdaHttpClientResponse(refSessionId, urlString);

        }

        public dynamic GetSitesByNameDataFromAPI(string refSessionId, string siteName)
        {
            string queryString = Constants.sitesDataAPI + $"?name={siteName}";
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);

            // return _jdaHttpClient.JdaHttpClientResponse(refSessionId, queryString);

        }

        public dynamic GetSiteByCurrentTimeInformationDataFromAPI(string refSessionId, long siteId)
        {
            string queryString = Constants.sitesDataAPI + $"/{siteId}/siteCurrentTimeInformation";
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);

            // return _jdaHttpClient.JdaHttpClientResponse(refSessionId, queryString);

        }

        public dynamic GetSitesByOrganizationalUnitIdDataFromAPI(string refSessionId, long organizationUnitId)
        {
            string queryString = Constants.sitesDataAPI + $"?organizationalUnitId={organizationUnitId}";
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);

            // return _jdaHttpClient.JdaHttpClientResponse(refSessionId, queryString);

        }

        #endregion

        #region Countries 
        public dynamic GetCountriesDataFromAPI(string refSessionId)
        {
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], Constants.CountriesDataAPI, refSessionId);
            //return _jdaHttpClient.JdaHttpClientResponse(refSessionId, Constants.CountriesDataAPI);
        }
        public dynamic GetCountriesByIdDataFromAPI(string refSessionId, long countriesId)
        {
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], Constants.CountriesByIdDataUrl + countriesId, refSessionId);
            //return _jdaHttpClient.JdaHttpClientResponse(refSessionId, Constants.CountriesByIdDataUrl + countriesId);
        }
        #endregion

        #region Languages
        public dynamic GetLangauesDataFromAPI(string refSessionId)
        {
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], Constants.LanguagesDataAPI, refSessionId);
            //return _jdaHttpClient.JdaHttpClientResponse(refSessionId, Constants.LanguagesDataAPI);
        }
        public dynamic GetLangaueByIdDataFromAPI(string refSessionId, long languagesId)
        {
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], Constants.LanguagesByIdDataUrl + languagesId, refSessionId);
            //return _jdaHttpClient.JdaHttpClientResponse(refSessionId, Constants.LanguagesByIdDataUrl + languagesId);
        }
        #endregion

        #region ActualLaborCost

        public dynamic GetActualLaborCostsBySiteId(string refSessionId, long siteId, string businessDate, string frequency)
        {
            if (frequency == null)
            {
                string queryString = Constants.ActualLaborCostjdaClientAPI + $"site/{siteId}/businessDate/{businessDate}";
                return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);
            }
            else
            {
                string queryString = Constants.ActualLaborCostjdaClientAPI + $"site/{siteId}/businessDate/{businessDate}?frequency={frequency}";
                return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);
            }

            //return _jdaHttpClient.JdaHttpClientResponse(refSessionId, queryString);
        }

        public dynamic GetActualLaborCostsByEmployeeId(string refSessionId, long siteId, long employeeId, string businessDate, string frequency)
        {
            if (frequency == null)
            {
                string queryString = Constants.ActualLaborCostjdaClientAPI + $"site/{siteId}/employee/{employeeId}/businessDate/{businessDate}";
                return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);
            }
            else
            {
                string queryString = Constants.ActualLaborCostjdaClientAPI + $"site/{siteId}/employee/{employeeId}/businessDate/{businessDate}?frequency={frequency}";
                return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);
            }

            //  return _jdaHttpClient.JdaHttpClientResponse(refSessionId, queryString);
        }
        #endregion

        #region IsoLanguages 
        public dynamic GetIsoLangauesDataFromAPI(string refSessionId, long isolanguagesId)
        {
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], Constants.IsoLangauesDataUrl + isolanguagesId, refSessionId);
            //return _jdaHttpClient.JdaHttpClientResponse(refSessionId, Constants.IsoLangauesDataUrl + isolanguagesId);
        }
        #endregion

        #region TimeZone

        public dynamic GetTimeZonesDataFromAPI(string refSessionId)
        {
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], Constants.TimeZonesDataUrl, refSessionId);
            // return _jdaHttpClient.JdaHttpClientResponse(refSessionId, Constants.TimeZonesDataUrl);

        }
        #endregion

        #region Metrics
        public dynamic GetMetricsDataFromAPI(string refSessionId, long metricsId)
        {
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], Constants.MetricsDataUrl + metricsId, refSessionId);
            //return _jdaHttpClient.JdaHttpClientResponse(refSessionId, Constants.MetricsDataUrl + metricsId);
        }
        #endregion

        #region Metrics Group
        public dynamic GetMetricsGroupsDataFromAPI(string refSessionId, long metricsGroupsId)
        {
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], Constants.MetricsDataUrl + metricsGroupsId, refSessionId);
            //return _jdaHttpClient.JdaHttpClientResponse(refSessionId, Constants.MetricsGroupsDataUrl + metricsGroupsId);
        }
        #endregion

        #region Jobs API

        public dynamic GetJobsDataByIdFromAPI(string refSessionId, long jobsId)
        {
            //Code to Get Jobs Data By Id from Blue Yonder API
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], string.Concat(Constants.jobsDataByIdUrl, jobsId), refSessionId);

        }
        public dynamic GetJobsDataByNameFromAPI(string refSessionId, string name)
        {
            //Code to Get Jobs Data By Id from Blue Yonder API
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], string.Concat(Constants.jobsDataByNameUrl, name), refSessionId);

        }
        public dynamic GetJobsDataBySiteIdFromAPI(string refSessionId, long siteId)
        {
            //Code to Get Jobs Data By Id from Blue Yonder API
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], string.Concat(Constants.jobsDataBySiteIdUrl, siteId), refSessionId);

        }
        #endregion

        #region Job Title API
        public dynamic GetJobTitleDataFromAPI(string refSessionId)
        {
            //Code to Get Job Title List from Blue Yonder API
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], string.Concat(Constants.jobTitleDataUrl), refSessionId);

        }

        public dynamic GetJobTitleDataByTitleIdFromAPI(string refSessionId, long titleId)
        {
            //Code to Get Job Title List from Blue Yonder API
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], string.Concat(Constants.jobTitleDataByTitleIdUrl, titleId), refSessionId);

        }

        public dynamic GetJobTitleDataByNameFromAPI(string refSessionId, string name)
        {
            //Code to Get Job Title List from Blue Yonder API
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], string.Concat(Constants.jobTitleDataByNameUrl, name), refSessionId);

        }


        public dynamic GetJobTitleDataBySiteIdFromAPI(string refSessionId, long siteId)
        {
            //Code to Get Job Title List from Blue Yonder API
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], string.Concat(Constants.jobTitleDataBySiteIdUrl, siteId), refSessionId);

        }
        #endregion

        #region DepartmentAPI

        public dynamic GetDepartmentDataByIdFromAPI(string refSessionId, long departmentId)
        {
            //Code to Get Job Title List from Blue Yonder API
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], string.Concat(Constants.departmentDataByIdUrl, departmentId), refSessionId);

        }
        public dynamic GetDepartmentDataBySiteIdFromAPI(string refSessionId, long siteIdFilter)
        {
            //Code to Get Job Title List from Blue Yonder API
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], string.Concat(Constants.departmentDataBySiteIdUrl, siteIdFilter), refSessionId);

        }
        public dynamic GetDepartmentDataByNameFromAPI(string refSessionId, string name)
        {
            //Code to Get Job Title List from Blue Yonder API
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], string.Concat(Constants.departmentDataByNameUrl, name), refSessionId);

        }


        #endregion

        #region ForcastGroups

        public dynamic GetForecastGroupByIdDataFromAPI(string refSessionId, long ForcastGroupId)
        {
            string queryString = Constants.ForecastGrouptjdaClientAPI + $"/{ForcastGroupId}";
            //  return _jdaHttpClient.JdaHttpClientResponse(refSessionId, queryString);
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);

        }

        public dynamic GetForecastGroupByNameDataFromAPI(string refSessionId, string ForcastGroupName)
        {
            string queryString = Constants.ForecastGrouptjdaClientAPI + $"?name={ForcastGroupName}";
            // return _jdaHttpClient.JdaHttpClientResponse(refSessionId, queryString);
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);

        }
        #endregion

        #region ActualLaborHours
        public dynamic GetActualLaborHoursByJobIdDataFromAPI(string refSessionId, long siteId, long jobId, string businessDate)
        {
            string queryString = Constants.ActualLaborHours + $"/site/{siteId}/job/{jobId}/businessDate/{businessDate}";
            //   return _jdaHttpClient.JdaHttpClientResponse(refSessionId, queryString);
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);
        }

        public dynamic GetActualLaborHoursBySiteIdDataFromAPI(string refSessionId, long siteId, string businessDate)
        {
            string queryString = Constants.ActualLaborHours + $"/site/{siteId}/businessDate/{businessDate}";
            // return _jdaHttpClient.JdaHttpClientResponse(refSessionId, queryString);
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);
        }

        public dynamic GetActualLaborHoursByWorkgroupIdDataFromAPI(string refSessionId, long siteId, long workgroupId, string businessDate, string frequency)
        {
            if (frequency == null)
            {
                string queryString = Constants.ActualLaborHours + $"/site/{siteId}/workgroup/{workgroupId}/businessDate/{businessDate}";
                //  return _jdaHttpClient.JdaHttpClientResponse(refSessionId, queryString);
                return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);
            }
            else
            {
                string queryString = Constants.ActualLaborHours + $"/site/{siteId}/workgroup/{workgroupId}/businessDate/{businessDate}?frequency={frequency}";
                //  return _jdaHttpClient.JdaHttpClientResponse(refSessionId, queryString);
                return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);
            }
        }

        public dynamic GetActualLaborHoursByEmployeeIdDataFromAPI(string refSessionId, long siteId, long eployeeId, string businessDate, string frequency)
        {
            if (frequency == null)
            {
                string queryString = Constants.ActualLaborHours + $"/site/{siteId}/employee/{eployeeId}/businessDate/{businessDate}";
                // return _jdaHttpClient.JdaHttpClientResponse(refSessionId, queryString);
                return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);
            }
            else
            {
                string queryString = Constants.ActualLaborHours + $"/site/{siteId}/employee/{eployeeId}/businessDate/{businessDate}?frequency ={frequency}";
                // return _jdaHttpClient.JdaHttpClientResponse(refSessionId, queryString);
                return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);
            }
        }
        #endregion

        #region idealLaborHours

        public dynamic GetidealLaborHoursByBusinessDateFromAPI(string refSessionId, long siteId, string businessDate, string frequency)
        {
            //Code to Get idealLaborHours List By BusinessDate from Blue Yonder API
            string queryString = "";
            if (string.IsNullOrEmpty(frequency))
            {
                queryString = Constants.idealLaborHoursDataUrl + $"site/" + siteId + "/businessDate/" + businessDate;
            }
            else
            {
                queryString = Constants.idealLaborHoursDataUrl + $"site/" + siteId + "/businessDate/" + businessDate + "/frequency=" + frequency;
            }

            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);
        }
        public dynamic GetidealLaborHoursByWorkGroupIdFromAPI(string refSessionId, long siteId, long workgroupId, string businessDate, string frequency)
        {
            //Code to Get idealLaborHours List By WorkGroupId from Blue Yonder API
            string queryString = "";
            if (string.IsNullOrEmpty(frequency))
            {
                queryString = Constants.idealLaborHoursDataUrl + $"site/" + siteId + "/workgroup/" + workgroupId + "/businessDate/" + businessDate;
            }
            else
            {
                queryString = Constants.idealLaborHoursDataUrl + $"site/" + siteId + "/workgroup/" + workgroupId + "/businessDate/" + businessDate + "?frequency=" + frequency;
            }

            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);
        }
        public dynamic GetidealLaborHoursByLaborRoleIdFromAPI(string refSessionId, long siteId, long laborRoleId, string businessDate, string frequency)
        {
            //Code to Get idealLaborHours List By LaborRoleId from Blue Yonder API
            string queryString = "";
            if (string.IsNullOrEmpty(frequency))
            {
                queryString = Constants.idealLaborHoursDataUrl + $"site/" + siteId + "/laborRole/" + laborRoleId + "/businessDate/" + businessDate;
            }
            else
            {
                queryString = Constants.idealLaborHoursDataUrl + $"site/" + siteId + "/laborRole/" + laborRoleId + "/businessDate/" + businessDate + "?frequency=" + frequency;
            }

            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);
        }

        #endregion

        #region ScheduledLaborCost
        public dynamic GetScheduledLaborCostBySiteIdDataFromAPI(string refSessionId, long siteId, string businessDate, string frequency)
        {
            if (frequency == null)
            {
                string queryString = Constants.ScheduledLaborCost + $"/site/{siteId}/businessDate/{businessDate}";
                // return _jdaHttpClient.JdaHttpClientResponse(refSessionId, queryString);
                return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);
            }
            else
            {
                string queryString = Constants.ScheduledLaborCost + $"/site/{siteId}/businessDate/{businessDate}?frequency={frequency}";
                // return _jdaHttpClient.JdaHttpClientResponse(refSessionId, queryString);
                return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);
            }
        }

        public dynamic GetScheduledLaborCostByEmployeeIdDataFromAPI(string refSessionId, long siteId, long employeeId, string businessDate, string frequency)
        {
            if (frequency == null)
            {
                string queryString = Constants.ScheduledLaborCost + $"/site/{siteId}/employee/{employeeId}/businessDate/{businessDate}";
                // return _jdaHttpClient.JdaHttpClientResponse(refSessionId, queryString);
                return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);
            }
            else
            {
                string queryString = Constants.ScheduledLaborCost + $"/site/{siteId}/employee/{employeeId}/businessDate/{businessDate}?frequency={frequency}";
                // return _jdaHttpClient.JdaHttpClientResponse(refSessionId, queryString);
                return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);
            }
        }
        #endregion

        #region LaborDemand
        public dynamic GetLaborDemandBySiteIdDataFromAPI(string refSessionId, long siteId, string businessDate, string frequency)
        {

            if (frequency == null)
            {
                string queryString = Constants.LaborDemandAPI + $"/site/{siteId}/businessDate/{businessDate}";
                // return _jdaHttpClient.JdaHttpClientResponse(refSessionId, queryString);
                return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);
            }
            else
            {
                string queryString = Constants.LaborDemandAPI + $"/site/{siteId}/businessDate/{businessDate}?frequency={frequency}";
                // return _jdaHttpClient.JdaHttpClientResponse(refSessionId, queryString);
                return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);
            }
        }
        public dynamic GetLaborDemandByWorkgroupIdDataFromAPI(string refSessionId, long siteId, long workgroupId, string businessDate, string frequency)
        {
            if (frequency == null)
            {
                string queryString = Constants.LaborDemandAPI + $"/site/{siteId}/workgroup/{workgroupId}/businessDate/{businessDate}";
                // return _jdaHttpClient.JdaHttpClientResponse(refSessionId, queryString);
                return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);
            }
            else
            {
                string queryString = Constants.LaborDemandAPI + $"/site/{siteId}/workgroup/{workgroupId}/businessDate/{businessDate}?frequency={frequency}";
                // return _jdaHttpClient.JdaHttpClientResponse(refSessionId, queryString);
                return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);
            }
        }
        public dynamic GetLaborDemandByLaborRoleIdDataFromAPI(string refSessionId, long siteId, long laborRoleId, string businessDate, string frequency)
        {
            if (frequency == null)
            {
                string queryString = Constants.LaborDemandAPI + $"/site/{siteId}/laborRole/{laborRoleId}/businessDate/{businessDate}";
                // return _jdaHttpClient.JdaHttpClientResponse(refSessionId, queryString);
                return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);
            }
            else
            {
                string queryString = Constants.LaborDemandAPI + $"/site/{siteId}/laborRole/{laborRoleId}/businessDate/{businessDate}?frequency={frequency}";
                // return _jdaHttpClient.JdaHttpClientResponse(refSessionId, queryString);
                return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);
            }
        }
        #endregion

        #region LaborRoles
        public dynamic GetLaborRoleByRoleIddDataFromAPI(string refSessionId, long laborRoleId)
        {

            string queryString = Constants.LaborRolesAPI + $"/{laborRoleId}";
            // return _jdaHttpClient.JdaHttpClientResponse(refSessionId, queryString);
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);
        }

        public dynamic GetLaborRoleByRoleNamedDataFromAPI(string refSessionId, string laborRoleName)
        {

            string queryString = Constants.LaborRolesAPI + $"?name={laborRoleName}";
            // return _jdaHttpClient.JdaHttpClientResponse(refSessionId, queryString);
            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], queryString, refSessionId);
        }
        #endregion

        #region MetricData
        public dynamic GetMetricDataFromAPI(string sessionId, long[] metricId, long[]siteId, string sourceType, string startDate, string EndDate)
        {


            string querystring = $"sourceType/"+sourceType+"?";
            for (int i = 0; i < metricId.Length; i++)
            {
                querystring = querystring + "metric=" + metricId[i];
                //if (i != metricId.Length - 1)
                //{
                    querystring = querystring + "&";
                //}
            }
            for (int i = 0; i < siteId.Length; i++)
            {
                querystring = querystring + "site=" + siteId[i];
                //if (i != siteId.Length - 1)
                //{
                    querystring = querystring + "&";
                //}
            }
            querystring = querystring + "startDate=" + startDate + "&endDate=" + EndDate;
            string urlString = Constants.MetricDataAPI + querystring;

            return _iInnovatePlugin.GetDataResponseFromAPI(_iconfiguration["ServerUrl"], urlString, sessionId);
        }
        #endregion
    }
}
