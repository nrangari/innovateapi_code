﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace InnovateAPI.Business
{
    public class ExceptionLogger
    {
        public static void LogError(Exception err)
        {
            //Code to save Exception in File or a Databases
            Console.Error.WriteLine(err);
            string filePath = Path.GetFullPath((System.IO.Directory.GetCurrentDirectory())) + Path.Combine("\\ExceptionLogger.txt");
            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine("-----------------------------------------------------------------------------");
                writer.WriteLine("Date : " + DateTime.Now.ToString());
                writer.WriteLine();
                while (err != null)
                {
                    writer.WriteLine(err.GetType().FullName);
                    writer.WriteLine("Message : " + err.Message);
                    writer.WriteLine("StackTrace : " + err.StackTrace);
                    err = err.InnerException;
                }
            }
        }
    }
}
