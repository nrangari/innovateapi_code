﻿using InnovateAPI.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Interface
{
    public interface ISitesService
    {
        List<Sites> GetSites(string refSessionId);
        List<Sites> GetSitesById(string refSessionId, long siteId);
        List<Sites> GetSitesByMultipleId(string refSessionId, long[] siteId);
        List<Sites> GetSitesByName(string refSessionId, string siteName);
        SiteCurrentTimeInformation GetSiteCurrentTimeInformation(string refSessionId, long siteId);
        SiteOrganizationalUnit GetSiteByOrganizationalUnitId(string refSessionId, long organizationUnitId);
       
    }
}
