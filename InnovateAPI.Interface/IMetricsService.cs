﻿using InnovateAPI.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Interface
{
    public interface IMetricsService
    {
        List<Metrics> GetMetrics(string refSessionId, long metricesId);
    }
}
