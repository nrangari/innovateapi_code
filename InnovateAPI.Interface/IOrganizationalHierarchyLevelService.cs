﻿using InnovateAPI.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Interface
{
    public interface IOrganizationalHierarchyLevelService
    {
        List<OrganizationalHierarchyLevels> GetOrganizationalHierarchyLevels(string refSessionId);
    }
}
