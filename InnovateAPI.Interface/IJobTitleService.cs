﻿using InnovateAPI.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Interface
{
   public interface IJobTitleService
    {
        List<JobTitle> GetJobTitleData(string refSessionId);
        List<JobTitle> GetJobTitleDataByTitleId(string refSessionId, long titleId);
        List<JobTitle> GetJobTitleDataByName(string refSessionId, string name);
        List<JobTitle> GetJobTitleDataBySiteId(string refSessionId, long siteId);

    }
}
