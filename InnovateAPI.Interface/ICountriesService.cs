﻿using InnovateAPI.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Interface
{
    public interface ICountriesService
    {
        List<Countries> GetCountries(string refSessionId);
        List<Countries> GetCountriesById(string refSessionId, long countriesId);
    }
}
