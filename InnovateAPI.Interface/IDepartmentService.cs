﻿using InnovateAPI.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Interface
{
   public interface IDepartmentService
    {
        List<Department> GetDepartmentDataById(string refSessionId, long departmentId);
        List<Department> GetDepartmentDataByName(string refSessionId, string name);
        List<Department> GetDepartmentDataBySiteId(string refSessionId, long siteId);

    }
}
