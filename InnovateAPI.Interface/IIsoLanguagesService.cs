﻿using InnovateAPI.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Interface
{
    public interface IIsoLanguagesService
    {
        List<IsoLanguages> GetIsoLanguages(string refSessionId, long isolanguagesid);
    }
}
