﻿using InnovateAPI.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Interface
{
   public interface IJobsService
    {
        List<Jobs> GetJobsById(string refSessionId, long jobId);
        List<Jobs> GetJobsByName(string refSessionId, string name);
        List<Jobs> GetJobsBySiteId(string refSessionId, long siteId);
    }
}
