﻿using InnovateAPI.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Interface
{
    public interface ITimeZoneService
    {
        List<TimeZones> GetTimeZones(string refSessionId);
    }
}
