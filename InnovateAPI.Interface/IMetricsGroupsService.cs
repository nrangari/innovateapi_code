﻿using InnovateAPI.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Interface
{
    public interface IMetricsGroupsService
    {
        List<MetricsGroups> MetricsGroups(string refSessionId,long metricsGroupsId);
    }
}
