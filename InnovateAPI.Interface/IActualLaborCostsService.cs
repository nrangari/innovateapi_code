﻿using InnovateAPI.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Interface
{
    public interface IActualLaborCostsService
    {
       ActualLaborCosts GetActualLaborCostsBySiteId(string refSessionId, long siteId, string businessDate, string frequency);
       ActualLaborCosts GetActualLaborCostsByEmployeeId(string refSessionId, long siteId, long employeeId, string businessDate, string frequency);
    }
}
