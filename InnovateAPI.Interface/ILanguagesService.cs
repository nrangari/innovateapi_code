﻿using InnovateAPI.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Interface
{
    public interface ILanguagesService
    {
        List<Languages> GetLanguages(string refSessionId);
        List<Languages> GetLanguageById(string refSessionId, long languagesid);
    }
}
