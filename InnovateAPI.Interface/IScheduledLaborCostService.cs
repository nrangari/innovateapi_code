﻿using InnovateAPI.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Interface
{
    public interface IScheduledLaborCostService
    {
        ScheduledLaborCost GetScheduledLaborCostBySiteId(string sessionId, long siteId, string businessDate, string frequency);
        ScheduledLaborCost GetScheduledLaborCostByEmployeeId(string sessionId, long siteId, long employeeId, string businessDate, string frequency);
    }
}
