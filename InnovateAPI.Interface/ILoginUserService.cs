﻿using InnovateAPI.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Interface
{
    public interface ILoginUserService
    {
        ResponseResult LoginUser(string userName, string password);
    }
}
