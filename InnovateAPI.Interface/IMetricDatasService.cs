﻿using InnovateAPI.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Interface
{
   public interface IMetricDatasService
    {
       dynamic GetMetricDatas(string startDate, string endDate);
    }
}
