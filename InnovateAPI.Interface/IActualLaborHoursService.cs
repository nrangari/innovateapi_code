﻿using InnovateAPI.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Interface
{
   public interface IActualLaborHoursService
    {
        ActualLaborHours GetActualLaborHoursByJobId(string refSessionId, long siteId, long jobId, string businessDate);
        ActualLaborHours  GetActualLaborHoursBySiteId(string refSessionId, long siteId, string businessDate);
        ActualLaborHours GetActualLaborHoursByWorkgroupId(string refSessionId, long siteId, long workgroupId, string businessDate, string frequency);
        ActualLaborHours  GetActualLaborHoursByEmployeeId(string refSessionId, long siteId, long employeeId, string businessDate, string frequency);
    }
}

