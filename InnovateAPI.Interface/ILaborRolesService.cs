﻿using InnovateAPI.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Interface
{
    public interface ILaborRolesService
    {
        LaborRoles GetLaborRoleByRoleId(string sessionId, long laborRoleId);
        LaborRoles GetLaborRoleByRoleName(string sessionId, string laborRoleName);
    }
}

