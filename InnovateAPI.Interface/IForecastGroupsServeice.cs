﻿using InnovateAPI.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Interface
{
    public interface IForecastGroupsServeice
    {
        ForecastGroups GetForecoastGroupById(string refSessionId, long ForecastId);
        ForecastGroups GetForecoastGroupByName(string refSessionId, string ForecastName);
    }
}
