﻿using InnovateAPI.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Interface
{
    public interface IIdealLaborHoursService
    {
        List<IdealLaborHours> GetidealLaborHoursByBusinessDate(string refSessionId, long siteId, string businessDate, string frequency);
        List<IdealLaborHours> GetidealLaborHoursByWorkGroupId(string refSessionId, long siteId, long workgroupId, string businessDate, string frequency);
       // List<IdealLaborHours> GetidealLaborHoursByLaborRoleId(string refSessionId, long siteId, long laborRoleId, string businessDate, string frequency);
    }
}
