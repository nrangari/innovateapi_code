﻿using InnovateAPI.Models;
using InnovateAPI.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Interface
{
    public interface IOrganizationalUnitsService
    {
        List<OrganizationalUnitsVM> GetOrganizationalUnits(string refSessionId);
        List<OrganizationalUnitsVM> GetOrganizationalUnitsMetricData(string sessionId, string startDate, string endDate, string[] metricId);


    }
}
