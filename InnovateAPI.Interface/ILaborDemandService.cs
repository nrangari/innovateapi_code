﻿using InnovateAPI.Models;
using System;
using System.Collections.Generic;
using InnovateAPI.ViewModel;
using System.Text;

namespace InnovateAPI.Interface
{
    public interface ILaborDemandService
    {
        LaborDemand GetlaborDemandBySiteId(string sessionId, long siteId, string businessDate, string frequency);
        LaborDemand GetlaborDemandByWorkgroupId(string sessionId, long siteId, long workgroupId, string businessDate, string frequency);
        LaborDemand GetlaborDemandByLaborRoleId(string sessionId, long siteId, long laborRoleId, string businessDate, string frequency);
        List<LaborDemandHourByDayVM> GetLaborDemandDataFromMongoDB(string sessionId, string startdate, string enddate, string[] siteId);
    }
}
