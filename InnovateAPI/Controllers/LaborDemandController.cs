﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using InnovateAPI.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace InnovateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LaborDemandController : BaseController
    {
        readonly ILaborDemandService _ILaborDemand;
        string refSessionID = "_refSessionId";
        public LaborDemandController(ILaborDemandService laborDemand, IConfiguration iconfiguration, ILoginUserService LoginUser)
             : base(iconfiguration, LoginUser)
        {
            _ILaborDemand = laborDemand;

        }
        [HttpGet]
        [Route("GetlaborDemandBySiteId")]
        public string GetlaborDemandBySiteId(string refSessionId, long siteId, string businessDate, string frequency)
        {
            var json = string.Empty;
            try
            {
                if (CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);

                    LaborDemand lstLaborDemand = _ILaborDemand.GetlaborDemandBySiteId(sessionId, siteId, businessDate, frequency);

                    json = JsonConvert.SerializeObject(lstLaborDemand, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                }

                return json;
                // return "okay";
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        [HttpGet]
        [Route("GetlaborDemandByWorkgroupId")]
        public string GetlaborDemandByWorkgroupId(string refSessionId, long siteId, long workgroupId, string businessDate, string frequency)
        {
            var json = string.Empty;
            try
            {
                if (CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    LaborDemand lstLaborDemand = _ILaborDemand.GetlaborDemandByWorkgroupId(sessionId, siteId, workgroupId, businessDate, frequency);
                    json = JsonConvert.SerializeObject(lstLaborDemand, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                }

                return json;
                // return "okay";
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
        [HttpGet]
        [Route("GetlaborDemandByLaborRoleId")]
        public string GetlaborDemandByLaborRoleId(string refSessionId, long siteId, long laborRoleId, string businessDate, string frequency)
        {
            var json = string.Empty;
            try
            {
                if (CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    LaborDemand lstLaborDemand = _ILaborDemand.GetlaborDemandByLaborRoleId(sessionId, siteId, laborRoleId, businessDate, frequency);
                    json = JsonConvert.SerializeObject(lstLaborDemand, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                }

                return json;
                // return "okay";
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }


        [HttpGet]
        [Route("GetLaborDemandDataFromMongoDB")]
        public string GetLaborDemandDataFromMongoDB(string refSessionId, string startDate, string endDate, [FromQuery] string[] siteId)
        {
            var json = string.Empty;
            string cacheKeyValue = "myval";
            try
            {
                if (CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    List<LaborDemandHourByDayVM> lstOrgunit = _ILaborDemand.GetLaborDemandDataFromMongoDB(sessionId, startDate, endDate, siteId);
                    List<LaborDemandHourByDayVM> lstorgUnitstoRemove = new List<LaborDemandHourByDayVM>();

                    foreach (var org in lstOrgunit)
                    {
                        var children = lstOrgunit.Where(e => e.data.parentUnitId == org.data.id).ToList();
                        if (children.Count > 0)
                        {
                            org.children = children;
                            foreach (var data in children)
                            {
                                lstorgUnitstoRemove.Add(data);
                            }
                        }
                    }
                    foreach (var orgunitremove in lstorgUnitstoRemove)
                    {

                        lstOrgunit.Remove(orgunitremove);
                    }

                    json = JsonConvert.SerializeObject(lstOrgunit, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                    var newJSON = "{ \"data\" : " + json + "}";
                    //json = JsonConvert.SerializeObject(response, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                    return newJSON;
                }
                //redisCacheHelper.SetCacheIntoData(cacheKeyValue, response);

                return json;

            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
    }
}
