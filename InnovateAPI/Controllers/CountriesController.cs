﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace InnovateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountriesController : BaseController
    {
        public ICountriesService _ICountries;
        string refSessionID = "_refSessionId";
        public CountriesController(ICountriesService countries, IConfiguration iconfiguration, ILoginUserService LoginUser)
            :base(iconfiguration, LoginUser)
        {
            _ICountries = countries;
        }

        [HttpGet]
        [Route("GetCountries")]
        public string GetCountries(string refSessionId)
        {
            var json = string.Empty;
            try
            {
               if(CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    List<Countries> lstcountries = _ICountries.GetCountries(sessionId);
                    json = JsonConvert.SerializeObject(lstcountries);
                }
               
                return json;
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }

        }
        [HttpGet]
        [Route("GetCountrieById")]
        public string GetCountriesById(string refSessionId, long countriesId)
        {
            var json = string.Empty;
            try
            {
                if(CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    List<Countries> lstcountries = _ICountries.GetCountriesById(sessionId, countriesId);
                     json = JsonConvert.SerializeObject(lstcountries);
                   
                }
                return json;
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
    }
}
