﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EasyCaching.Core;
using InnovateAPI.DataAccess;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace InnovateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SitesController : BaseController
    {
      
        readonly ISitesService _ISites;
        private RedisCacheHelper redisCacheHelper;
        string refSessionID = "_refSessionId";

        public SitesController(ISitesService sites, IConfiguration iconfiguration, ILoginUserService LoginUser, 
            IEasyCachingProviderFactory cachingProviderFactor)
            :base(iconfiguration,LoginUser)
        {
        
            redisCacheHelper = new RedisCacheHelper(cachingProviderFactor);
            _ISites = sites;
        }

        [HttpGet]
        [Route("GetSites")]
        public  string GetSites(string refSessionId)
        {
            var json = string.Empty;
            string cacheKeyValue = "sitesList";
            try
            {
                   
                if (CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    
                    string resultData = redisCacheHelper.GetCacheResponse(cacheKeyValue);
                    if (resultData != null)
                    {
                        return  resultData;
                    }

                    List<Sites> lstSites = _ISites.GetSites(sessionId);
                    json = JsonConvert.SerializeObject(lstSites, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });

                    redisCacheHelper.SetCacheIntoData(cacheKeyValue, json);
                  

                }
               
                return  json;
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        [HttpGet]
        [Route("GetSitesById")]
        public string GetSitesById(string refSessionId, long siteId)
        {
            var json = string.Empty;
            string cacheKeyValue = "site"+siteId.ToString().ToLower();
            try
            {
                if(CheckAuthentication(refSessionId))
                {
                    string resultData = redisCacheHelper.GetCacheResponse(cacheKeyValue);
                    if (resultData != null)
                    {
                        return resultData;
                    }

                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    List<Sites> lstSites = _ISites.GetSitesById(sessionId, siteId);
                    json = JsonConvert.SerializeObject(lstSites, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                    redisCacheHelper.SetCacheIntoData(cacheKeyValue, json);
                    // return "okay";
                }
                return json;
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        [HttpGet]
        [Route("GetSitesByMultipleId")]
        public string GetSitesByMultipleId(string refSessionId, [FromQuery] long[] siteId)
        {
            var json = string.Empty;
            string cacheKeyValue = "site";
            for (int i = 0; i < siteId.Length; i++)
            {
                cacheKeyValue = cacheKeyValue + siteId[i].ToString().ToLower();
            }
            try
            {
                if(CheckAuthentication(refSessionId))
                {
                    string resultData = redisCacheHelper.GetCacheResponse(cacheKeyValue);
                    if (resultData != null)
                    {
                        return resultData;
                    }
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    List<Sites> lstSites = _ISites.GetSitesByMultipleId(sessionId, siteId);
                    json = JsonConvert.SerializeObject(lstSites, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                    redisCacheHelper.SetCacheIntoData(cacheKeyValue, json);

                }
                return json;
                //return "okay";
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        [HttpGet]
        [Route("GetSitesByName")]
        public string GetSitesByName(string refSessionId, string siteName)
        {
            var json = string.Empty;
            string cacheKeyValue = siteName.ToString().ToLower();

            try
            {
                if(CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    string resultData = redisCacheHelper.GetCacheResponse(cacheKeyValue);
                    if (resultData != null)
                    {
                        return resultData;
                    }
                    List<Sites> lstSites = _ISites.GetSitesByName(sessionId, siteName);
                    json = JsonConvert.SerializeObject(lstSites, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                    redisCacheHelper.SetCacheIntoData(cacheKeyValue, json);
                }
                return json;
                // return "okay";
            }

            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        [HttpGet]
        [Route("GetSiteCurrentTimeInformation")]
        public string GetSiteCurrentTimeInformation(string refSessionId, long siteId)
        {
            var json = string.Empty;
            string cacheKeyValue = "siteCurrentTime" + siteId.ToString().ToLower();

            try
            {
                if(CheckAuthentication(refSessionId))
                {
                    string resultData = redisCacheHelper.GetCacheResponse(cacheKeyValue);
                    if (resultData != null)
                    {
                        return resultData;
                    }
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    SiteCurrentTimeInformation lstSites = _ISites.GetSiteCurrentTimeInformation(sessionId, siteId);
                    json = JsonConvert.SerializeObject(lstSites, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                    redisCacheHelper.SetCacheIntoData(cacheKeyValue, json);
                }
                return json;
                // return "okay";
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        [HttpGet]
        [Route("GetSiteByOrganizationalUnitId")]
        public string GetSiteByOrganizationalUnitId(string refSessionId, long organizationUnitId)
        {
            var json = string.Empty;
            string cacheKeyValue = "site"+ organizationUnitId.ToString().ToLower();

            try
            {
                if(CheckAuthentication(refSessionId))
                {
                    string resultData = redisCacheHelper.GetCacheResponse(cacheKeyValue);
                    if (resultData != null)
                    {
                        return resultData;
                    }
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    SiteOrganizationalUnit lstSites = _ISites.GetSiteByOrganizationalUnitId(sessionId, organizationUnitId);
                     json = JsonConvert.SerializeObject(lstSites, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                    redisCacheHelper.SetCacheIntoData(cacheKeyValue, json);

                }
                return json;
                // return "okay";
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
               
    }
}
