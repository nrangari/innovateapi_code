﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using InnovateAPI.Models;
using InnovateAPI.Interface;
using Newtonsoft.Json;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Configuration;

namespace InnovateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActualLaborCostController : BaseController
    {
        readonly IActualLaborCostsService _IActualLaborCostsService;


        
        string refSessionID = "_refSessionId";
        public ActualLaborCostController(IActualLaborCostsService atualLaborCost, IConfiguration iconfiguration, ILoginUserService LoginUser)
             : base(iconfiguration, LoginUser)
        {
            _IActualLaborCostsService = atualLaborCost;

        }

        [HttpGet]
        [Route("GetActualLaborCostsBySiteId")]
        public string GetActualLaborCostsBySiteId(string refSessionId, long siteId, string businessDate, string frequency)
        {
            var json = string.Empty;
            try
            {
                if (CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    ActualLaborCosts lstActualLaborCosts = _IActualLaborCostsService.GetActualLaborCostsBySiteId(sessionId, siteId, businessDate, frequency);
                    json = JsonConvert.SerializeObject(lstActualLaborCosts, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                }

                return json;
                // return "okay";
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
        [HttpGet]
        [Route("GetActualLaborCostsByEmployeeId")]
        public string GetActualLaborCostsByEmployeeId(string refSessionId, long siteId, long employeeId, string businessDate, string frequency)
        {
            var json = string.Empty;
            try
            {
                if (CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    ActualLaborCosts lstActualLaborCosts = _IActualLaborCostsService.GetActualLaborCostsByEmployeeId(sessionId, siteId, employeeId, businessDate, frequency);
                    json = JsonConvert.SerializeObject(lstActualLaborCosts, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                }

                return json;
                // return "okay";
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
    }
}
