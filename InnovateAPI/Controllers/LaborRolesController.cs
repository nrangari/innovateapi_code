﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson.Serialization.IdGenerators;
using Newtonsoft.Json;

namespace InnovateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LaborRolesController : BaseController
    {
        readonly ILaborRolesService _ILaborRolesService;



        string refSessionID = "_refSessionId";
        public LaborRolesController(ILaborRolesService laborRole, IConfiguration iconfiguration, ILoginUserService LoginUser)
             : base(iconfiguration, LoginUser)
        {
            _ILaborRolesService = laborRole;

        }

        [HttpGet]
        [Route("GetLaborRoleByRoleId")]
        public string GetLaborRoleByRoleId(string refSessionId, long laborRoleId)
        {
            var json = string.Empty;
            try
            {
                if (CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    LaborRoles lstLaborRole = _ILaborRolesService.GetLaborRoleByRoleId(sessionId, laborRoleId);
                    json = JsonConvert.SerializeObject(lstLaborRole, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                }

                return json;
                // return "okay";
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        [HttpGet]
        [Route("GetLaborRoleByRoleName")]
        public string GetLaborRoleByRoleName(string refSessionId, string laborRoleName)
        {
            var json = string.Empty;
            try
            {
                if (CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    LaborRoles lstLaborRole = _ILaborRolesService.GetLaborRoleByRoleName(sessionId, laborRoleName);
                    json = JsonConvert.SerializeObject(lstLaborRole, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                }

                return json;
                // return "okay";
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
    }
}
