﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InnovateAPI.Business;
using InnovateAPI.DataAccess;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace InnovateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IdealLaborHoursController : BaseController
    {
        string refSessionID = "_refSessionId";
        readonly IIdealLaborHoursService _IIIdealLaborHours;
        APILayer _apiLayer;
        MongoDBDataHelper _mdbHelper;
        IConfiguration _iconfiguration;
        public IdealLaborHoursController(IIdealLaborHoursService IIdealLaborHours, IConfiguration iconfiguration, ILoginUserService LoginUser)
            : base(iconfiguration, LoginUser)
        {
            _IIIdealLaborHours = IIdealLaborHours;
            _iconfiguration = iconfiguration;
            _apiLayer = new APILayer(_iconfiguration);
            _mdbHelper = new MongoDBDataHelper(_iconfiguration);
        }

        [HttpGet]
        [Route("GetidealLaborHoursByBusinessDate")]
        public string GetidealLaborHoursByBusinessDate(string refSessionId, long siteId, string businessDate, string frequency)
        {
            var json = string.Empty;
            try
            {
                if (CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    List<IdealLaborHours> lstlaborhours = _IIIdealLaborHours.GetidealLaborHoursByBusinessDate(sessionId, siteId, businessDate, frequency);

                    if (lstlaborhours.Count > 0)
                    {
                        json = JsonConvert.SerializeObject(lstlaborhours);
                    }
                    else
                    {
                        return json = JsonConvert.SerializeObject("Record not found.Please insert valide input.");                         
                    }
                    
                }

            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
            return json;

        }

        [HttpGet]
        [Route("GetidealLaborHoursByWorkGroupId")]
        public string GetidealLaborHoursByWorkGroupId(string refSessionId, long siteId, long workgroupId, string businessDate, string frequency = "")
        {
            var json = string.Empty;
            try
            {
                if (CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    List<IdealLaborHours> lstlaborhours = _IIIdealLaborHours.GetidealLaborHoursByWorkGroupId(sessionId, siteId, workgroupId, businessDate, frequency);
                    if (lstlaborhours.Count > 0)
                    {
                        json = JsonConvert.SerializeObject(lstlaborhours);
                    }
                    else
                    {
                        return json = JsonConvert.SerializeObject("Record not found.Please insert valide input.");
                    }
                }

            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
            return json;

        }
    }
}
