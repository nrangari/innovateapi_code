﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace InnovateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MetricsController : BaseController
    {
        public IMetricsService _IMetrics;
        IConfiguration _iconfiguration;
        readonly ILoginUserService _ILoginUser;
        string refSessionID = "_refSessionId";
        public MetricsController(IMetricsService metrics, IConfiguration iconfiguration, ILoginUserService LoginUser)
            :base(iconfiguration, LoginUser)
        {
            _IMetrics = metrics;
            _iconfiguration = iconfiguration;
            _ILoginUser = LoginUser;
        }
        [HttpGet]
        [Route("GetMetrics")]
        public string GetMetrics(string refSessionId, long metricsId)
        {
            var json = string.Empty;
            try
            {
                if(CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    List<Metrics> lstmetrics = _IMetrics.GetMetrics(sessionId, metricsId);
                    json = JsonConvert.SerializeObject(lstmetrics);
                }
                return json;
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
    }
}
