﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace InnovateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LanguagesController : BaseController
    {
        public ILanguagesService _IILanguages;
        string refSessionID = "_refSessionId";
        public LanguagesController(ILanguagesService languages, IConfiguration iconfiguration, ILoginUserService LoginUser)
             : base(iconfiguration, LoginUser)
        {
            _IILanguages = languages;
        }

        [HttpGet]
        [Route("GetLanguages")]
        public string GetLanguages(string refSessionId)
        {
            var json = string.Empty;
            try
            {
                if(CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    List<Languages> lstcountries = _IILanguages.GetLanguages(sessionId);
                     json = JsonConvert.SerializeObject(lstcountries);
                }               
                return json;
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
        [HttpGet]
        [Route("GetLanguageById")]
        public string GetLanguageById(string refSessionId, long languagesId)
        {
            var json = string.Empty;
            try
            {
                if(CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    List<Languages> lstlanguages = _IILanguages.GetLanguageById(sessionId, languagesId);
                    json = JsonConvert.SerializeObject(lstlanguages);
                }
                return json;
            }
              
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
    }
}
