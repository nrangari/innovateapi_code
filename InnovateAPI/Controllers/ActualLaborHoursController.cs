﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace InnovateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActualLaborHoursController : BaseController
    {
        IActualLaborHoursService _IActualLaborHoursService;
        string refSessionID = "_refSessionId";
        public ActualLaborHoursController(IActualLaborHoursService actualLaborHoursService, IConfiguration iconfiguration, ILoginUserService LoginUser)
              : base(iconfiguration, LoginUser)
        {
            _IActualLaborHoursService = actualLaborHoursService;
        }

        [HttpGet]
        [Route("GetActualLaborHoursByJobId")]
        public string GetActualLaborHoursByJobId(string refSessionId, long siteId, long jobId, string businessDate)
        {
            var json = string.Empty;
            if (CheckAuthentication(refSessionId))
            {
                var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                ActualLaborHours lstActualLaborHours = _IActualLaborHoursService.GetActualLaborHoursByJobId(sessionId, siteId, jobId, businessDate);
                json = JsonConvert.SerializeObject(lstActualLaborHours, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            }
            return json;
        }

        [HttpGet]
        [Route("GetActualLaborHoursBySiteId")]
        public string GetActualLaborHoursBySiteId(string refSessionId, long siteId, string businessDate)
        {
            var json = string.Empty;
            if (CheckAuthentication(refSessionId))
            {
                var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                ActualLaborHours lstActualLaborHours = _IActualLaborHoursService.GetActualLaborHoursBySiteId(sessionId, siteId, businessDate);
                json = JsonConvert.SerializeObject(lstActualLaborHours, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            }
            return json;
        }


        [HttpGet]
        [Route("GetActualLaborHoursByWorkgroupId")]
        public string GetActualLaborHoursByWorkgroupId(string refSessionId, long siteId, long workgroupId,  string businessDate, string frequency)
        {
            var json = string.Empty;
            if (CheckAuthentication(refSessionId))
            {
                var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                ActualLaborHours lstActualLaborHours = _IActualLaborHoursService.GetActualLaborHoursByWorkgroupId(sessionId, siteId, workgroupId, businessDate, frequency);
                json = JsonConvert.SerializeObject(lstActualLaborHours, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            }
            return json;
        }

        [HttpGet]
        [Route("GetActualLaborHoursByEmployeeId")]
        public string GetActualLaborHoursByEmployeeId(string refSessionId, long siteId, long employeeId, string businessDate, string frequency)
        {
            var json = string.Empty;
            if (CheckAuthentication(refSessionId))
            {
                var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);

                ActualLaborHours lstActualLaborHours = _IActualLaborHoursService.GetActualLaborHoursByEmployeeId(sessionId, siteId, employeeId, businessDate, frequency);
                json = JsonConvert.SerializeObject(lstActualLaborHours, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            }
            return json;
        }
    }
}
