﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using EasyCaching.Core;
using InnovateAPI.DataAccess;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using InnovateAPI.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;

namespace InnovateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrganizationalUnitsController : BaseController
    {
        readonly IOrganizationalUnitsService _IOrganizationalUnits;
        private RedisCacheHelper redisCacheHelper;
        private IConfiguration _iconfiguration;
        readonly ILoginUserService _ILoginUser;
        string refSessionID = "_refSessionId";
        //Testing
        public OrganizationalUnitsController(IOrganizationalUnitsService organizationalUnits, IConfiguration iconfiguration, ILoginUserService LoginUser,
             IEasyCachingProviderFactory cachingProviderFactor)
          : base(iconfiguration, LoginUser)
        {
            redisCacheHelper = new RedisCacheHelper(cachingProviderFactor);
            _IOrganizationalUnits = organizationalUnits;
            _iconfiguration = iconfiguration;
            _ILoginUser = LoginUser;
        }

        [HttpGet]
        [Route("GetOrganizationalUnits")]
        public string GetOrganizationalUnits(string refSessionId)
        {
            var json = string.Empty;
            try
            {
                if (CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    List<OrganizationalUnitsVM> lstOrgunit = _IOrganizationalUnits.GetOrganizationalUnits(sessionId);
                    List<OrganizationalUnitsVM> lstorgUnitstoRemove = new List<OrganizationalUnitsVM>();
                    foreach (var org in lstOrgunit)
                    {
                        var children = lstOrgunit.Where(e => e.data.parentUnitId == org.data.id).ToList();
                        if (children.Count > 0)
                        {
                            org.children = children;
                            foreach (var data in children)
                            {
                                lstorgUnitstoRemove.Add(data);
                            }
                        }
                    }
                    foreach (var orgunitremove in lstorgUnitstoRemove)
                    {
                        lstOrgunit.Remove(orgunitremove);
                    }
                    json = JsonConvert.SerializeObject(lstOrgunit, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                    var newJSON = "{ \"data\" : " + json + "}";
                    return newJSON;
                }
                else
                {
                    return json = "Connection attempt failed";
                }
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        [HttpGet]
        [Route("GetOrganizationalUnitsMetricData")]
        public string GetOrganizationalUnitsMetricData(string refSessionId, string startDate, string endDate, [FromQuery] string[] metricId)
        {
            var json = string.Empty;
            string cacheKeyValue = cacheKeyValue = startDate + endDate;
            if (metricId.Length > 0)
            {
                for (int i = 0; i < metricId.Length; i++)
                {
                    cacheKeyValue = cacheKeyValue + metricId[i];
                }
            }
           
            try
            {
                string resultData = redisCacheHelper.GetCacheResponse(cacheKeyValue);
                if (resultData != null)
                {
                    return resultData;
                }
                if (CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    List<OrganizationalUnitsVM> lstOrgunit = _IOrganizationalUnits.GetOrganizationalUnitsMetricData(sessionId,startDate,endDate, metricId);
                    List<OrganizationalUnitsVM> lstorgUnitstoRemove = new List<OrganizationalUnitsVM>();
                    foreach (var org in lstOrgunit)
                    {
                        var children = lstOrgunit.Where(e => e.data.parentUnitId == org.data.id).ToList();
                        if (children.Count > 0)
                        {
                            org.children = children;
                            foreach (var data in children)
                            {
                                lstorgUnitstoRemove.Add(data);
                            }
                        }
                    }
                    foreach (var orgunitremove in lstorgUnitstoRemove)
                    {
                        lstOrgunit.Remove(orgunitremove);
                    }
                    json = JsonConvert.SerializeObject(lstOrgunit, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                    var newJSON = "{ \"data\" : " + json + "}";
                    redisCacheHelper.SetCacheIntoData(cacheKeyValue, newJSON);
                    return newJSON;
                }
                else
                {
                    return json = "Connection attempt failed";
                }
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

       
    }
}
