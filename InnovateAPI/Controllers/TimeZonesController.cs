﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace InnovateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TimeZonesController : BaseController
    {
        public ITimeZoneService _ITimezone;
        string refSessionID = "_refSessionId";
        public TimeZonesController(ITimeZoneService timezone, IConfiguration iconfiguration, ILoginUserService LoginUser)
            : base(iconfiguration, LoginUser)
        {
            _ITimezone = timezone;
        }

        [HttpGet]
        [Route("GetTimeZone")]
        public string GetTimeZone(string refSessionId)
        {
            var json = string.Empty;
            try
            {
                if(CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    List<TimeZones> lsttimezone = _ITimezone.GetTimeZones(sessionId);
                    json = JsonConvert.SerializeObject(lsttimezone);                  
                }
                return json;
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
    }
}
