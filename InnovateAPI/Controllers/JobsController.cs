﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InnovateAPI.Business;
using InnovateAPI.DataAccess;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace InnovateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobsController : BaseController
    {
        string refSessionID = "_refSessionId";
        readonly IJobsService _IJobs;
        APILayer _apiLayer;
        MongoDBDataHelper _mdbHelper;
        IConfiguration _iconfiguration;
        public JobsController(IJobsService IJobs, IConfiguration iconfiguration, ILoginUserService LoginUser)
            : base(iconfiguration, LoginUser)
        {
            _IJobs = IJobs;
            _iconfiguration = iconfiguration;
            _apiLayer = new APILayer(_iconfiguration);
            _mdbHelper = new MongoDBDataHelper(_iconfiguration);

        }
        /// <summary>
        /// Get Job Details By JobId
        /// </summary>

        [HttpGet]
        [Route("GetJobsById")]
        public string GetJobsById(string refSessionId, long jobId)
        {
            var json = string.Empty;
            try
            {
                if (CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    List<Jobs> lstjobs = _IJobs.GetJobsById(sessionId, jobId);
                    if (lstjobs.Count > 0)
                    {
                        json = JsonConvert.SerializeObject(lstjobs);
                    }
                    else
                    {
                        return json = JsonConvert.SerializeObject("Record not found.Please insert valide input.");
                    }
                }

            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
            return json;

        }

        /// <summary>
        /// Get Job Details By Name
        /// </summary>
        [HttpGet]
        [Route("GetJobsByName")]
        public string GetJobsByName(string refSessionId, string name)
        {
            var json = string.Empty;
            try
            {
                if (CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    List<Jobs> lstjobs = _IJobs.GetJobsByName(sessionId, name);
                    if (lstjobs.Count > 0)
                    {
                        json = JsonConvert.SerializeObject(lstjobs);
                    }
                    else
                    {
                        return json = JsonConvert.SerializeObject("Record not found.Please insert valide input.");
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
            return json;

        }

        /// <summary>
        /// Get Job Details By SiteId
        /// </summary>
        [HttpGet]
        [Route("GetJobsBySiteId")]
        public string GetJobsBySiteId(string refSessionId, long siteId)
        {
            var json = string.Empty;
            try
            {
                if (CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    List<Jobs> lstjobs = _IJobs.GetJobsBySiteId(sessionId, siteId);
                    if (lstjobs.Count > 0)
                    {
                        json = JsonConvert.SerializeObject(lstjobs);
                    }
                    else
                    {
                        return json = JsonConvert.SerializeObject("Record not found.Please insert valide input.");
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
            return json;
        }

    }
}
