﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InnovateAPI.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace InnovateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        IConfiguration _iconfiguration;
        readonly ILoginUserService _ILoginUser;
        string refSessionID = "_refSessionId";
        public BaseController( IConfiguration iconfiguration, ILoginUserService LoginUser)
        {           
            _iconfiguration = iconfiguration;
            _ILoginUser = LoginUser;
        }

        [HttpGet]
        [Route("SetSession")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public bool SetSession()
        {
            try
            {
                var userName = _iconfiguration["LoginName"];
                var password = _iconfiguration["Password"];
                var _response = _ILoginUser.LoginUser(userName, password);
                if (!string.IsNullOrEmpty(_response.REFSessionId))
                {
                    HttpContext.Session.SetString(refSessionID, _response.REFSessionId);
                    return true;
                }
                else { return false; }           
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
                return false;
            }

        }

        [HttpGet]
        [Route("Authentication")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public bool CheckAuthentication(string RefsessionId)
        {
            var sessionId = RefsessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
            if (!string.IsNullOrEmpty(sessionId))
                return true;
            else 
            { bool result = SetSession();
                return result;          
            }                  
        }
    }
}
