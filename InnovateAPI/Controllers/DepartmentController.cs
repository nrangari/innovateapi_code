﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InnovateAPI.Business;
using InnovateAPI.DataAccess;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace InnovateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentController : BaseController
    {

        public IDepartmentService _IDept;
        string refSessionID = "_refSessionId";
        APILayer _apiLayer;
        MongoDBDataHelper _mdbHelper;
        IConfiguration _iconfiguration;

        public DepartmentController(IDepartmentService department, IConfiguration iconfiguration, ILoginUserService LoginUser)
             : base(iconfiguration, LoginUser)
        {
            _IDept = department;
            _iconfiguration = iconfiguration;
            _apiLayer = new APILayer(_iconfiguration);
            _mdbHelper = new MongoDBDataHelper(_iconfiguration);
        }

        [HttpGet]
        [Route("GetDepartmentDataById")]
        public string GetDepartmentDataById(string refSessionId, long id)
        {
            var json = string.Empty;
            try
            {
                if (CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    List<Department> lstDept = _IDept.GetDepartmentDataById(sessionId, id);
                    if (lstDept.Count > 0)
                    {
                        json = JsonConvert.SerializeObject(lstDept);
                    }
                    else
                    {
                        return json = JsonConvert.SerializeObject("Record not found.Please insert valide input.");
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
            return json;
        }

        [HttpGet]
        [Route("GetDepartmentDataByName")]
        public string GetDepartmentDataByName(string refSessionId, string name)
        {
            var json = string.Empty;
            try
            {
                if (CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    List<Department> lstDept = _IDept.GetDepartmentDataByName(sessionId, name);
                    if (lstDept.Count > 0)
                    {
                        json = JsonConvert.SerializeObject(lstDept);
                    }
                    else
                    {
                        return json = JsonConvert.SerializeObject("Record not found.Please insert valide input.");
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
            return json;
        }

        [HttpGet]
        [Route("GetDepartmentDataBySiteId")]
        public string GetDepartmentDataBySiteId(string refSessionId, long siteId)
        {
            var json = string.Empty;
            try
            {
                if (CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    List<Department> lstDept = _IDept.GetDepartmentDataBySiteId(sessionId, siteId);
                    if (lstDept.Count > 0)
                    {
                        json = JsonConvert.SerializeObject(lstDept);
                    }
                    else
                    {
                        return json = JsonConvert.SerializeObject("Record not found.Please insert valide input.");
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
            return json;
        }
    }
}
