﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InnovateAPI.Business;
using InnovateAPI.DataAccess;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace InnovateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobTitleController : BaseController
    {
        public IJobTitleService _IJobTitle;
        string refSessionID = "_refSessionId";
        APILayer _apiLayer;
        MongoDBDataHelper _mdbHelper;
        IConfiguration _iconfiguration;

        public JobTitleController(IJobTitleService jobTitle, IConfiguration iconfiguration, ILoginUserService LoginUser)
             : base(iconfiguration, LoginUser)
        {
            _IJobTitle = jobTitle;
            _iconfiguration = iconfiguration;
            _apiLayer = new APILayer(_iconfiguration);
            _mdbHelper = new MongoDBDataHelper(_iconfiguration);
        }

        [HttpGet]
        [Route("GetJobTitleData")]
        public string GetJobTitleData(string refSessionId)
        {
            var json = string.Empty;
            try
            {
                if (CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    List<JobTitle> lstJobTitle = _IJobTitle.GetJobTitleData(sessionId);
                    if (lstJobTitle.Count > 0)
                    {
                        json = JsonConvert.SerializeObject(lstJobTitle);
                    }
                    else
                    {
                        return json = JsonConvert.SerializeObject("Record not found.Please insert valide input.");
                    }
                }

            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
            return json;
        }

        [HttpGet]
        [Route("GetJobTitleDataByTitleId")]
        public string GetJobTitleDataByTitleId(string refSessionId, long titleId)
        {
            var json = string.Empty;
            try
            {
                if (CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    List<JobTitle> lstJobTitle = _IJobTitle.GetJobTitleDataByTitleId(sessionId, titleId);
                    if (lstJobTitle.Count > 0)
                    {
                        json = JsonConvert.SerializeObject(lstJobTitle);
                    }
                    else
                    {
                        return json = JsonConvert.SerializeObject("Record not found.Please insert valide input.");
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
            return json;
        }

        [HttpGet]
        [Route("GetJobTitleDataByName")]
        public string GetJobTitleDataByName(string refSessionId, string name)
        {
            var json = string.Empty;
            try
            {
                if (CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    List<JobTitle> lstJobTitle = _IJobTitle.GetJobTitleDataByName(sessionId, name);
                    if (lstJobTitle.Count > 0)
                    {
                        json = JsonConvert.SerializeObject(lstJobTitle);
                    }
                    else
                    {
                        return json = JsonConvert.SerializeObject("Record not found.Please insert valide input.");
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
            return json;
        }

        [HttpGet]
        [Route("GetJobTitleDataBySiteId")]
        public string GetJobTitleDataBySiteId(string refSessionId, long siteId)
        {
            var json = string.Empty;
            try
            {
                if (CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    List<JobTitle> lstJobTitle = _IJobTitle.GetJobTitleDataBySiteId(sessionId, siteId);
                    if (lstJobTitle.Count > 0)
                    {
                        json = JsonConvert.SerializeObject(lstJobTitle);
                    }
                    else
                    {
                        return json = JsonConvert.SerializeObject("Record not found.Please insert valide input.");
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
            return json;
        }
    }
}
