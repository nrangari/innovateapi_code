﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using InnovateAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace InnovateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrganizationalHierarchyLevelsController : BaseController
    {
        public IOrganizationalHierarchyLevelService _IOrganizationalHierarchyLevel;
        string refSessionID = "_refSessionId";
        public OrganizationalHierarchyLevelsController(IOrganizationalHierarchyLevelService organizationalHierarchyLevel , IConfiguration iconfiguration, ILoginUserService LoginUser)
            :base(iconfiguration,LoginUser)
        {
            _IOrganizationalHierarchyLevel = organizationalHierarchyLevel;
        }

        [HttpGet]
        [Route("GetOrganizationalHierarchyLevels")]
        public string GetOrganizationalHierarchyLevels(string refSessionId)
        {
            var json = string.Empty;
            try
            {
                if(CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    List<OrganizationalHierarchyLevels> lstOrgLevels = _IOrganizationalHierarchyLevel.GetOrganizationalHierarchyLevels(sessionId);
                    json = JsonConvert.SerializeObject(lstOrgLevels);
                }
                return json;
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();           
            }               
        }
    }
}
