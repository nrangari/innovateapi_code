﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace InnovateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MetricsGroupsController : BaseController
    {
        public IMetricsGroupsService _IMetricsGroups;
        IConfiguration _iconfiguration;
        readonly ILoginUserService _ILoginUser;
        string refSessionID = "_refSessionId";
        public MetricsGroupsController(IMetricsGroupsService metricsGroups, IConfiguration iconfiguration, ILoginUserService LoginUser) 
            : base(iconfiguration, LoginUser)
        {
            _IMetricsGroups = metricsGroups;
            _iconfiguration = iconfiguration;
            _ILoginUser = LoginUser;
        }
        [HttpGet]
        [Route("GetMetricsGroup")]
        public string GetMetricsGroup(string refSessionId, long metricsGroupsId)
        {
            var json = string.Empty;
            try
            {
                if(CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    List<MetricsGroups> lstmetrics = _IMetricsGroups.MetricsGroups(sessionId, metricsGroupsId);
                    json = JsonConvert.SerializeObject(lstmetrics);
                   
                }
                return json;
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
    }
}
