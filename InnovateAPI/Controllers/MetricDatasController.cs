﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EasyCaching.Core;
using InnovateAPI.DataAccess;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace InnovateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MetricDatasController : BaseController
    {
        readonly IMetricDatasService _IMetricDatas;
        private RedisCacheHelper redisCacheHelper;
        string refSessionID = "_refSessionId";
        public MetricDatasController(IMetricDatasService metricData, IConfiguration iconfiguration, ILoginUserService LoginUser,
            IEasyCachingProviderFactory cachingProviderFactor)
             : base(iconfiguration, LoginUser)
        {
            redisCacheHelper = new RedisCacheHelper(cachingProviderFactor);
            _IMetricDatas = metricData;

        }
        [HttpGet]
        [Route("GetMetricData")]
        public string GetMetricData(string startDate, string endDate)
        {
            var json = string.Empty;
            string cacheKeyValue = "myval";
            try
            {

                //string resultData = redisCacheHelper.GetCacheResponse(cacheKeyValue);
                //if (resultData != null)
                //{
                //    return resultData;
                //}

                var response = _IMetricDatas.GetMetricDatas(startDate,endDate);
                json = JsonConvert.SerializeObject(response, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                //redisCacheHelper.SetCacheIntoData(cacheKeyValue, response);

                return json;
              
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
      
    }
}
