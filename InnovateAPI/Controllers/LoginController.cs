﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using InnovateAPI.Models.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace InnovateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        ResponseResult _response;
        readonly ILoginUserService _ILoginUser;
        IConfiguration _iconfiguration;
        public LoginController(ILoginUserService LoginUser, IConfiguration iconfiguration)
        {
            _ILoginUser = LoginUser;
            _iconfiguration = iconfiguration;
        }
        [HttpGet]
        public ResponseResult LoginUser()
        {
            var userName = _iconfiguration["LoginName"];
            var password = _iconfiguration["Password"];
            _response = _ILoginUser.LoginUser(userName, password);
            return _response;
        }
    }
}
