﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace InnovateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ForecastGroupsController : BaseController
    {
        readonly IForecastGroupsServeice _IForecastGroupsServeice;
        string refSessionID = "_refSessionId";
        public ForecastGroupsController(IForecastGroupsServeice forecastGroups, IConfiguration iconfiguration, ILoginUserService LoginUser)
             : base(iconfiguration, LoginUser)
        {
            _IForecastGroupsServeice = forecastGroups;
        }

        [HttpGet]
        [Route("GetForecoastGroupById")]
        public string GetForecoastGroupById(string refSessionId, long ForecastId)
        {
            var json = string.Empty;
            try
            {
                if (CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    ForecastGroups lstForecast = _IForecastGroupsServeice.GetForecoastGroupById(sessionId, ForecastId);
                    json = JsonConvert.SerializeObject(lstForecast, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                }
                return json;
                // return "okay";
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        [HttpGet]
        [Route("GetForecoastGroupByName")]
        public string GetForecoastGroupByName(string refSessionId, string ForecastName)
        {
            var json = string.Empty;
            try
            {
                if (CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    ForecastGroups lstForecast = _IForecastGroupsServeice.GetForecoastGroupByName(sessionId, ForecastName);
                    json = JsonConvert.SerializeObject(lstForecast, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                }
                return json;
                // return "okay";
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
    }
}
