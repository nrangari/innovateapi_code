﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InnovateAPI.Interface;
using InnovateAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace InnovateAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ScheduledLaborCostController : BaseController
    {
        readonly IScheduledLaborCostService _IScheduledLaborCostService;

      
        string refSessionID = "_refSessionId";
        public ScheduledLaborCostController(IScheduledLaborCostService scheduleLaborCost, IConfiguration iconfiguration, ILoginUserService LoginUser)
           : base(iconfiguration, LoginUser)
        {
            _IScheduledLaborCostService = scheduleLaborCost;

        }
        [HttpGet]
        [Route("GetScheduledLaborCostBySiteId")]
        public string GetScheduledLaborCostBySiteId(string refSessionId, long siteId, string businessDate,string frequency)
        {
            var json = string.Empty;
            try
            {
                if (CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    ScheduledLaborCost lstScheduledCost = _IScheduledLaborCostService.GetScheduledLaborCostBySiteId(sessionId, siteId, businessDate, frequency);
                    json = JsonConvert.SerializeObject(lstScheduledCost, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                }

                return json;
                // return "okay";
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        [HttpGet]
        [Route("GetScheduledLaborCostByEmployeeId")]
        public string GetScheduledLaborCostByEmployeeId(string refSessionId, long siteId, long employeeId, string businessDate,string frequency)
        {
            var json = string.Empty;
            try
            {
                if (CheckAuthentication(refSessionId))
                {
                    var sessionId = refSessionId != null ? refSessionID : HttpContext.Session.GetString(refSessionID);
                    ScheduledLaborCost lstScheduledCost = _IScheduledLaborCostService.GetScheduledLaborCostByEmployeeId(sessionId, siteId, employeeId, businessDate, frequency);
                    json = JsonConvert.SerializeObject(lstScheduledCost, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                }

                return json;
                // return "okay";
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
    }
}
