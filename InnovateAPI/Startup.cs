using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EasyCaching.Core.Configurations;
using InnovateAPI.Controllers;
using InnovateAPI.Interface;
using InnovateAPI.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;

namespace InnovateAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddEasyCaching(options =>
            {
                options.UseInMemory("default");
                options.UseInMemory(config =>
                {
                     //     config.DBConfig = new InMemoryCachingOptions
                     //     {
                     //         // scan time, default value is 60s
                     //         ExpirationScanFrequency = 60, 
                     //         // total count of cache items, default value is 10000
                     //         SizeLimit = 100 
                     //     };
                     //     // the max random second will be added to cache's expiration, default value is 120
                     //     config.MaxRdSecond = 120;
                     //     // whether enable logging, default is false
                     //     config.EnableLogging = false;
                     //     // mutex key's alive time(ms), default is 5000
                         config.LockMs = 15000;
                     //     // when mutex key alive, it will sleep some time, default is 300
                     //     config.SleepMs = 300;
                      });
                     options.UseRedis(redisConfig =>
                {
                    
                    redisConfig.DBConfig.Endpoints.Add(new ServerEndPoint(Configuration.GetValue<string>("RedisHost"), Configuration.GetValue<int>("RedisPort")));
                    //redisConfig.DBConfig.Password = "Admin2021";
                    redisConfig.DBConfig.AllowAdmin = true;
                },
              "redis1");
            });

            services.AddDistributedMemoryCache();
            services.AddSession(options => {
                options.IdleTimeout = TimeSpan.FromMinutes(1);//You can set Time   
            });
            services.AddCors(c =>
            {
                c.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin());
            });
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "InnovateAPI", Version = "v1", Description = "InnovateAPI" });
            });
            services.AddSingleton<IConfiguration>(Configuration);
            services.AddScoped<ILoginUserService, LoginUserService>();
            services.AddScoped<IOrganizationalUnitsService, OrganizationalUnitsService>();
            services.AddScoped<IOrganizationalHierarchyLevelService, OrganizationalHierarchyLevelService>();
            services.AddScoped<ISitesService, SitesService>();
            services.AddScoped<ICountriesService, CountriesService>();
            services.AddScoped<ILanguagesService, LanguagesService>();
            services.AddScoped<IActualLaborCostsService, ActualLaborCostsService>();
            services.AddScoped<IIsoLanguagesService, IsoLanguagesService>();
            services.AddScoped<ITimeZoneService, TimeZoneService>();
            services.AddScoped<IMetricsService, MetricsService>();
            services.AddScoped<IMetricsGroupsService, MetricsGroupsService>();
            services.AddScoped<IJobsService, JobsService>();
            services.AddScoped<IJobTitleService, JobTitleService>();
            services.AddScoped<IDepartmentService, DepartmentService>();
            services.AddScoped<IActualLaborHoursService, ActualLaborHoursService>();
            services.AddScoped<IForecastGroupsServeice, ForecastGroupsService>();
            services.AddScoped<IIdealLaborHoursService, IdealLaborHoursService>();
            services.AddScoped<IScheduledLaborCostService, ScheduledLaborCostService>();
            services.AddScoped<ILaborDemandService, LaborDemandService>();
            services.AddScoped<ILaborRolesService, LaborRolesService>();
            services.AddScoped<IMetricDatasService, MetricDatasService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors(options => options.AllowAnyOrigin());
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSession();
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json","V1");
            });
        }
    }
}
