﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.IPlugin
{
    public interface InnovateInterface
    {
        //Expose common Methods for Plugin Like Blue Yonder,Other Server  
        dynamic GetLoginAPIResponse(string serverUrl, string apiUrl, string userName, string password);
        dynamic GetDataResponseFromAPI(string serverUrl, string apiUrl, string refSessionId);

        //IEnumerable<object> GetActualLaborCostsBySiteId(string serverUrl, string apiUrl,string refSessionId, long siteId, string businessDate);
        //IEnumerable<object> GetActualLaborCostsByEmployeeId(string serverUrl, string apiUrl,string refSessionId, long siteId, long employeeId, string businessDate);
        //IEnumerable<object> GetCountries(string serverUrl, string apiUrl,string refSessionId);
        //IEnumerable<object> GetLanguages(string serverUrl, string apiUrl, string refSessionId);
        //IEnumerable<object> GetOrganizationalHierarchyLevels(string serverUrl, string apiUrl,string refSessionId);
        //IEnumerable<object> GetOrganizationalUnits(string serverUrl, string apiUrl, string refSessionId);
        //IEnumerable<object> GetSitesById(string serverUrl, string apiUrl, string refSessionId, long siteId);
        //IEnumerable<object> GetSitesByMultipleId(string serverUrl, string apiUrl, string refSessionId, long[] siteId);
        //IEnumerable<object> GetSitesByName(string serverUrl, string apiUrl, string refSessionId, string siteName);
        //object GetSiteCurrentTimeInformation(string serverUrl, string apiUrl, string refSessionId, long siteId);
        //object GetSiteByOrganizationalUnitId(string serverUrl, string apiUrl, string refSessionId, long organizationUnitId);
    }
}
