﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Models
{
    public class ActualLaborCosts
    {
        public ObjectId Id { get; set; }
       
        public long siteId { get; set; }
        public long? employeeId { get; set; }
        public string businessDate { get; set; }
        public string frequencyCode { get; set; }
        public List<ActualLaborCostByDay> actualLaborCostByDay { get; set; }
        public List<ActualLaborCostByHour> actualLaborCostByHour { get; set; }
    }

    public class ActualLaborCostByDay
    {
        public long? siteId { get; set; }
        public long employeeId { get; set; }
        public string businessDate { get; set; }
        public double actualLaborCost { get; set; }
    }
   
    public class ActualLaborCostByHour
    {
        public long? siteId { get; set; }
        public long employeeId { get; set; }
        public string businessDate { get; set; }
        public string startTime { get; set; }
        public string endTime { get; set; }
        public double actualLaborCost { get; set; }

    }
}
