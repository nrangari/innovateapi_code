﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Models
{
    public class OrganizationalHierarchyLevels
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }

    public class OrgHierarchyLevels
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long Level { get; set; }
    }

}
