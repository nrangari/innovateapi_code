﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Models.Common
{
    public static class Constants
    {
       

        public static string LoginAPI = "/retail/data/login";


        ///<summary>
        ///  Organizational Unit  Data Blue Yonder API Url
        /// </summary>
        public static string orgUnitsDataAPI = $"retail/data/retailwebapi/api/v1/organizationalUnits";

        ///<summary>
        /// Organizational Hierarchy Level Data Blue Yonder API Url
        /// </summary>
        public static string orgHierarchyLevelsDataAPI = $"retail/data/retailwebapi/api/v1/organizationalHierarchyLevels";


        public static string sitesDataAPI = $"retail/data/retailwebapi/api/v1/Sites";

        /// <summary>
        /// Countries Data From Blue Yonder API Url
        /// </summary>

        public static string CountriesDataAPI = $"retail/data/retailwebapi/api/v1/Countries";
        /// <summary>
        /// Langaues Data From Blue Younder API Url 
        /// </summary>
        public static string LanguagesDataAPI = $"retail/data/retailwebapi/api/v1/Languages";

        /// <summary>
        /// Langaues Data From Blue Younder API Url 
        /// </summary>
        public static string ActualLaborCostjdaClientAPI = $"retail/data/retailwebapi/api/v1/actualLaborCost/";

        public static string IsoLangauesDataUrl = $"retail/data/retailwebapi/api/v1/isoLanguages/";

        /// <summary>
        /// Time Zone Data From Blue Younder API Url 
        /// </summary>
        public static string TimeZonesDataUrl = $"retail/data/retailwebapi/api/v1/TimeZones";

        /// <summary>
        /// LanguagesbyId Data From Blue Younder API Url 
        /// </summary>
        public static string LanguagesByIdDataUrl = $"retail/data/retailwebapi/api/v1/Languages/";

        /// <summary>
        /// CountriesbyId Data From Blue Younder API Url 
        /// </summary>
        public static string CountriesByIdDataUrl = $"retail/data/retailwebapi/api/v1/Countries/";

        /// <summary>
        /// Metrices Data From Blue Younder API Url 
        /// </summary>
        public static string MetricsDataUrl = $"retail/data/retailwebapi/api/v1/metrics/";

        /// <summary>
        /// MetricesGroupsId Data From Blue Younder API Url 
        /// </summary>
        public static string MetricsGroupsDataUrl = $"retail/data/retailwebapi/api/v1/metricGroups/";

        #region Jobs API url


        /// <summary>
        /// Get Jobs By Id Data Blue Yonder API Url
        /// </summary>
        public static string jobsDataByIdUrl = $"retail/data/retailwebapi/api/v1/jobs/";
        /// <summary>
        /// Get Jobs By Name Data Blue Yonder API Url
        /// </summary>
        public static string jobsDataByNameUrl = $"retail/data/retailwebapi/api/v1/jobs?name=";
        /// <summary>
        /// Get Jobs By SiteIdFilter Data Blue Yonder API Url
        /// </summary>
        public static string jobsDataBySiteIdUrl = $"retail/data/retailwebapi/api/v1/jobs?siteIdFilter=";
        #endregion

        #region JobTitle API url        

        /// <summary>
        /// Get Job Titles resource Data Blue Yonder API Url
        /// </summary>
        public static string jobTitleDataUrl = $"retail/data/retailwebapi/api/v1/jobTitles";

        /// <summary>
        /// Get Job Titles resource Data By Id  Blue Yonder API Url
        /// </summary>
        public static string jobTitleDataByTitleIdUrl = $"retail/data/retailwebapi/api/v1/jobTitles/";

        /// <summary>
        /// Get Job Titles resource Data By Name Blue Yonder API Url
        /// </summary>
        public static string jobTitleDataByNameUrl = $"retail/data/retailwebapi/api/v1/jobTitles?name=";

        /// <summary>
        /// Get Job Titles resource Data By SiteId  Blue Yonder API Url
        /// </summary>
        public static string jobTitleDataBySiteIdUrl = $"retail/data/retailwebapi/api/v1/jobTitles/site/";
        #endregion

        #region Department API url

        /// <summary>
        /// Get Department Data By Id  Blue Yonder API Url
        /// </summary>
        public static string departmentDataByIdUrl = $"retail/data/retailwebapi/api/v1/departments/";

        /// <summary>
        /// Get Department Data By Site Id  Blue Yonder API Url
        /// </summary>
        public static string departmentDataBySiteIdUrl = $"retail/data/retailwebapi/api/v1/departments?siteIdFilter=";

        /// <summary>
        /// Get Department Data By Name Blue Yonder API Url
        /// </summary>
        public static string departmentDataByNameUrl = $"retail/data/retailwebapi/api/v1/departments?name=";

        #endregion


        /// <summary>
        /// ForecastGroup Data From Blue Younder API Url 
        /// </summary>
        public static string ForecastGrouptjdaClientAPI = $"retail/data/retailwebapi/api/v1/forecastGroups";
        /// <summary>
        /// ForecastGroup Data From Blue Younder API Url 
        /// </summary>
        public static string ActualLaborHours = $"retail/data/retailwebapi/api/v1/actualLaborHours";

        #region idealLaborHours
        public static string idealLaborHoursDataUrl = $"retail/data/retailwebapi/api/v1/idealLaborHours/";

        #endregion

        /// <summary>
        /// ScheduledLaborCost Data From Blue Younder API Url 
        /// </summary>
        public static string ScheduledLaborCost = $"retail/data/retailwebapi/api/v1/scheduledLaborCost";

        /// <summary>
        /// LaborDemand Data From Blue Younder API Url 
        /// </summary>
        public static string LaborDemandAPI = $"retail/data/retailwebapi/api/v1/laborDemand";

        /// <summary>
        /// LaborRoles Data From Blue Younder API Url 
        /// </summary>
        public static string LaborRolesAPI = $"retail/data/retailwebapi/api/v1/laborRoles";

        public static string MetricDataAPI = $"retail/data/retailwebapi/api/v1/metricData";

    }
}
