﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Models
{
   public class Department
    {
        public long departmentId { get; set; }
        public string name { get; set; }
        public long siteIdFilter { get; set; }
    }
}
