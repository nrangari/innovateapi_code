﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Models
{
   public class JobTitle
    {
        public long jobTitleId { get; set; }
        public string name { get; set; }
        public long siteId { get; set; }
    }
}
