﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Models
{
   public class ScheduledLaborCost
    {
        public long? SiteId { get; set; }
        public long? EmployeeId { get; set; }
        public string BusinessDate { get; set; }
        public string FrequencyCode { get; set; }
      
       public List<ScheduledLaborCostByDay> ScheduledLaborCostByDay { get; set; }
      public List<ScheduledLaborCostByHour> ScheduledLaborCostByHour { get; set; }


    }

    public class ScheduledLaborCostByDay
    {
        public long? SiteId { get; set; }
        public long? EmployeeId { get; set; }
        public string BusinessDate { get; set; }
        public string ScheduledLaborCost { get; set; }
    }
    public class ScheduledLaborCostByHour
    {
        public long? SiteId { get; set; }
        public long? EmployeeId { get; set; }
        public string BusinessDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string ScheduledLaborCost { get; set; }
    }
  
}
