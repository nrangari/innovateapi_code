﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Models
{
    public class Languages
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long IsoLanguageId { get; set; }
    }
}
