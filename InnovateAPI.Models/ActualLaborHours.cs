﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Models
{
    public class ActualLaborHours
    {
        public long? JobId { get; set; }
        public long? JobTitleId { get; set; }
        public long? SiteId { get; set; }
        public long? WorkgroupId { get; set; }
        public long? EmployeeId { get; set; }
        public string BusinessDate { get; set; }
        public string FrequencyCode { get; set; }

        public List<ActualLaborHourByDay> ActualLaborHourByDay { get; set; }
        public List<ActualLaborHourByHour> ActualLaborHourByHour { get; set; }

    }
    public class ActualLaborHourByDay
    {
        public long? SiteId { get; set; }
        public long? EmployeeId { get; set; }
        public long? JobId { get; set; }
        public long? JobTitleId { get; set; }
        public long? WorkgroupId { get; set; }
        public string BusinessDate { get; set; }
        public string ActualLaborHours { get; set; }
    }
    public class ActualLaborHourByHour
    {
        public long? SiteId { get; set; }
        public long? EmployeeId { get; set; }
        public long? JobId { get; set; }
        public long? JobTitleId { get; set; }
        public long? WorkgroupId { get; set; }
        public string BusinessDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string ActualLaborHours { get; set; }
    }
}
