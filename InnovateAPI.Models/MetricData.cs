﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Models
{
    public class MetricData
    {
      public ObjectId Id { get; set; }
      public string siteId { get; set; }
      public string metricId { get; set; }
      public DateTime businessDate { get; set; }
      public long metricValue { get; set; }
       
    }
    public class MetricDatas
    {
        public string Id { get; set; }
        public string siteId { get; set; }
        public string metricId { get; set; }
        public DateTime businessDate { get; set; }
        public long metricValue { get; set; }
      
    }

    public class WeekMatricData
    {
     public string MetricId { get; set; }
     public long MetricValue { get; set; }
     public string Store { get; set; }
      

    }
    public class MetricCollection
    { 
        public List<WeekMatricData> WeekMatric { get; set; }
        public long Year { get; set; }
        public long Week { get; set; }
        public long Quarter { get; set; }
        public int Month { get; set; }
        
    }

    public class MatricDataOG
    {
        public long MetricValue { get; set; }
        public string Store { get; set; }
        public long Year { get; set; }
        public long Week { get; set; }
        public long Quarter { get; set; }
        public int Month { get; set; }
        public string Metric { get; set; }
    }

    public class MetricCalendar
    {
        public ObjectId Id { get; set; }
        public long BudgetWeekId { get; set; }
        public long Year { get; set; }
        public long Week { get; set; }
        public long Quarter { get; set; }
        public int Month { get; set; }
        public DateTime Start_date { get; set; }
        public DateTime End_date { get; set; }
        

    }


}
