﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Models
{
    public class MetricsGroups
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
