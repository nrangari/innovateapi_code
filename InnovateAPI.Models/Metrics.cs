﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Models
{
    public class Metrics
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string ExternalName { get; set; }
    }
}
