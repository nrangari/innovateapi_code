﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Models
{
   public class IdealLaborHours
    {
        public long SiteId { get; set; }
        public long WorkgroupId { get; set; }
        public long LaborRoleId { get; set; }
        public string BusinessDate { get; set; }
        public string Frequency { get; set; }
        public string FrequencyCode { get; set; }
        public List<IdealLaborHoursByDay> IdealLaborHoursByDay { get; set; }
        public List<IdealLaborHoursByQuarterHour> IdealLaborHoursByQuarterHour { get; set; }

    }

    public class IdealLaborHoursByDay
    {
        public long? SiteId { get; set; }
        public long WorkgroupId { get; set; }
        public long LaborRoleId { get; set; }
        public string BusinessDate { get; set; }
        public double IdealLaborHours { get; set; }

    }

    public class IdealLaborHoursByQuarterHour
    {
        public long? SiteId { get; set; }
        public long WorkgroupId { get; set; }
        public long LaborRoleId { get; set; }
        public string BusinessDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public double IdealLaborHours { get; set; }

    }
}
