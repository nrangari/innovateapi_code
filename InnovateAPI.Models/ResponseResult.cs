﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Models
{
    public class ResponseResult
    {
        public string returnStatus { get; set; }
        public int HttpStatusCode { get; set; }
        public string REFSessionId { get; set; }
      
    }
}
