﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Models
{
    public class Jobs
    {
        public long jobId { get; set; }
        public string name { get; set; }
        public long siteIdFilter { get; set; }
        public long departmentId { get; set; }
        public string jobCode { get; set; }
    }
}
