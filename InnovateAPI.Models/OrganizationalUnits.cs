﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InnovateAPI.Models
{
    public class OrganizationalUnits
    {
        public long? Id { get; set; }
        public string OrgName { get; set; }
        public long? HierarchyLevelId { get; set; }
        public long? ParentUnitId { get; set; }
        public long? Week1 { get; set; }
        public long? Week2 { get; set; }
        public long? Week3 { get; set; }
        public long? Week4 { get; set; }
        public long? Week5 { get; set; }
        public long? Week6 { get; set; }
        public long? Week7 { get; set; }
        public long? Week8 { get; set; }
        public long? Week9 { get; set; }
        public long? Week10 { get; set; }
        public long? Week11 { get; set; }
        public long? Week12 { get; set; }
        public long? Week13 { get; set; }
    }
}
