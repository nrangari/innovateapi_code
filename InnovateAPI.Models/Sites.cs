﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Models
{
    public class Sites
    {
        public long? Id { get; set; }
        public string Name { get; set; }

        public string LongName { get; set; }
        public string Status { get; set; }
        public string OpenedDate { get; set; }
        public string TimeZoneAssignmentId { get; set; }

        public dynamic TimeZoneAssignment { get; set; }

        public dynamic MailingAddress { get; set; }

        public string ParentOrganizationalUnitAssignmentId { get; set; }
        public dynamic ParentOrganizationalUnitAssignment { get; set; }

        public List<OperatingHoursSiteAssignments> OperatingHoursSiteAssignments { get; set; }

    }
    public class OperatingHoursSiteAssignments
    {
        public long? Id { get; set; }
        public string SiteId { get; set; }
        public string Start { get; set; }
        public string OperatingHoursId { get; set; }
        public dynamic OperatingHours { get; set; }
        public string Self { get; set; }

    }

    public class SiteCurrentTimeInformation
    {
       public long? SiteId { get; set; }
        public string CurrentSiteLocalTimestamp { get; set; }
        public string LaborDayStartTime { get; set; }
        public string CurrentBusinessDate { get; set; }
        public string PayPeriodStartDateTime { get; set; }
        public string PayPeriodEndDateTime { get; set; }
        public string LaborWeekStartDateTime { get; set; }
        public string LaborWeekEndDateTime { get; set; }
        public string CurrentBusinessDayStartTime { get; set; }
        public string CurrentBusinessDayEndTime { get; set; }
    }

    public class SiteOrganizationalUnit
    {
       public long? OrganizationalUnitId { get; set; }
       public List<SiteIds> SiteIds { get; set; }

    }

    public class SiteIds
    {
        public long? SiteId { get; set; }
    }
//    public class Actions
//    {
//        public string Rel { get; set; }
//        public string Method { get; set; }
//        public string Uri { get; set; }

    //    }
}
