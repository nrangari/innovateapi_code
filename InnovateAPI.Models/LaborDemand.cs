﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InnovateAPI.Models
{
    public class LaborDemand
    {
        public List<LaborDemandByDay> LaborDemandByDay { get; set; }
        public List<LaborDemandByQuarterHour> LaborDemandByQuarterHour { get; set; }

    }

    public class LaborDemandByDay
    {
        public object id { get; set; }
        public long? SiteId { get; set; }
        //public long? WorkgroupId { get; set; }
        //public long? LaborRoleId { get; set; }
        public string BusinessDate { get; set; }
        public string Date { get; set; }
        public string Hours { get; set; }
    }

    public class LaborDemandByDayOG
    {
        public object id { get; set; }
        public string siteId { get; set; }
        public DateTime businessDate { get; set; }        
        public long hours { get; set; }
    }
    public class LaborDemandByQuarterHour
    {
        public long? SiteId { get; set; }
        //public long? WorkgroupId { get; set; }
        //public long? LaborRoleId { get; set; }
        public string BusinessDate { get; set; }
        //public string StartTime { get; set; }
        //public string EndTime { get; set; }
        public string Hours { get; set; }
    }

    public class LaborHourDataOG
    {
        public object id { get; set; }
        public string SiteId { get; set; }
        public long Hours { get; set; }

        public long Year { get; set; }
        public long Week { get; set; }
        public long Quarter { get; set; }
        public int Month { get; set; }
        
    }
    public class WeekLaborDemandData
    {
        public object id { get; set; }
        public string siteId { get; set; }
        public string store { get; set; }
        public long hours { get; set; }
        public DateTime businessDate { get; set; }


    }

    public class LaborCollection
    {
        public List<WeekLaborDemandData> WeekLabor { get; set; }
        public long Year { get; set; }
        public long Week { get; set; }
        public long Quarter { get; set; }
        public int Month { get; set; }

    }
}
