﻿using InnovateAPI.IPlugin;
using System;
using System.Collections.Generic;
using System.Text;

namespace PluginBlueYonder
{
    public class BlueYonderAPI : InnovateInterface
    {
        BlueYonderHttpClient blClient;
        public BlueYonderAPI()
        {
           blClient = new BlueYonderHttpClient();
        }

        public dynamic GetLoginAPIResponse(string serverUrl, string apiUrl, string userName, string password)
        {
            dynamic logiResponse = blClient.LoginResponse(serverUrl, apiUrl, userName, password);
            return logiResponse;
        }
        public dynamic GetDataResponseFromAPI(string serverUrl, string apiUrl, string refSessionId)
        {
            dynamic responseResult = blClient.HttpClientResponse(serverUrl, apiUrl, refSessionId);
            return responseResult;
        }

        //public IEnumerable<object> GetActualLaborCostsByEmployeeId(string serverUrl,string apiUrl, string refSessionId, long siteId, long employeeId, string businessDate)
        //{
        //    dynamic responseResult = blClient.HttpClientResponse(serverUrl, apiUrl, refSessionId);
        //    return responseResult;
        //}

        //public IEnumerable<object> GetActualLaborCostsBySiteId(string serverUrl, string apiUrl, string refSessionId, long siteId, string businessDate)
        //{
        //    dynamic responseResult = blClient.HttpClientResponse(serverUrl, apiUrl, refSessionId);
        //    return responseResult;
        //}

        //public IEnumerable<object> GetCountries(string serverUrl, string apiUrl, string refSessionId)
        //{
        //    dynamic responseResult = blClient.HttpClientResponse(serverUrl, apiUrl, refSessionId);
        //    return responseResult;
        //}

        //public IEnumerable<object> GetLanguages(string serverUrl, string apiUrl, string refSessionId)
        //{
        //    dynamic responseResult = blClient.HttpClientResponse(serverUrl, apiUrl, refSessionId);
        //    return responseResult;
        //}

        //public IEnumerable<object> GetOrganizationalHierarchyLevels(string serverUrl, string apiUrl, string refSessionId)
        //{
        //    dynamic responseResult = blClient.HttpClientResponse(serverUrl, apiUrl, refSessionId);
        //    return responseResult;
        //}

        //public IEnumerable<object> GetOrganizationalUnits(string serverUrl, string apiUrl, string refSessionId)
        //{
        //    dynamic responseResult = blClient.HttpClientResponse(serverUrl, apiUrl, refSessionId);
        //    return responseResult;
        //}

        //public object GetSiteByOrganizationalUnitId(string serverUrl, string apiUrl, string refSessionId, long organizationUnitId)
        //{
        //    dynamic responseResult = blClient.HttpClientResponse(serverUrl, apiUrl, refSessionId);
        //    return responseResult;
        //}

        //public object GetSiteCurrentTimeInformation(string serverUrl, string apiUrl, string refSessionId, long siteId)
        //{
        //    dynamic responseResult = blClient.HttpClientResponse(serverUrl, apiUrl, refSessionId);
        //    return responseResult;
        //}

        //public IEnumerable<object> GetSitesById(string serverUrl, string apiUrl, string refSessionId, long siteId)
        //{
        //    dynamic responseResult = blClient.HttpClientResponse(serverUrl, apiUrl, refSessionId);
        //    return responseResult;
        //}

        //public IEnumerable<object> GetSitesByMultipleId(string serverUrl, string apiUrl, string refSessionId, long[] siteId)
        //{
        //    dynamic responseResult = blClient.HttpClientResponse(serverUrl, apiUrl, refSessionId);
        //    return responseResult;
        //}

        //public IEnumerable<object> GetSitesByName(string serverUrl, string apiUrl, string refSessionId, string siteName)
        //{
        //    dynamic responseResult = blClient.HttpClientResponse(serverUrl, apiUrl, refSessionId);
        //    return responseResult;
        //}
    }
}
