﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace PluginBlueYonder
{
    public class BlueYonderHttpClient
    {
        public dynamic LoginResponse(string serverUrl, string apiUrl,string userName,string password)
        {
            var request = WebRequest.Create(string.Concat(serverUrl, apiUrl)) as HttpWebRequest;
            request.CookieContainer = new CookieContainer();
            request.Method = "POST";
            string postData = string.Concat("loginName=", userName, "&password=", password);
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = byteArray.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            var response = request.GetResponse() as HttpWebResponse;
            return response;
        }
        public dynamic HttpClientResponse(string serverUrl, string apiUrl,string refSessionId)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("REFSSessionID", refSessionId);
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.BaseAddress = new Uri(serverUrl);
                    var response = client.GetAsync(apiUrl).Result;

                    var result = response.Content.ReadAsStringAsync().Result;
                    dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(result);
                    return dynamicObject;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
